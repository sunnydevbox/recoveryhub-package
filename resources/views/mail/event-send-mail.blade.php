<p>Dear {{ $patient_first_name }} {{ $patient_last_name }}</p>

<p>{{ $body }}</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>