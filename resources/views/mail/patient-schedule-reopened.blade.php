<p>Hi {{ $patient }},</p>

<p>The schedule you booked for {{ $availability }} with Dr. {{ $doctorName }} has been re-opened due to failure to the complete payment for <a href="{{ $orderUrl }}" target="_blank">Order #{{ $orderNumber }}</a>.</p>

<p>You may try to rebook this again however if payment was made through credit card, we suggest that before rebooking, you might want to get in touch with your bank to verify if the amount was charged to your account or not. </p>

<p>For questions or concerns, you can get in touch with us at <a href="mailto:info@recoveryhub.ph">info@recoveryhub.ph</a></p>

<p>Your friends,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>


