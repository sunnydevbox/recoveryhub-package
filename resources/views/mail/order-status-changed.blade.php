<p>Hi {{ $user->first_name }},</p>

<p>Your Order has been updated from <strong>{{ $oldStatus }}</strong> to <strong>{{ $newStatus }}</strong> 
    with order #<strong>{{ $order->id }}</strong> </p>


<h2>WHAT'S RECOVERYHUB?</h2>
 
<p>Is an online application where mental health consultation and treatment could be had via voice and video call with a registered licensed psychiatrist of choice. Having trouble getting started? Try pasting the link into your browser.</p>

<p>Cheers,<br/>
<strong>Recoveryhub Team</strong>
</p>