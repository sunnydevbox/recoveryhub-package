<p>Hi Admin,</p>

<p>Feedback from patient {{ $patient->first_name }} {{ $patient->last_name }} after call</p>
<p>Feedback : {{ $eventFeedback->comment }}</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>