<p>Dear {{ $patient->first_name }},</p>

<p>Your prescription is ready. Open the attached PDF file, print it and present it to any drugstore of choice. You can also call Lifeport Pharmacy for delivery of your medicines at this number +639988881602. If you have questions please don’t hesitate to email <a href = "mailto:info@recoveryhub.ph">info@recoveryhub.ph.</a></p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>