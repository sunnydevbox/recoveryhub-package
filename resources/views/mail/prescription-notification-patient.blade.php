<p>Dear {{ $patient->first_name }} {{ $patient->last_name }},</p>

<p>Good day!</p>

<p>Thank you for the purchase! Your Order has been received and being process.</p>

<h5>Transaction Number : {{ $transactionNo }}</h5>

<table>
    <tr>
        <th>Name</th>
        <th>Unit</th>
        <th>Dosage</th>
        <th>Quantity</th>
    </tr>
    @foreach ($order as $item)
    <tr>
        <td> {{ $item['medicineName'] }} </td>
        <td> {{ $item['medicineUnit'] }} </td>
        <td> {{ $item['medicineDosage'] }} </td>
        <td> {{ $item['qty'] }} </td>
    </tr>
    @endforeach

</table>


<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>