<p>Dear {{ $patient }},</p>

<p>
Good day! You have successfully scheduled an appointment with Dr. {{ $doctor }}. The details are as follows
</p>

<p>
Date: {{ $date }} @ {{ $time }}
</p>

<p>
On the scheduled time, make sure that you have a good internet connection, quiet place and please be reminded of the following online consultation etiquette:
</p>
<ol>
    <li>Please be on time and stay on the duration of the consultation schedule. You can initiate the call or wait for the doctor/therapist to call you. You are given 15 minutes to be online so that the doctor/therapist will not be late on his/her next appointment. Your fees is not refundable and schedule is non-transferrable.</li>
    <li>Come prepared by listing your concerns to your doctor/therapist.</li>
    <li>Wear appropriate clothing.</li>
    <li>Stay focused with the doctor/therapist you are consulting with.</li>
    <li>Center your camera.</li>
    <li>Keep the wriggling to a minimum.</li>
    <li>No eating during the whole duration of the consultation.</li>
    <li>Be aware of your lighting source and the amount of light you have.</li>
    <li>Just as you are seeing your doctor/therapist, remember that your doctor/therapist can also see you and what you are doing.</li>
    <li>Please be respectful at all times.</li>
    <li>Please don’t invite your friends or entertain friends while doing the online consultation.</li>
    <li>Treat your online consultation as if you are in the clinic.</li>
    <li>The doctor has the right to terminate the call should patient not follow online consultation etiquette 1 to 12. Fees made will not be refunded.</li<
</ol>

<p>
Equipment Required:
</p>
<ol>
    <li>Laptop or desktop with camera.</li>
    <li>Head phones.</li>
    <li>Good internet connection.  A minimum of 2mbps internet connection.</li>
    <li>Use of iPad or smartphones will be available in the near future.</li>
</ol>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>