<p>Dear {{ $patient->first_name }},</p>

<p>Your prescription is ready. <a href="{{ $prescriptionUrl }}"> Check this link </a></p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>