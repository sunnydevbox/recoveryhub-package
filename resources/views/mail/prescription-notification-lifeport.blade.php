<p>Dear LifePort,</p>

<p>{{ $patient->first_name }} {{ $patient->last_name }} purchased online, Please prepare listed medicine below</p>


<table>
    <tr>
        <th>Name</th>
        <th>Unit</th>
        <th>Dosage</th>
        <th>Quantity</th>
        <th>Update Url</th>
    </tr>
    @foreach ($order as $item)
    <tr>
        <td> {{ $item['medicineName'] }} </td>
        <td> {{ $item['medicineUnit'] }} </td>
        <td> {{ $item['medicineDosage'] }} </td>
        <td> {{ $item['qty'] }} </td>
        <td> <a href="{{ $item['url'] }}">Update</a></td>
    </tr>
    @endforeach

</table>

<p>Open the attached PDF file for Prescription Reference</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>