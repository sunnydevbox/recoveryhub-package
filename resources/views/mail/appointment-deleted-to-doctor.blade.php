<p>Dear Dr. {{ $doctor }},</p>

<p>Scheduled appointment with {{ $patient }}, {{ $date }} @ {{ $time }} has been cancelled.</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>