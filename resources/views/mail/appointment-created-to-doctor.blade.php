<p>Dear Dr. {{ $doctor }},</p>

<p>Patient {{ $patient }} booked your availablity.</p>

<p>
Date: {{ $date }} @ {{ $time }}
</p>

<p>
On the scheduled time, make sure that you have a good internet connection, quiet place and please be reminded of the following online consultation etiquette:
</p>
<ol>
    <li>Please be on time and stay on the duration of the consultation schedule. You can initiate the call or wait for the patient to call on the scheduled time. If the patient will not call after 15 mins, please make the initiative to call so that the fees will not be refunded. If you will be absent on the scheduled time, the fees will be automatically be refunded to the patient.</li>
    <li>Come prepared by reviewing the patient’s mental health sheet or progress notes prior to the consultation.</li>
    <li>Wear appropriate clothing.</li>
    <li>Stay focused with the patient you are consulting with.</li>
    <li>Center your camera.</li>
    <li>Keep the wriggling to a minimum.</li>
    <li>No eating during the whole duration of the consultation.</li>
    <li>Be aware of your lighting source and the amount of light you have.</li>
    <li>Remember that your patient can see where you are and what you are doing.</li>
    <li>Please be professional at all times.</li>
    <li>Please don’t invite your friends or entertain friends while doing the online consultation.</li>
    <li>Treat your online consultation as if you are in the clinic and respect your patient.</li>
</ol>

<p>
Equipment Required:
</p>
<ol>
    <li>Laptop or desktop with camera.</li>
    <li>Head phones.</li>
    <li>Good internet connection. Recommended: A minimum of 2mbps internet speed connection.</li>
    <li>Use of iPad will be available in the near future.</li>
</ol>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>