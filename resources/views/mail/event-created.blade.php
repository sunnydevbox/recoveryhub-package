<p>Dear {{ $event->doctor->getMeta('first_name') }},</p>

<p>You have successfully scheduled your availability. The details are as follows</p>

<p>
Date: {{ $date }} @ {{ $time }}
</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>