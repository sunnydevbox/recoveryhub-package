<p>Hi {{ $name }},</p>

<p>You have verified your email. Thank you</p>

<p>Welcome to RecoveryHub</p>

<h2>WHAT'S RECOVERYHUB?</h2>
 
<p>Is an online application where mental health consultation and treatment could be had via voice and video call with a registered licensed psychiatrist of choice. Having trouble getting started? Try pasting the link into your browser.</p>

<p>Cheers,<br/>
<strong>Recoveryhub Team</strong>
</p>