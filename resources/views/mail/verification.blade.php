<p>Hi {{ $user->getMeta('first_name') }},</p>

<p>You're almost ready to get started with <strong>Recovery Hub the first online mental health tool for your consultation needs</strong>. Please confirm your email, so we know it's really you.</p>
<p>Click on this link, <a href="{{ $verification_url }}">{{ $verification_url }}</a> , or copy and paste it into your browser's address bar to complete the process</p>

<p>After confirming, please log in and complete your mental health sheet so your doctor will be guided before the consultation.</p>

<h2>WHAT'S RECOVERYHUB?</h2>
 
<p>Is an online application where mental health consultation and treatment could be had via voice and video call with a registered licensed psychiatrist of choice. Having trouble getting started? Try pasting the link into your browser.</p>