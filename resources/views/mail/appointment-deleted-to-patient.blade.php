<p>Dear {{ $patient }},</p>

<p>Your appointment schedule with Dr. {{ $doctor }} scheduled at {{ $date }} @ {{ $time }} has been cancelled.</p>

<p>Hope you will reschedule with us sometime soon.</p>

<p>Thank you for your understanding.</p>

<p>Cheers,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>