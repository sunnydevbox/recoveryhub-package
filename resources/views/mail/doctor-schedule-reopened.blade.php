<p>Hi Dr. {{ $doctor }},</p>

<p>Your availability for {{ $availability }} re-opened due to patient's failure to the complete payment.</p>

<p>If there are any questions or concerns you can e-mail us at <a href="mailto:info@recoveryhub.ph">info@recoveryhub.ph</a></p>

<p>Your friends,<br/>
<strong>Recoveryhub.ph Team</strong>
</p>