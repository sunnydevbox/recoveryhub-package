<style>
     table {  
         color: #333;
         font-family: Helvetica, Arial, sans-serif;
         width: 100%; 
         font-size: 12px;
         border-collapse: collapse; 
         border-spacing: 0; 
     }

     p {
         line-height: 1.3;
         padding: 0;
         margin: 0;
     }
 
     .prescription-table td, th {  
         border: 1px solid transparent;
         height: 20px; 
         transition: all 0.3s;
     }
 
     .prescription-table th {  
         background: #DFDFDF;
         font-weight: bold;
     }
 
     .prescription-table td {  
         text-align: left;
     }

     .prescription-table tr:nth-child(odd) td { 
        background-color: rgba(255,255,255, 0.3);
    }
     .prescription-table tr:nth-child(even) td { 
        background-color: rgba(24,24,24, 0.3);
    }
 
     td {
         padding: 1px;
     }
     
     .dev-border-bot {
         border-bottom: 2px solid black;
     }
     .left {
         text-align: left;
     }
     .doc-addr {
         width: 140px;
     }
     .prescription {
         width: 50px;
     }
     .logo {
         width: 150px;
     }
     .main-container {
         border: 2px solid black;
         width: 100%;
         height: 675px;
         background: url("{{ base_path() }}/public/vendor/recoveryhub/img/watermark.jpg") repeat-y center center fixed; 
        -webkit-background-size: Contain;
        -moz-background-size: Contain;
        -o-background-size: Contain;
        background-size: Contain;
     }
     .main {
         padding-top: 10px;
         padding-right: 4px;
     }
     .note-block{
        /*
         position:absolute;
        bottom:150px;
        width:55%;
         */
        
        font-size:10px;
     }
     .mother-table {
         height: 100%;
         border: 1px solid;
     }
 </style>

 <div class="main">
     <div class="main-container">
         <table class="mother-table">
             <tbody>
                <tr>
                    <td>
                        <!-- HEADER -->
                        <table style="height:20%;">
                            <tr>
                                <td colspan="4">
                                    <table style="">
                                        <tr>
                                            <td style="width: 30%;">
                                                <img class="logo" src="{{ base_path() }}/public/vendor/recoveryhub/img/logo.jpg"/>
                                            </td>
                                            <td style="text-align: center;font-size:8px">
                                                <h3 style="line-height: 1.3; padding: 0; margin:0;">{{ (isset($doctor->general_information)) ? $doctor->general_information->pull('prefix') : '' }} {{ $doctor->first_name }} {{ $doctor->last_name }} {{ (isset($doctor->general_information)) ? $doctor->general_information->pull('suffix') : '' }}</h3>
                                                <p>PSYCHIATRIST</p>                                    
                                                @if(isset($doctor->general_information))
                                                <p>
                                                    {{ $doctor->general_information->pull('fellowOrDiplomate') == 'fellow' ? $doctor->general_information->pull('life') ? 'Fellow, Philippine Psychiatric Association (Life)': 'Fellow, Philippine Psychiatric Association' : 'Diplomate of the Specialty Board of Philippine Psychiatry' }}
                                                    <br/>{{ $doctor->general_information->pull('hospitalAffiliation') }}
                                                    <br/>{{ $doctor->general_information->pull('streetNo') }}, {{ $doctor->general_information->pull('streetAddress') }}, {{ $doctor->general_information->pull('city') }}
                                                </p>
                                                @endif
                                                <p>Tel No. {{ isset($doctor->general_information) ? $doctor->general_information->pull('phone') : '' }} </p>
                                            <p>Consultation Date: @if (isset($prescription->created_at) && !is_array($prescription->created_at)) {{ date('M d, Y', strtotime($prescription->created_at)) }} @endif</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td style="width: 10%;">Name :</td>
                                                <td><div class="dev-border-bot">{{ $patient->first_name }} {{ $patient->last_name }}</div></td>
                                                <td style="width: 10%;">Age : </td>
                                                <td style="width: 13%;"><div class="dev-border-bot">{{ (isset($patient->general_information)) ? $patient->general_information->pull('age') : '' }}</div></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 18%;">Address : </td>
                                                <td colspan="3" style=" font-size:10px">
                                                <div class="dev-border-bot text-small small">
                                                    {{ $patientAddress }}
                                                </div>
                                            </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>


                        <!-- list -->
                        <table style="height:50%;max-height:60%">
                            <tr style="">
                                <td colspan="4" valign="top">
                                    <img class="prescription" src="{{ base_path() }}/public/vendor/recoveryhub/img/rx.png"/>
                                    <div style="">
                                        <table class="prescription-table">
                                            <tbody>
                                                @foreach($items as $key => $item)
                                                    <tr>
                                                        <td>
                                                            <p style="font-weight:bold;"> {{ $key+1 }}.  {{ $item->medicine->generic->name}} ({{ $item->medicine->name}}) {{ $item->dosage}}/tab #{{ $item->qty_tabs_dispensed }} ({{ ucwords(convertNumber($item->qty_tabs_dispensed)) }})</p>
                                                        </td>
                                                    </tr>
                                                    <tr style="line-height: 3px;">
                                                        <td>
                                                            <p>Sig: {{ $item->instructions }}</p>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>  
                                </td>
                            </tr>
                        </table>

                        <!-- footer --> 
                        <table  style="">
                        <tr>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td colspan="2" width="50%">
                                                <div class="note-block">
                                                    <h4 style="margin:0">Notes : </h4>
                                                    @if (isset($prescription->notes) && !is_array($prescription->notes))
                                                        <p style="font-size:9px">
                                                            {{str_replace("\n","<br>",$prescription->notes)}}
                                                        </p>
                                                    @else
                                                        <em style="font-size:9px">- Not Specified -</em>
                                                    @endif

                                                    
                                                    <h4 style="margin:5px 0 0">Follow up Date: </h4>
                                                    @if ($date)
                                                        <p style="font-size:9px">{{ $date }}</p>
                                                    @else
                                                        <em style="font-size:9px">- Not Specified -</em>
                                                    @endif
                                                </div>
                                            </td>
                                            <td colspan="2" width="50%">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2" style=";text-align: center;position:relative;padding:0;margin:0">
                                                                                    @if ( $doctorSignature)
                                                                                        <!--<img style="width: 100px; height: 60px; position:absolute; bottom: 10.8rem; right: 4.8rem;" src="{{ (isset($doctor->signature)) ? $doctor->signature : '' }}" />-->
                                                                                        <img style="
                                                                                            height: 60px; 
                                                                                            position:absolute; 
                                                                                            bottom:-6px;
                                                                                            left: 0;
                                                                                            right: 0;
                                                                                            margin: auto;
                                                                                        " src="{{ $doctorSignature }}" />
                                                                                    @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="text-align: center;">
                                                                                <div>{{ (isset($doctor->general_information)) ? $doctor->general_information->pull('prefix') : '' }} {{ $doctor->first_name }} {{ $doctor->last_name }} {{ (isset($doctor->general_information)) ? $doctor->general_information->pull('suffix') : '' }}</div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 50%;">License No. :</td>
                                                                            <td class="dev-border-bot doc-addr">{{ (isset($doctor->general_information)) ? $doctor->general_information->pull('license_number') : '' }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>S2 No. :</td>
                                                                            <td class="dev-border-bot doc-addr">{{ ($hasS2 && isset($doctor->general_information)) ? $doctor->general_information->pull('s2_number') : '' }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Expiration Date :</td> 
                                                                            <td class="dev-border-bot doc-addr">{{ (isset($doctor->general_information)) ? $doctor->general_information->pull('s2_expiration_date') : '' }}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>PTR No. :</td>
                                                                            <td class="dev-border-bot doc-addr">{{ (isset($doctor->general_information)) ? $doctor->general_information->pull('prt_number') : '' }}</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                @if ( isset($notes->notes['medExpiration']) )
                                                    <p style="text-align: center; font-size:10px; font-weight: bold;">This prescription is valid only until {{ isset($notes->notes['medExpiration']) ? date('M d, Y', strtotime($notes->notes['medExpiration']))  : ''}}</p>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>   
                            </td>
                        </tr>
                        </table>
                    </td>
                </tr>
             </tbody>
         </table>
     </div>
 </div>
