<style>
     table {  
         color: #333;
         font-family: Helvetica, Arial, sans-serif;
         width: 100%; 
         font-size: 11px;
         border-collapse: collapse; 
         border-spacing: 0; 
     }
 
     .prescription-table td, th {  
         border: 1px solid transparent;
         height: 30px; 
         transition: all 0.3s;
     }
 
     .prescription-table th {  
         background: #DFDFDF;
         font-weight: bold;
     }
 
     .prescription-table td {  
         background: #FAFAFA;
         text-align: left;
     }
 
     .prescription-table tr:nth-child(even) td { background: #F1F1F1; }   
 
     .prescription-table tr:nth-child(odd) td { background: #FEFEFE; } 
 
     td {
         padding: 2px;
     }
     
     .dev-border-bot {
         border-bottom: 2px solid black;
     }
     .left {
         text-align: left;
     }
     .doc-addr {
         width: 140px;
     }
     .prescription {
         width: 30px;
     }
     .logo {
         width: 150px;
     }
     .main-container {
     }
     .main {
         padding-top: 10px;
         padding-right:2px;
     }

    @page { 
        margin: 70px !important; 
        padding-top:70px;
        padding-bottom:70px !important;
     
    }
    body { 
        margin: 70px !important; 
        padding-top:70px;
        padding-bottom:70px !important;
        background: url("{{ base_path() }}/public/vendor/recoveryhub/img/watermark.jpg") no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
    label {
        font-weight: bold;
    }
 </style>
 <div class="main">
     <div class="main-container">
         <table class="mother-table">
            <tbody>
                <tr>
                    <td colspan="4">
                        <table>
                            <tr>
                                <td style="width: 30%;">
                                    <img class="logo" src="{{ base_path() }}/public/vendor/recoveryhub/img/logo.jpg"/>
                                </td>
                                <td style="text-align: center;">
                                    @if ( $event->doctor->hasRole(['medical-practitioner']) ) 
                                        <h3>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }} {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('suffix') : '' }}</h3>
                                        <p>PSYCHIATRIST</p>
                                    @else
                                        <h3>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }}</h3>
                                        <p>Clinical Psychologist</p>
                                    @endif
                                    @if(isset($event->doctor->general_information))
                                        @if ( $event->doctor->hasRole(['medical-practitioner']) )
                                            <p>{{ $event->doctor->general_information->pull('fellowOrDiplomate') == 'fellow' ? $event->doctor->general_information->pull('life') ? 'Fellow, Philippine Psychiatric Association (Life)': 'Fellow, Philippine Psychiatric Association' : 'Diplomate of the Specialty Board of Philippine Psychiatry' }}</p>
                                            <p>{{ $event->doctor->general_information->pull('hospitalAffiliation') }}</p>
                                        @endif
                                        
                                        <p>{{ $event->doctor->general_information->pull('streetNo') }}, {{ $event->doctor->general_information->pull('streetAddress') }}, {{ $event->doctor->general_information->pull('city') }}</p>
                                    @endif
                                    <p>Tel No. {{ isset($event->doctor->general_information) ? $event->doctor->general_information->pull('phone') : '' }} </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width: 5%;">Name :</td>
                                    <td><div class="dev-border-bot">{{ $event->bookings->patient->first_name }} {{ $event->bookings->patient->last_name }}</div></td>
                                    <td style="width: 5%;">Age : </td>
                                    <td style="width: 13%;"><div class="dev-border-bot">{{ (isset($event->bookings->patient->general_information)) ? $event->bookings->patient->general_information->pull('age') : '' }}</div></td>
                                </tr>
                                <tr>
                                    <td style="width: 6%;">Address : </td>
                                    <td colspan="3"><div class="dev-border-bot">{{ $event->bookings->patient->full_address }}</div></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" >
                    @if ( $event->doctor->hasRole(['medical-practitioner']) )
                        <h3 style="text-align: center; padding-top:1rem;">MEDICAL ABSTRACT</h3>
                    @else 
                    <h3 style="text-align: center; padding-top:1rem;font-size:14px;">SUMMARY EVALUATION</h3>
                    @endif
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <P>DATE : {{ isset($notes->created_at) ? $notes->created_at : '' }}</P>
                    </td>
                </tr>

                @if ( $event->doctor->hasRole(['medical-practitioner']) )
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>General Data </label>
                        @isset($feedback->general_data)
                            <?php $generalData = explode("\n", $feedback->general_data); ?>
                            @foreach ($generalData as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Chief Complaint/s : </label>
                        @isset($feedback->chief_complaint)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->chief_complaint); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>History of Present Illness : </label>
                        @isset($feedback->history_of_present_illness)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->history_of_present_illness); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Course in the Ward/ Out Patient : </label>
                        @isset($feedback->course_in_the_ward_out_patient)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->course_in_the_ward_out_patient); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Laboratory Examination Result : </label>
                        @isset($feedback->laboratory_examination_result)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->laboratory_examination_result); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Psychological Examination Result : </label>
                        @isset($feedback->psychological_examination_result)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->psychological_examination_result); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Diagnosis : </label>
                        @isset($feedback->diagnosis)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->diagnosis); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Medication : </label>
                        @isset($feedback->medication)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->medication); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Recommendation : </label>
                        @isset($feedback->recommendation)
                            <?php $dataArr=null; $dataArr = explode("\n", $feedback->recommendation); ?>
                            @foreach ($dataArr as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>

                @else 
                <tr>
                    <td colspan="2" style="padding-top:1rem;">
                        <label>Evaluation </label>
                        @isset($feedback->general_data)
                            <?php $generalData = explode("\n", $feedback->general_data); ?>
                            @foreach ($generalData as $data)
                                <p>{{ $data }}</p>
                            @endforeach
                        @endisset
                    </td>
                </tr>
                @endif
                
                <tr>
                    <td colspan="4" style="position: absolute; bottom: 9rem; width: 100%; ">
                        <table style="width:100%;">
                            <tbody>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <div>
                                            <!-- <p>*This is issued upon patient's request and not valid for medico-legal purposes. </p> -->
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="position: absolute; bottom: 2rem;width: 100%;right: 6rem">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="width: 25%;">
                                        
                                    </td>
                                    <td style="width: 25%;">
                                        
                                    </td>
                                    <td style="text-align: center; border-top: 1px solid black; font-size:9px;">     
                                        <img style="width: 250px; height: 90px; position: absolute; bottom: 35; right: 145;" src="{{ (isset($event->doctor->signature)) ? $event->doctor->signature : '' }}" />                               
                                        <div>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }} {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('suffix') : '' }}</div>
                                        <p>PRC: {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('license_number') : '' }} </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
     </div>
 </div>