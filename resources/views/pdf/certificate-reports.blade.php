@extends('recoveryhub::layouts.reports')

@section('content')
    <table class="mother-table">
        <tbody>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td style="width: 30%;">
                                <img class="logo" src="{{ base_path() }}/public/vendor/recoveryhub/img/logo.jpg"/>
                            </td>
                            <td style="text-align: center;">
                                @if ( $event->doctor->hasRole(['medical-practitioner']) ) 
                                    <h3>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }} {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('suffix') : '' }}</h3>
                                    <p>PSYCHIATRIST</p>
                                @else
                                    <h3>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }}</h3>
                                    <p>Clinical Psychologist</p>
                                @endif
                                @if(isset($event->doctor->general_information))
                                    @if ( $event->doctor->hasRole(['medical-practitioner']) )
                                        <p>{{ $event->doctor->general_information->pull('fellowOrDiplomate') == 'fellow' ? $event->doctor->general_information->pull('life') ? 'Fellow, Philippine Psychiatric Association (Life)': 'Fellow, Philippine Psychiatric Association' : 'Diplomate of the Specialty Board of Philippine Psychiatry' }}</p>
                                        <p>{{ $event->doctor->general_information->pull('hospitalAffiliation') }}</p>
                                    @endif
                                    
                                    <p>{{ $event->doctor->general_information->pull('streetNo') }}, {{ $event->doctor->general_information->pull('streetAddress') }}, {{ $event->doctor->general_information->pull('city') }}</p>
                                @endif
                                <p>Tel No. {{ isset($event->doctor->general_information) ? $event->doctor->general_information->pull('phone') : '' }} </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 10%;">Name :</td>
                                <td><div class="dev-border-bot">{{ $event->bookings->patient->first_name }} {{ $event->bookings->patient->last_name }}</div></td>
                                <td style="width: 10%;">Age : </td>
                                <td style="width: 13%;"><div class="dev-border-bot">{{ (isset($event->bookings->patient->general_information)) ? $event->bookings->patient->general_information->pull('age') : '' }}</div></td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">Address : </td>
                                <td colspan="3"><div class="dev-border-bot">{{ $event->bookings->patient->full_address }}</div></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                @if ( $event->doctor->hasRole(['medical-practitioner']) )
                    <h3 style="text-align: center; padding-top:1rem;">MEDICAL CERTIFICATE</h3>
                @else 
                    <h3 style="text-align: center; padding-top:1rem;">CONSULTATION REPORT</h3>
                @endif
                </td>
            </tr>
            <tr>
                <td>
                    <P>DATE : {{ isset($notes->created_at) ? $notes->created_at : '' }} </P>
                </td>
            </tr>
            <!-- <tr>
                <td style="padding-top:1rem;">
                    <label>DIAGNOSIS : </label>
                    <table style="border: 1px dotted black !important; padding: 2px;">
                        <tr>
                            <th style="border: 1px dotted black !important; width:5%;"></th>
                            <th style="text-align:left; width:35%; border: 1px dotted black !important;">Diagnostic Request</th>
                            <th style="text-align:left; border: 1px dotted black !important;">Diagnostic Type</th>
                            <th style="text-align:left; border: 1px dotted black !important;">Diagnostic Test</th>
                        </tr>
                        <tr>
                        @if (isset($notes->notes['medCert']))
                            @foreach ($notes->notes['medCert'] as $key => $value)
                            <td style="border: 1px dotted black !important;">{{ $key+1 }}</td>
                            <td style="border: 1px dotted black !important;">{{ isset($value['diagnosticRequest']) ? $value['diagnosticRequest'] : '' }}</td>
                            <td style="border: 1px dotted black !important;">{{ isset($value['diagnosticType']) ? $value['diagnosticType'] : '' }}</td>
                            <td style="border: 1px dotted black !important;">{{ isset($value['diagnosticTest']) ? $value['diagnosticTest'] : '' }}</td>
                            @endforeach
                        @endif
                        </tr>
                    </table>
                </td>
            </tr> -->
            <tr>
                <td style="padding-top:1rem;">
                    <!-- <label>RECOMENDATIONS : </label> -->
                    
                <?php 
                    if ( isset($notes->notes['medCertRecomendation']) ){
                        echo "<p>".str_replace("\n","<br>",$notes->notes['medCertRecomendation']) . "</p>";
                    } 
                ?>
                    
                    <!-- <P>{{ isset($notes->notes['medCertRecomendation']) ? str_replace("\n","<br>",$notes->notes['medCertRecomendation']) : '' }}</P> -->
                </td>
            </tr>
            <tr>
                <td colspan="4" style="position: absolute; bottom: 7rem; width: 100%; ">
                    <table style="width:100%;">
                        <tbody>
                            <tr>
                                <td colspan="2" style="text-align: center;">
                                    <div>
                                    @if ( $event->doctor->hasRole(['medical-practitioner']) )
                                        <p>*This Certification is issued upon patient's request and not valid for medico-legal purposes. </p>
                                    @endif
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="position: absolute; bottom: 2rem;width: 100%;right: 1rem">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 25%;">
                                    
                                </td>
                                <td style="width: 25%;">
                                    
                                </td>
                                <td style="text-align: center; border-top: 1px solid black; font-size:9px;">  
                                    <img style="width: 100px; height: 50px; position: absolute; bottom: 35; right: 85;" src="{{ (isset($event->doctor->signature)) ? $event->doctor->signature : '' }}" />                                  
                                    <div>{{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('prefix') : '' }} {{ $event->doctor->first_name }} {{ $event->doctor->last_name }} {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('suffix') : '' }}</div>
                                    <p>PRC: {{ (isset($event->doctor->general_information)) ? $event->doctor->general_information->pull('license_number') : '' }} </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
@endsection