<style>
     table {  
         color: #333;
         font-family: Helvetica, Arial, sans-serif;
         width: 100%; 
         font-size: 11px;
         border-collapse: collapse; 
         border-spacing: 0; 
     }
 
     .prescription-table td, th {  
         border: 1px solid transparent;
         height: 30px; 
         transition: all 0.3s;
     }
 
     .prescription-table th {  
         background: #DFDFDF;
         font-weight: bold;
     }
 
     .prescription-table td {  
         background: #FAFAFA;
         text-align: left;
     }
 
     .prescription-table tr:nth-child(even) td { background: #F1F1F1; }   
 
     .prescription-table tr:nth-child(odd) td { background: #FEFEFE; } 
 
     td {
         padding: 2px;
     }
     
     .dev-border-bot {
         border-bottom: 2px solid black;
     }
     .left {
         text-align: left;
     }
     .doc-addr {
         width: 140px;
     }
     .prescription {
         width: 30px;
     }
     .logo {
         width: 150px;
     }
     .main-container {
         border: 2px solid black;
         width: 100%;
         height: 695px;
         background: url("{{ base_path() }}/public/vendor/recoveryhub/img/watermark.jpg") no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
     }
     .main {
         padding-top: 10px;
         padding-right:2px;
     }
 </style>
 <div class="main">
     <div class="main-container">
         @yield('content')
     </div>
 </div>