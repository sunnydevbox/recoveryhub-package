<?php

return [

    'allowMultipleBookings' => false,

    'generalEmail' => ['info@recoveryhub.ph', 'contactus@recoveryhub.ph'],

    'lifeport_email' => ['felix.bongo@tweeklabs.com'],

    'info_recovery_email' => ['info@recoveryhub.ph'],

    'models' => [
        'medical-practitioner' => 'Sunnydevbox\\Recoveryhub\\Models\\MedicalPractitioner',
        'patient' => 'Sunnydevbox\\Recoveryhub\\Models\\Patient',
        'user' => 'Sunnydevbox\\Recoveryhub\\Models\\User',
        'event' => 'Sunnydevbox\\Recoveryhub\\Models\\Event',
        'appointment' => 'Sunnydevbox\\Recoveryhub\\Models\\Appointment',
        'event-note' => 'Sunnydevbox\\Recoveryhub\\Models\\EventNote',
        'tokbox-callback' => 'Sunnydevbox\\Recoveryhub\\Models\\TokboxCallback',
        'profile-question' => 'Sunnydevbox\\Recoveryhub\\Models\\ProfileQuestion',
        'profile-question-answer' => 'Sunnydevbox\\Recoveryhub\\Models\\ProfileQuestionAnswer',
        'event-prescription' => 'Sunnydevbox\\Recoveryhub\\Models\\EventPrescription',
        'event-prescription-item' => 'Sunnydevbox\\Recoveryhub\\Models\\EventPrescriptionItem',
        'medicine-generic-name' => 'Sunnydevbox\\Recoveryhub\\Models\\MedicineGenericName',
        'medicine-inventory' => 'Sunnydevbox\\Recoveryhub\\Models\\MedicineInventory',
        'mental-health-data' => 'Sunnydevbox\\Recoveryhub\\Models\\MentalHealthData',
        'diagnostic-request' => 'Sunnydevbox\\Recoveryhub\\Models\\DiagnosticRequest',
    ],

    'controllers' => [
        'user' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\UserController',
        'medical-practitioner' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\MedicalPractitionerController',
        'patient' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\PatientController',
        'event' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\UserEventController',
        'event-notes' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\UserEventNoteController',
        'appointment' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\UserAppointmentController',
        'my-medical-practitioner' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\MyMedicalPractitionerController',
        'my-patient' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\MyPatientController',
        'tokbox-callback' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\TokboxController',
        'profile-question' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\ProfileQuestionController',
        'profile-question-answer' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\UserProfileQuestionAnswerController',
        'event-prescriptions' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\EventPrescriptionController',
        'event-prescription-items' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\EventPrescriptionItemController',

        'medicine-generic-name' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\MedicineGenericNameController',
        'medicine-inventory' => '\\Sunnydevbox\\Recoveryhub\\Http\\Controllers\\API\\V1\\MedicineInventoryController',
    ],

    'events' => [
        'interval' => 60, // default duration of an event; 60 mins
    ],

    'users' => [
        'timezone' => 'Asia/Manila', // default timezone of users
    ],
    'status' => [
        'open' => 'OPEN',         // newly created, no patient has booked this yet
        'cancelled' => 'CANCELLED',    // booked but cancelled
        'completed' => 'COMPLETED',    // appointment done/completed
        'started' => 'STARTED',      // call is ongoing
        'pending' => 'PENDING_APPROVAL', // pending apporval from doctor of the booked appointment
        'booked' => 'BOOKED',       // event has been booked
    ],


    'prescription_pdf_location' => base_path() . '/storage/prescriptions/',

    'dragonpay_mode' => env('DRAGONPAY_MODE', null),
    'dragonpay_api_url' => env('DRAGONPAY_API_URL', null),
    'dragonpay_callback' => env('DRAGONPAY_CALLBACK_URL', null),
    'dragonpay_merchant_id' => env('DRAGONPAY_MERCHANT_ID', null),
    'dragonpay_merchant_key' => env('DRAGONPAY_MERCHANT_KEY', null),
    'dragonpay_rest_api_url' => env('DRAGONPAY_REST_API_URL', null),

    'event_default_tax' => env('EVENT_DEFAULT_TAX', 0),


    'organization_email' => 'pehjeh@gmail.com',
];