FORMAT: 1A

# Recoveryhub

# Cart [/cart]
User resource representation.

## Checkout [POST /cart/checkout]
Apply a coupon code to the current cart. Additional data may be passed

+ Response 200 (application/json)

## Place order [POST /cart/place-order]
Order is placed and charging to payment gateway is initiated. Additional data may be passed

+ Response 200 (application/json)

## Gateway Callback [GET /cart/callback]
Receives transaction information from payment gateway via callback

+ Response 200 (application/json)

## Get cart content [GET /cart]
Returns cart's content including calculations and applied coupons

+ Response 200 (application/json)

## Destroy cart [GET /cart]
Destroy's current cart

+ Response 200 (application/json)

# Coupon [/shop/coupons]
User resource representation.

## Apply coupon [POST /shop/coupons/apply]
Apply a coupon code to the current cart

+ Parameters
    + code: (string, required) - Coupon code.

+ Request (application/json)
    + Body

            {
                "code": "COUPONCODE123"
            }

+ Response 200 (application/json)
    + Body

            []

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_already_applied",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_inactive",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_expired",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_not_found",
                "status_code": "500"
            }

## Remove coupon [POST /shop/coupons/remove]
Remove an applied coupon code from the current cart

+ Parameters
    + code: (string, required) - Coupon code.

+ Request (application/json)
    + Body

            {
                "code": "COUPONCODE123"
            }

+ Response 200 (application/json)
    + Body

            []

## Calculate [POST /shop/coupons/calculate]
Calculate the amount of discount applicable to the current cart

+ Parameters
    + code: (string, required) - Coupon code.

+ Request (application/json)
    + Body

            {
                "code": "COUPONCODE123"
            }

+ Response 200 (application/json)
    + Body

            []

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_already_applied",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_inactive",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_expired",
                "status_code": "500"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "coupon_not_found",
                "status_code": "500"
            }

# Cart Transactions [/cart/transaction]
User's cart transactions.

## User's transactions [POST /cart/transaction]
Returns user's list of transactions

+ Response 200 (application/json)
    + Body

            {
                "id": 53,
                "order_id": 53,
                "gateway": "coupon",
                "transaction_id": "5b9b984b7b8c4",
                "detail": "successful callback",
                "token": "5b9b984b7b8cf",
                "created_at": "2018-09-14 19:15:23",
                "updated_at": "2018-09-14 19:15:23",
                "user_id": 3,
                "statusCode": "completed"
            }

# Supplementary Data [/suppdata]
Supplementary data.

## Data list [GET /suppdata]
Returns data based upon request

+ Parameters
    + data: `medicines;medicine-generic-names` (string, required) - Separate items by semicolon (;)

+ Response 200 (application/json)

# Event Prescription [/event-prescription]
Event Prescription

## Store [POST /event-prescription]
Save prescription

+ Request (application/json)
    + Body

            {
                "event_id": 1,
                "notes": "Do not forget to take your medicines",
                "items": [
                    {
                        "medicine_id": 1,
                        "qty_tabs_dispensed": "2",
                        "dosage": "500mg",
                        "instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
                    }
                ]
            }

+ Response 200 (application/json)
    + Body

            {
                "data": {
                    "event_id": 1,
                    "notes": "Do not forget to take your medicines",
                    "created_at": "2018-09-16 19:48:52",
                    "id": 8,
                    "items": [
                        {
                            "id": 27,
                            "event_prescription_id": 8,
                            "medicine_id": 1,
                            "qty_tabs_dispensed": "2",
                            "dosage": "500mg",
                            "instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
                        }
                    ]
                }
            }

+ Response 400 (application/json)
    + Body

            {
                "message": "MESSAGE_HERE",
                "status_code": "400"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "MESSAGE_HERE",
                "status_code": "500"
            }

## Update [PUT /event-prescription/{id}]
Update a prescription

+ Parameters
    + id: (integer, required) - Event Prescription ID

+ Request (application/json)
    + Body

            {
                "event_id": 1,
                "notes": "Do not forget to take your medicines",
                "items": [
                    {
                        "medicine_id": 1,
                        "qty_tabs_dispensed": "2",
                        "dosage": "500mg",
                        "instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
                    }
                ]
            }

+ Response 200 (application/json)

+ Response 400 (application/json)
    + Body

            {
                "message": "MESSAGE_HERE",
                "status_code": "400"
            }

+ Response 500 (application/json)
    + Body

            {
                "message": "MESSAGE_HERE",
                "status_code": "500"
            }