<?php

Broadcast::channel('opentok.session.{sessionId}', function ($user, $sessionId) {
    return $user->id === Order::findOrNew($orderId)->user_id;
});