<?php
use Sunnydevbox\Recoveryhub\Models\DiagnosisDSM5;

Route::get('/', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\HomeController@index');

if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {

	$api = app('Dingo\Api\Routing\Router');

	$api->version('v1', ['middleware' => ['api.auth']], function ($api) {

		// test
		// $api->post('users/register2', config('recoveryhub.controllers.user') . '@register')->name('user.recovery.register');


		$api->get('suppdata', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\SuppDataController@index');

		$api->resource('users', config('recoveryhub.controllers.user'));
		$api->post('users/{id}/picture', config('recoveryhub.controllers.user') . '@uploadPicture')->name('user.pictureupload');
		$api->post('users/{id}/signature', config('recoveryhub.controllers.user') . '@uploadSignature')->name('user.signatureupload');
		$api->post('users/{id}/status', config('recoveryhub.controllers.user') . '@assignStatus')->name('user.status');

		$api->get('user/events/appointments', config('recoveryhub.controllers.event') . '@getAppointments')->name('user.events.appointments');
		$api->post('user/events/{id}/set-session', config('recoveryhub.controllers.event') . '@setSession')->name('user.events.set-session');
		$api->get('user/events/{id}/get-session', config('recoveryhub.controllers.event') . '@getSession')->name('user.events.get-session');

		$api->resource('user/events/notes', config('recoveryhub.controllers.event-notes'), ['only' => ['index', 'show', 'store']]);

		$api->resource('user/events', config('recoveryhub.controllers.event'));

		$api->post('user/appointments/cancel/{id}', config('recoveryhub.controllers.appointment').'@cancel')->name('user.appointments-cancel');
		$api->resource('user/appointments', config('recoveryhub.controllers.appointment'));

		
		$api->get('medical-practitioners/my', config('recoveryhub.controllers.medical-practitioner').'@myDoctors')->name('my.doctors');
		$api->resource('medical-practitioners', config('recoveryhub.controllers.medical-practitioner'));

		$api->get('patients/my', config('recoveryhub.controllers.patient').'@myPatients')->name('my.patients');
		$api->resource('patients', config('recoveryhub.controllers.patient'));
		$api->resource('staff', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\Admin\StaffController');
		$api->resource('profile-questions', config('recoveryhub.controllers.profile-question'));
		$api->post('profile-questions/answer', config('recoveryhub.controllers.profile-question') . '@answer')->name('profile-question-answer');
		$api->resource('profile-questions/answer', config('recoveryhub.controllers.profile-question'), ['only' => ['index']]);

		$api->resource('tokbox-callback', config('recoveryhub.controllers.tokbox-callback'), ['only' => ['index', 'show']]);

		// CART 
		$api->post('events/add-to-cart', config('recoveryhub.controllers.event') . '@addToCart')->name('event.add-to-cart');

		$api->get('rate-charges/doctor/{id?}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\RateChargeController@getByDoctor');
		$api->put('rate-charges/doctor/{id?}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\RateChargeController@updateByDoctor');
		$api->resource('rate-charges', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\RateChargeController');

		// EVENT FEEDBACK
		$api->resource('event-feedbacks', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventFeedbackController');
		$api->get('event-feedbacks/{event_id}/{role}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventFeedbackController@getByEventRole');
		$api->post('event-feedbacks/send-medication', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventFeedbackController@sendMedication');
		$api->get('feedbacks/{role}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventFeedbackController@getByRole');
		$api->get('feedbacks/{id}/{role}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventFeedbackController@getByIdRole');

		// EVENT PRESCRIPTIONS
		// PRESCRIPTION FLAG UPDATE 

		$api->get('event-prescriptions/preview/{event_id}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionController@previewGet');
		$api->post('event-prescriptions/preview', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionController@preview');
		$api->resource('event-prescriptions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionController');
		$api->post('prescription-items/check-available/{id}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionItemController@checkAvailableQuantity')->name('event-prespcription-item.check-remaining-qty');
		$api->post('prescription-items/add-to-cart', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionItemController@addToCart')->name('event-prespcription-item.add-to-cart');
		$api->resource('event-prescription-items', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionItemController');
		

		$api->post('event-prescription/flag', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\EventPrescriptionController@flagUpdate');

		// MEDICINE INVENTORY
		$api->resource('medicine-inventory', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\MedicineInventoryController');
		$api->resource('medicine-generic-name', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\MedicineGenericNameController');		

		// MENTAL HEALTH
		$api->get('mental-health-data/user/{id}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\MentalHealthDataController@getByUserId');
		$api->put('mental-health-data/user/{id}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\MentalHealthDataController@updateByUserId');
		$api->resource('mental-health-data', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\MentalHealthDataController');

		// ALTPAY PAYMENT !!!! TEMPORARY
		$api->post('payment/checkout-id', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\PaymentController@checkout')->name('payment.altpay-getcheckoutid');
		
		//REPORTS
		$api->get('reports/generate', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\ReportController@generate');
		// SEND EMAIL WITH PDF
		$api->get('reports/email', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\ReportController@sendEmail');

		$api->get('diagnostic', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosticController@getList');

		// PRESCRIPTON TRANSACTION
		$api->get('prescription-transactions/status/{id}/{status}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\PrescriptionTransactionController@updateStatus');

		$api->resource('prescription-transactions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\PrescriptionTransactionController');

		$api->resource('cart/items', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartItemController');
		$api->resource('cart/orders', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartOrderController');

		$api->get('cart', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartController@getContent')->name('cart.get-content2');
		// $api->delete('cart/item', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartController@deleteCartItem')->name('cart.delete-cart-item');
		$api->post('cart/checkout','\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartController@checkout')->name('cart.checkout');
		$api->post('cart/place-order','\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartController@placeOrder')->name('cart.place-order2');
		$api->post('cart/item/delete', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartController@deleteCartItem')->name('cart.delete-cart-item');

		// $api->resource('cart/transactions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController');
		$api->get('cart/transactions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController@index')->name('cart-transactions2.index');
		
		$api->get('cart/transactions/{cart_transaction}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController@show')->name('cart-transactions2.show');
		$api->post('cart/transactions/status',	'\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController@callback')->name('shop.post.callback');
		$api->resource('cart/transactions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController');


		// FOR CURRENT LOGGED IN USER
		$api->get('appointments/my', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\AppointmentController@my')->name('appointment.my');


		$api->resource('shop/coupons', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController');

		$api->post('appointments/my/marked-dates', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\AppointmentController@myMarkedDates')->name('appointment.my.marked-dates');


		// OVERRIDING THIS ROUTE
		$api->post('shop/coupons/apply', 		'\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController@apply')->name('shop.coupon-apply-to-cart');
		$api->post('shop/coupons/remove', 		'\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController@remove')->name('shop.coupon-remove-from-cart');
		$api->post('shop/coupons/apply', 		'\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController@apply')->name('shop.coupon-apply-to-cart');
		$api->post('shop/coupons/calculate', 	'\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController@calculate')->name('shop.coupon-calculate');
		$api->resource('shop/coupons', 			 '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CouponController');

		// TOKBOX
		$api->post('rtc/log', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@log')->name('opentok.log');
		$api->post('rtc/generate-token', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@generateToken')->name('opentok.generate-token');
		$api->post('rtc/generate-session', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@generateSession')->name('opentok.generate-session');
		$api->post('rtc/session', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@getSessionByEvent')->name('opentok.get-session-by-event');
		$api->post('rtc/set-status/{sessionId}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@setStatusByUserType')->name('opentok.set-status-by-user-type');
			// $api->get('rtc/test', function() {
			// 	event(new \Sunnydevbox\Recoveryhub\Events\OpentokSessionStatus('2_MX40NjE5MDEzMn5-MTU2MTcyNzI1Nzc5Nn5PRmFBMTdhcEdyemNnYmJYc1d1dkgrK25-QX4'));
			// });
		$api->get('rtc/pusher', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@pusher');
		$api->get('rtc/redis/{sessionId}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController@redis');
		$api->resource('rtc', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\OpenTokController');


		$api->group(['prefix' => 'admin'], function($api) {
			$api->resource('appointments', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\Admin\AppointmentController');
		});


	});	

	$api->version('v1', [], function ($api) {

		$api->resource('contact-form-submissions', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\ContactFormSubmissionController');

		// $api->get('prescription-transactions/status/{id}/{status}', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\PrescriptionTransactionController@updateStatus');

		$api->group(['prefix' => 'diagnosis-dsm5'], function($api) {
			$api->post('descendants-by-name', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosisDSM5Controller@getDescendantsByLabel')->name('diagnosis.dsm5-get-by-name');
			$api->get('fix', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosisDSM5Controller@fix')->name('diagnosis.dsm5-fix');
			$api->get('root', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosisDSM5Controller@root')->name('diagnosis.dsm5-root');
			$api->get('descendants', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosisDSM5Controller@descendants')->name('diagnosis.dsm5-descendants');
			$api->get('ancestors', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\DiagnosisDSM5Controller@ascendants')->name('diagnosis.dsm5-ancestors');
		});
	});
}
Route::post('shop/{gateway}/postback', '\Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\CartTransactionController@postback')->name('shop.postback-gateway');
Route::post('tokbox-callback', config('recoveryhub.controllers.tokbox-callback') . '@store1'); 
// Route::get('pj', function() {
// 	$p =  'MmLoe32REA6FcKs2';//\Illuminate\Support\Str::random();
// 	// MmLoe32REA6FcKs2
// 	// echo $p;
// 	// dd(\Hash::make($p));
// 	// $file_ = \File::get(storage_path('dsm5.csv'));

// 	// $file = fopen(storage_path('dsm5.csv'), "r");
// 	// $all_data = array();
// 	$list = array_map('str_getcsv', file(storage_path('dsm5-2.csv')));

// 	$last0 = $last1 = $last2 = $last3 = $last4 = $last5 = null;

// 	foreach($list as $k => $row) {

// 		if ($k != 0){
// 			$row0 = $row1 = $row2 = $row3 = $row4 =$row5 = null;
			
// 			if($row[0] != '') {
// 				$last0 = $row0 = DiagnosisDSM5::create([
// 					'label' => $row[0],
// 					'parent_id' => null,
// 				]);				
// 			} else {
// 				$row0 = $last0;
// 			}

// 			if($row[1] != '') {
// 				$last1 = $row1 = DiagnosisDSM5::create([
// 					'label' => $row[1],
// 					'parent_id' => $row0->id,
// 				]);				
// 			} else {
// 				$row1 = $last1;
// 			}

// 			if($row[2] != '') {
// 				$last2 = $row2 = DiagnosisDSM5::create([
// 					'label' => $row[2],
// 					'parent_id' => $row1->id,
// 				]);				
// 			} else {
// 				$row2 = $last2;
// 			}

// 			if($row[3] != '') {
// 				$last3 = $row3 = DiagnosisDSM5::create([
// 					'label' => $row[3],
// 					'parent_id' => $row2->id,
// 				]);				
// 			} else {
// 				$row3 = $last3;
// 			}

// 			if($row[4] != '') {
// 				$last4 = $row4 = DiagnosisDSM5::create([
// 					'label' => $row[4],
// 					'parent_id' => $row3->id,
// 				]);				
// 			} else {
// 				$row4 = $last4;
// 			}

// 			if($row[5] != '') {
// 				$last5 = $row5 = DiagnosisDSM5::create([
// 					'label' => $row[5],
// 					'parent_id' => $row4->id,
// 				]);				
// 			} else {
// 				$row5 = $last5;
// 			}
// 		}
// 	}

// 	// dd(fgetcsv($file, 200, ","));
// 	// while ( ($data = fgetcsv($file, 200, ",")) {

// 	// 	$name = $data[0];
// 	// 	$city = $data[1];
// 	// 	$all_data = $name. " ".$city;

// 	// 	array_push($array, $all_data);
// 	// }
// 	// fclose($file);
// }); 