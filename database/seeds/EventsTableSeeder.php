<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\User;
use Sunnydevbox\Recoveryhub\Models\MedicalPractitioner;
use Sunnydevbox\Recoveryhub\Models\Patient;
use Sunnydevbox\TWUser\Models\Role;

use Sunnydevbox\Recoveryhub\Repositories\MedicalPractitioner\MedicalPractitionerRepository;
use Sunnydevbox\TWEvents\Models\Event;


class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(MedicalPractitionerRepository $rpo)
    {
        \DB::table('events')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('events')->truncate();

        $faker = \Faker\Factory::create();


        foreach($rpo->all() as $user) {
            // $user->assignEvent([

            //     'start_at' => $faker->dateTime(),
            //     'end_at' => $faker->dateTime(),
            //     'label' => 'TEt',
            //     ''
            // ]);

            Event::create([
                'start_at' => $faker->dateTime(),
                'end_at' => $faker->dateTime(),
                'label' => 'TEt',
                'assignable_id' => $user->id,
                'assignable_type' => $rpo->model(),
            ]);
        }

    }
}
