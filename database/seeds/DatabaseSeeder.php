<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;
use Sunnydevbox\Recoveryhub\Models\DiagnosticRequest;
use Sunnydevbox\Recoveryhub\Models\DiagnosticTest;
use Sunnydevbox\Recoveryhub\Models\DiagnosticType;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ROLES
        $this->call(RolesTableSeeder::class);
        
        // PERMISSIONS
        $this->call(PermissionsTableSeeder::class);

        // ROLE-PERMISSION
        //$this->call(RolePermissionTableSeeder::class);

        // USERS
        $this->call(UsersTableSeeder::class);

        // USER META
        $this->call(UserMetaTableSeeder::class);

        // USER-ROLES
        $this->call(UserRoleTableSeeder::class);

        // EVENTS
        $this->call(EventsTableSeeder::class);
        
        // DIAGNOSTIC REQUEST
        $this->call(DiagnosticRequest::class);
        
        // DIAGNOSTIC TEST
        $this->call(DiagnosticTest::class);
        
        // DIAGNOSTIC TYPE
        $this->call(DiagnosticType::class);

        // APPOINTMENTS
        //$this->call(AppointmentsTableSeeder::class);
        
    }
}
