<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $R = new User;
        \DB::table($R->getTable())->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table($R->getTable())->truncate();

        $faker = \Faker\Factory::create();
        
        $defaultUsers = [
            [
                'email'     => 'admin@admin.com',
                'password'  => '1234qwer',
                // 'first_name' => 'Admin',
                // 'last_name' => 'Admin',

                'is_verified'   => $faker->dateTime()
            ],

            [
                'email'     => 'doctor@doctor.com',
                'password'  => '1234qwer',
                // 'first_name' => 'Doctor',
                // 'last_name' => 'Doctor',
                
                'is_verified'   => $faker->dateTime(),
            ],

            [
                'email'     => 'patient@patient.com',
                'password'  => '1234qwer',
                // 'first_name' => 'Patient',
                // 'last_name' => 'Patient',
                
                'is_verified'   => $faker->dateTime(),
            ],
        ];

        foreach($defaultUsers as $user) {
            User::create($user);
        }


        foreach(range(1, 200) as $index) {

            $gender = rand(0,1) ? 'male' : 'female';

        	User::create([
                'email'      => $faker->unique()->email,
                'password'   => '1234qwer',
        		// 'first_name' => $faker->firstName($gender),
        		// 'last_name'  => $faker->lastName($gender),

                'is_verified'   => $faker->dateTime(),
        	]);
        }

    }
}
