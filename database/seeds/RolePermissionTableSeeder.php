<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\TWUser\Models\Role;


class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
        	'admin',
        	'medical-practitioner',
        	'patient',
        	'admin-staff',
        ];

        $roles = Role::all();


        foreach($roles as $role) {

        	Role::create([
        		'name' 	=> $role,
        		'guard_name'	=> 'web',
        	]);
        }

    }
}
