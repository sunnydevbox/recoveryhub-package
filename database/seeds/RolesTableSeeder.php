<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\TWUser\Models\Role;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $R = new Role;
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table($R->getTable())->truncate();
        
        $roles = [
        	'admin',
        	'medical-practitioner',
        	'patient',
        	'admin-staff',
        ];


        foreach($roles as $role) {

        	Role::create([
        		'name' 	=> $role,
        		'guard_name'	=> 'web',
        	]);
        }

    }
}
