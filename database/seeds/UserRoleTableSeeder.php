<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\User;
use Sunnydevbox\Recoveryhub\Models\MedicalPractitioner;
use Sunnydevbox\Recoveryhub\Models\Patient;
use Sunnydevbox\TWUser\Models\Role;


class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \DB::table('model_has_roles')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('model_has_roles')->truncate();

        User::find(1)->assignRole('admin');
        // MedicalPractitioner::find(2)->assignRole('medical-practitioner');
        // Patient::find(3)->assignRole('patient');
        User::find(2)->assignRole('medical-practitioner');
        User::find(3)->assignRole('patient');

        //dd(User::count());
        foreach(range(4, User::count()) as $index) {
            $role = Role::inRandomOrder()->first();

            $random = 0;//rand(0,2);

            if ($random == 0) {
                User::find($index)->assignRole($role);
            } else if ($random == 1) {
                MedicalPractitioner::find($index)->assignRole($role);
            } else if ($random == 2) {
                Patient::find($index)->assignRole($role);
            } 

            
        }

    }
}
