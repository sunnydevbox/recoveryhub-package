<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\User;


class UserMetaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $R = new User;
        \DB::table('meta')->delete();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table('meta')->truncate();

        $faker = \Faker\Factory::create();

        $users = User::all();

        foreach($users as $user) {
            $user->addMeta('first_name', $faker->firstName());
            $user->addMeta('last_name', $faker->lastName());
            $user->save();
        }
    }
}
