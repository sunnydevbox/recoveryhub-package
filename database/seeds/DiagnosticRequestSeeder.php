<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\DiagnosticRequest;

class DiagnosticRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
            '24 hour Urine Test',
            'Bacteriology',
            'Chemistry',
            'Endocrine',
            'Enzymes',
            'Hematology',
            'Hepatitis Profile',
            'Immunology/Tumor Markers',
            'Pregnancy Test',
            'Psychological Test',
            'Serology',
            'Stool',
            'Therapeutic Assay',
            'Urinalysis',
            'Ultrasound',
            'Miscellaneous'
        ];


        foreach ($attributes as $attribute) {

            DiagnosticRequest::create([
                'name' => $attribute
            ]);
        }
    }
}
