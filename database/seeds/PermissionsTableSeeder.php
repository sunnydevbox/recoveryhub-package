<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\TWUser\Models\Permission;


class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $R = new Permission;
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        \DB::table($R->getTable())->truncate();
        
        $roles = [
        	'can_access_admin',
        	'can_create_user',
            'can_read_user',
            'can_update_user',
        	'can_delete_user',
        ];


        foreach($roles as $role) {

        	Permission::create([
        		'name' 	=> $role,
        		'guard_name'	=> 'web',
        	]);
        }

    }
}
