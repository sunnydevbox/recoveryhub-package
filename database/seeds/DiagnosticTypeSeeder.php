<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;
use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\DiagnosticType;

class DiagnosticTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
            1 => [
                ['17 Hydroxycorticoid' => null],
                ['17 Ketosteroid' => null],
                ['Amylase' => null],
                ['Calcium (Ca)' => null],
                ['Creatinine Clearance' => null],
                ['Glucose' => null],
                ['Phosphorus' => null],
                ['Potassium (K)' => null],
                ['Protein' => null],
                ['Sodium (Na)' => null],
                ['Uric Acid' => null],
                ['VMA' => null]
            ],
            2 => [
                ['AFB Stain' => null],
                ['Blood C/S with AR' => null],
                ['Culture and Sensitivity' => null],
                ['Gram Stain' => null],
                ['KOH' => null]
            ],
            3 => [
                ['Ammonia' => null],
                ['BUN' => null],
                ['Chloride (Cl)' => null],
                ['CO2' => null],
                ['Creatinine' => null],
                ['Ferritin' => null],
                ['Fructosamine' => null],
                ['Glucose' => 1],
                ['Glucose' => 2],
                ['Glucose' => 3],
                ['Glucose' => 4],
                ['HbA1C' => null],
                ['Inorg. Phosphorus' => null],
                ['Ionized Calcium (Ca)' => null],
                ['Lipoprotein Profile (HDL, LDL, VDRL, HDL Ratio)' => null],
                ['Lithium' => null],
                ['Magnesium (Mg)' => null],
                ['Potassium (K)' => null],
                ['Serum Insulin' => null],
                ['Serum Iron' => null],
                ['Serum Lithium Determination' => null],
                ['Sodium (Na)' => null],
                ['TIBC' => null],
                ['Total Bilirubin (B1, B2)' => null],
                ['Total Calcium (Ca)' => null],
                ['Total Cholesterol' => null],
                ['Total Protein (Albumin, Globulin, A/G )' => null],
                ['Triglyceride' => null],
                ['Uric Acid' => null],
                ['Valproic Acid Assay' => null]
            ],
            4 => [
                ['FSH' => null],
                ['FT3' => null],
                ['FT4' => null],
                ['LH' => null],
                ['T3' => null],
                ['T4' => null],
                ['TSH' => null]
            ],
            5 => [
                ['Acid Phosphatase' => null],
                ['Alkaline Phosphatase' => null],
                ['Amylase' => null],
                ['CPK' => null],
                ['CPK-MB' => null],
                ['G6PD' => null],
                ['GTPP' => null],
                ['LDH' => null],
                ['Lipase' => null],
                ['Prostatic Acid Phosphatase' => null],
                ['SGOT (AST)' => null],
                ['SGPT (ALT)' => null]
            ],
            6 => [
                ['Blood type' => null],
                ['BT & CT' => null],
                ['CBC' => null],
                ['Eosinophils' => null],
                ['ESR' => null],
                ['HB & Hct' => null],
                ['Malarial Smear' => null],
                ['Platelet Ct' => null],
                ['Prothrombin' => null],
                ['PTT' => null],
                ['Reticulocyte Ct' => null],
                ['WBC and Diff Ct' => null]
            ],
            7 => [
                ['Anti HAV (IgG)' => null],
                ['Anti HAV (IgM)' => null],
                ['Anti HBc (IgG)' => null],
                ['Anti HBc (IgM)' => null],
                ['Anti HCV' => null],
                ['Anti HBe' => null],
                ['Anti HBs' => null],
                ['Complete Hepatitis Profile' => null],
                ['HBeAg' => null],
                ['HBsAG' => null],
                ['HBV DNA' => null],
                ['Hepa Delta' => null]
            ],
            8 => [
                ['Alphafetoprotein' => null],
                ['CEA' => null],
                ['HIV (AIDS)' => null]
            ],
            9 => [
                ['Stat Pack' => null]
            ],
            10 => [
                ['Complete' => null],
                ['IQ' => null]
            ],
            11 => [
                ['VDRL (Quantitative)' => null],
                ['VDRL (Qualitative)' => null]
            ],
            12 => [
                ['Occult Blood' => null],
                ['Routine' => null]
            ],
            13 => [
                ['Carbamazepine' => null],
                ['Digoxin' => null],
                ['Ethanol' => null],
                ['Phenobarbital' => null],
                ['Phenytoin' => null],
                ['Theophyline' => null]
            ],
            14 => [
                ['Bence Jones Protein' => null],
                ['Bile' => null],
                ['Ketone' => null],
                ['Microalbumin' => null],
                ['Routine' => null],
                ['Urobilinogen' => null]
            ],
            15 => [
                ['Transrectal UTZ' => null],
                ['Tranvaginal UTZ' => null],
                ['UTZ of Kidneys, Ureter, & Bladder' => null],
                ['UTZ of Liver, Pancreas, & Biliary Tree' => null],
                ['UTZ of Scrotum with Doppler' => null],
                ['UTZ of Whole Abdomen' => null]
            ],
            16 => [
                ['2D Echo' => null],
                ['ECG' => null],
                ['Meth/THC Drug Test' => null],
                ['Semen Analysis' => null],
                ['Stone Analysis' => null],
                ['Treadmill Stress Test' => null]
            ]
        ];

        foreach ($attributes as $key => $attribute) {

            foreach ($attribute as $attrKey => $value) {

                DiagnosticType::create([
                    'name' => key($value),
                    'diagnostic_request_id' => $key,
                    'diagnostic_test_id' => $value[key($value)]
                ]);

            }

        }

    }
}
