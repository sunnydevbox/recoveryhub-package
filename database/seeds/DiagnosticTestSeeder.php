<?php
namespace Sunnydevbox\Recoveryhub\Database\Seeds;

use Illuminate\Database\Seeder;

use Sunnydevbox\Recoveryhub\Models\DiagnosticTest;

class DiagnosticTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = [
            'FBS',
            'RBS',
            '2 HR Post Prandial',
            'OGTT'
        ];


        foreach ($attributes as $attribute) {

            DiagnosticTest::create([
                'name' => $attribute
            ]);
        }
    }
}
