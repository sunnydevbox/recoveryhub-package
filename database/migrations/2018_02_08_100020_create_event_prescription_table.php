<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventPrescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('medicine_generic_name', function($table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
        });


        Schema::create('medicine_inventory', function($table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('dosage')->nullable();
            $table->string('unit')->nullable();
            $table->string('remarks')->nullable();

            $table->integer('generic_name_id')->unsigned();

            $table->index('generic_name_id');
            $table->foreign('generic_name_id')
                ->references('id')
                ->on('medicine_generic_name')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::create('event_prescriptions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('event_id')->unsigned();
            
            $table->text('notes')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->index('event_id');
            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::create('event_prescription_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('event_prescription_id')->unsigned();
            $table->integer('medicine_id')->unsigned();

            $table->string('qty_tabs_dispensed')->nullable();
            $table->string('dosage')->nullable();
            $table->text('instructions')->nullable();


            $table->timestamps();

            $table->index('event_prescription_id');
            $table->foreign('event_prescription_id')
                ->references('id')
                ->on('event_prescriptions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index('medicine_id');
            $table->foreign('medicine_id')
                ->references('id')
                ->on('medicine_inventory')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_prescription_items', function($table) {
            $table->dropForeign('event_prescription_items_event_prescription_id_foreign');
            $table->dropIndex('event_prescription_items_event_prescription_id_index');
            
            $table->dropForeign('event_prescription_items_medicine_id_foreign');
            $table->dropIndex('event_prescription_items_medicine_id_index');
        });
        Schema::dropIfExists('event_prescription_items');


        Schema::table('medicine_inventory', function($table) {
            $table->dropForeign('medicine_inventory_generic_name_id_foreign');
            $table->dropIndex('medicine_inventory_generic_name_id_index');
        });
        Schema::dropIfExists('medicine_inventory');
        Schema::dropIfExists('medicine_generic_name');

        Schema::table('event_prescriptions', function($table) {
            $table->dropForeign('event_prescriptions_event_id_foreign');
            $table->dropIndex('event_prescriptions_event_id_index');
        });

        Schema::dropIfExists('event_prescriptions');
    }
}
