<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpentokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('opentok', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            
            $table->integer('event_id')->unsigned();
            $table->string('session');
            $table->text('token');
            $table->string('status')->default('pending');
            $table->timestamps();
            $table->softDeletes();

            $table->index('event_id');

            $table->foreign('event_id')
                ->references('id')
                ->on('events');
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opentok', function (Blueprint $table) {
            $table->dropForeign('opentok_event_id_foreign');
            $table->dropIndex('opentok_event_id_index');
        }); 

        Schema::dropIfExists('opentok');


    }
}
