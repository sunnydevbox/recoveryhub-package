<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_notes', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            // User ID of the owner
            $table->integer('event_id')->unsigned();

            $table->text('notes');
            
            $table->timestamps();
            $table->softDeletes();

            $table->index('event_id');
            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onDelete('cascade'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        // BUSINESS ROLES 
        Schema::table('event_notes', function($table) {
            $table->dropForeign('event_notes_event_id_foreign');
            $table->dropIndex('event_notes_event_id_index');
        });

        Schema::dropIfExists('event_notes');
    }
}
