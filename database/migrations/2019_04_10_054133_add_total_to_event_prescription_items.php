<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalToEventPrescriptionItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('event_prescription_items', function (Blueprint $table) {
            $table->integer('total_qty_purchased_complete')->default(0);
            $table->integer('total_qty_purchased_pending')->default(0);
            $table->integer('total_qty_purchased')->default(0);
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_prescription_items', function (Blueprint $table) {
            $table->dropColumn('total_qty_purchased_complete');
            $table->dropColumn('total_qty_purchased_pending');
            $table->dropColumn('total_qty_purchased');
        }); 
    }
}
