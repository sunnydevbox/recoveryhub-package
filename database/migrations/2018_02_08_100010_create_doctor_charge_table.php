<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorChargeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_charges', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->integer('doctor_id')->unsigned();
            $table->decimal('default', 8, 2)->default(500);
            $table->boolean('use_default')->default(true);
            $table->decimal('monday', 8, 2)->default(500);
            $table->decimal('tuesday', 8, 2)->default(500);
            $table->decimal('wednesday', 8, 2)->default(500);
            $table->decimal('thursday', 8, 2)->default(500);
            $table->decimal('friday', 8, 2)->default(500);
            $table->decimal('saturday', 8, 2)->default(500);
            $table->decimal('sunday', 8, 2)->default(500);

            $table->timestamps();

            $table->index('doctor_id');
            $table->foreign('doctor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rate_charges', function($table) {
            $table->dropForeign('rate_charges_doctor_id_foreign');
            $table->dropIndex('rate_charges_doctor_id_index');
        });

        Schema::dropIfExists('rate_charges');
    }
}
