<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();

            $table->text('question');
            $table->text('settings')->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('weight')->default(0);
            $table->boolean('status')->default(false); // 1 or 0
        });

        Schema::create('profile_question_answers', function($table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('question_id')->unsigned();
            $table->text('answer')->nullable();

            $table->timestamps();

            $table->index('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->index('question_id');
            $table->foreign('question_id')
                ->references('id')
                ->on('profile_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_question_answers', function($table) {
            $table->dropForeign('profile_question_answers_user_id_foreign');
            $table->dropIndex('profile_question_answers_user_id_index');

            $table->dropForeign('profile_question_answers_question_id_foreign');
            $table->dropIndex('profile_question_answers_question_id_index');
        });

        Schema::dropIfExists('profile_question_answers');
        Schema::dropIfExists('profile_questions');
    }
}
