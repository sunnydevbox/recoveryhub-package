<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Feedbacks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->unsignedInteger('user_id');

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            
            $table->text('comment')->nullable();
            $table->text('history_of_present_illness')->nullable();
            $table->text('brief_summary_of_positive_aspects')->nullable();
            $table->text('symptom_information')->nullable();
            $table->text('developmental_history')->nullable();
            $table->text('past_medical_psychiatric_history')->nullable();
            $table->text('family_history')->nullable();
            $table->text('medication_history_current_medications')->nullable();
            $table->text('abuses_trauma')->nullable();
            $table->text('mental_status_examination')->nullable();
            $table->text('physical_appearance')->nullable();
            $table->text('behavior')->nullable();
            $table->text('speech')->nullable();
            $table->text('mood')->nullable();
            $table->text('affect')->nullable();
            $table->text('thoughts')->nullable();
            $table->text('suicidal_or_homicidal_intent_present')->nullable();
            $table->text('psychosis')->nullable();
            $table->text('cognition')->nullable();
            $table->text('insight')->nullable();
            $table->text('impression_and_codes')->nullable();
            $table->text('psychiatric_diagnosis')->nullable();
            $table->text('pedical_diagnosis')->nullable();
            $table->text('self_harmViolence_potential')->nullable();
            $table->text('treatment_recommendations')->nullable();
            $table->text('lifestyle')->nullable();
            $table->text('recommended_medication_changes')->nullable();
            $table->text('discuss_cautionsSide_effects_for_use_of_medication')->nullable();
            $table->text('referrals_required')->nullable();
            $table->text('psychotherapy')->nullable();
            $table->text('psycho_education')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_feedbacks');
    }
}
