<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionNumberToPrescriptionTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('prescription_transactions', function (Blueprint $table) {
            $table->string('transaction_number')->nullable();
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prescription_transactions', function (Blueprint $table) {
            $table->dropColumn('transaction_number');
        }); 


    }
}
