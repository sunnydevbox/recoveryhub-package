<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnostic_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();

            $table->integer('diagnostic_request_id')->unsigned()->nullable();
            $table->index('diagnostic_request_id');
            $table->foreign('diagnostic_request_id')->references('id')->on('diagnostic_request')->onDelete('cascade');

            $table->integer('diagnostic_test_id')->unsigned()->nullable();
            $table->index('diagnostic_test_id');
            $table->foreign('diagnostic_test_id')->references('id')->on('diagnostic_test')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        // DIAGNOSTIC REQUEST
        Schema::table('diagnostic_type', function ($table) {
            $table->dropForeign('diagnostic_type_diagnostic_request_id_foreign');
            $table->dropIndex('diagnostic_type_diagnostic_request_id_index');
        });

        // DIAGNOSTIC TEST
        Schema::table('diagnostic_type', function ($table) {
            $table->dropForeign('diagnostic_type_diagnostic_test_id_foreign');
            $table->dropIndex('diagnostic_type_diagnostic_test_id_index');
        });

        Schema::dropIfExists('diagnostic_type');
    }
}
