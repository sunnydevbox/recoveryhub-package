<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldToCartTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('cart_transactions', function (Blueprint $table) {
            $table->string('notes')->nullable();
            $table->string('status')->nullable()->default('pending');
            $table->string('received_by')->nullable();
            $table->dateTime('date_received')->nullable();
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_transactions', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->dropColumn('status');
            $table->dropColumn('received_by');
            $table->dropColumn('date_received');
        }); 


    }
}
