<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagToEventPrescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('event_prescriptions', function (Blueprint $table) {
            $table->enum('flag', ['send-to-email', 'deliver-to-lifeport'])->default('send-to-email');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_prescriptions', function (Blueprint $table) {
            $table->dropColumn('flag');
        }); 
    }
}
