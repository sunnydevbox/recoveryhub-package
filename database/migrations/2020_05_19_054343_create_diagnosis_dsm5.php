<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateDiagnosisDsm5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosis_dsm5', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('label');
            $table->boolean('status'); // 0 - inactive; 1 - active
            NestedSet::columns($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnosis_dsm5', function (Blueprint $table) {
            NestedSet::dropColumns($table);
        }); 

        Schema::dropIfExist('diagnosis_dsm5');
    }
}
