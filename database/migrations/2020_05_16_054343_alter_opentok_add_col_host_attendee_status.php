<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOpentokAddColHostAttendeeStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('opentok', function (Blueprint $table) {
            // DROP TOKEN FIELD BECAUSE THIS IS 
            // UNIQUE EVERYTIME THE USER CONNECTS 
            // TO THE SESSION
            $table->dropColumn(['token']);

            // $table->string('host_status')->default('OFFLINE')->nullable();
            // $table->string('attendee_status')->default('OFFLINE')->nullable();
            $table->tinyInteger('host_status')->default(0)->nullable();
            $table->tinyInteger('attendee_status')->default(0)->nullable();
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opentok', function (Blueprint $table) {
            $table->text('token')->nullable();
            $table->dropColumn(['host_status', 'attendee_status']);
        }); 
    }
}
