<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::table('event_feedbacks', function (Blueprint $table) {
            $table->string('general_data')->nullable();
            $table->string('course_in_the_ward_out_patient')->nullable();
            $table->string('laboratory_examination_result')->nullable();
            $table->string('psychological_examination_result')->nullable();
            $table->string('diagnosis')->nullable();
            $table->string('medication')->nullable();
            $table->string('recommendation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_feedbacks', function (Blueprint $table) {
            $table->dropColumn('general_data');
            $table->dropColumn('course_in_the_ward_out_patient');
            $table->dropColumn('laboratory_examination_result');
            $table->dropColumn('psychological_examination_result');
            $table->dropColumn('diagnosis');
            $table->dropColumn('medication');
            $table->dropColumn('recommendation');
        }); 
    }
}
