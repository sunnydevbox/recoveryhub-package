<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddS2ColMedicine extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('medicine_inventory', function (Blueprint $table) {
            $table->tinyInteger('is_s2')->default('0')->nullable();
            $table->tinyInteger('is_injectable')->default('0')->nullable();
            $table->tinyInteger('is_active')->default('0')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medicine_inventory', function (Blueprint $table) {
            $table->dropColumn(['is_s2', 'is_injectable', 'is_active']);
        });
    }
}
