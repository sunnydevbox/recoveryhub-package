<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateOpentokLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opentok_logs', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->bigInteger('event_id');
            $table->bigInteger('user_id')->nullable();
            $table->string('name');
            $table->text('detail');
            $table->datetime('event_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opentok_logs');
    }
}
