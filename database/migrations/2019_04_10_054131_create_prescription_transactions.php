<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptionTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prescription_transactions', function (Blueprint $table) {
            
            $table->increments('id');

            $table->bigInteger('cart_id')->unsigned()->nullable();
            $table->index('cart_id');
            $table->foreign('cart_id')->references('id')->on('cart_items')->onDelete('cascade');

            $table->integer('prescription_item_id')->unsigned()->nullable();
            $table->index('prescription_item_id');
            $table->foreign('prescription_item_id')->references('id')->on('event_prescription_items')->onDelete('cascade');


            $table->integer('quantity_ordered');

            $table->float('price')->default('0');
            $table->string('sku')->nullable();

            $table->enum('status', ['pending', 'processing', 'prepared' ,'complete', 'failed', 'cancelled'])->default('pending');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prescription_transactions');
    }
}
