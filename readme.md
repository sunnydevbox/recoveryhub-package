# RecoveryHub API
ReoveryHub API. Video conferencing for doctors and patients.


# Installation
1) Install and configure a fresh instance of Laravel 5.4.x
```sh
$ composer create-project laravel/laravel="5.4.*" folder-name
```
2) composer.json: Include this in the repositories section
```javascript
"minimum-stability": "dev",
"repositories": [
    ...
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-core.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-user.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-events.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/recoveryhub-package.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-bookings.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-cart.git"
        }
        ...
    ]
```

3) Composer: Require the recoveryhub package
```sh
$ composer require sunnydevbox/recoveryhub:dev-master
```

4) Update config/app.php. Add these under 'providers'
```ssh
Sunnydevbox\Recoveryhub\RecoveryhubServiceProvider::class,
```

5) Update
```sh
$ composer dumpautoload
```

# Installation

1) Remove all migration files from database/migrations

2) Configure database settings in .env file
```ssh
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

3) Publish ReoveryHub initial migration files
```sh
$ php artisan recoveryhub:publish-migration 
```

4) Manage ReoveryHub migrations
```sh
$ php artisan recoveryhub:migrate --action=[run/reset/refresh/rollback]
```

5) Run Laravel Shop's migrations and follow the on-screen instructions
```sh
$ php artisan laravel-shop:migration 
```
6) Add 'users' to the missing table name in Laravel shop's migration file.

7) Run migrate and seeder to migrate and seed Laravel Shop's tables
```ssh
$ php artisan migrate
$ composer dumpautoload
$ php artisan db:seed --class=LaravelShopSeeder
```

8) Read https://github.com/barryvdh/laravel-snappy
```ssh
php artisan vendor:publish --provider="Barryvdh\Snappy\ServiceProvider"
```

6) Configure .env
```javascript
API_NAME="MetroPysch API"
API_STRICT=true
API_VERSION=v1
API_PREFIX=api
API_DEFAULT_FORMAT=json
API_DEBUG=true
DOMAIN=metropsych-api.dev
API_SUBTYPE=metropsych

//////////////////////////////////
// OPTIONAL FOR development use //
// Note: these values could     //
//       change anytime without //
//       notice                 //
//////////////////////////////////
MAIL_DRIVER=mailgun
MAIL_HOST=smtp.mailgun.org
MAIL_PORT=2525
MAIL_USERNAME=postmaster@api.recoveryhub.world
MAIL_PASSWORD=c19ea7a60d9965b334f422e92f57be2f-833f99c3-7e109255
MAIL_ENCRYPTION=null

MAIL_FROM_ADDRESS=team.recoveryhub.ph
MAILGUN_DOMAIN=api.recoveryhub.world
MAILGUN_SECRET=key-685f161bc452fb1591bc805dd31c9faf

```

7) Empty all route files in root directory

8) Add Altpay Gateway to config/shop.php
```javascript
'gateways' => [
    ...
    'altpay' => Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\AltpayGateway::class,
    ...
]
```

9) Update config/api.php
```javascript
'middleware' => [
        \Barryvdh\Cors\HandleCors::class,
    ],
'auth' => [
        'jwt' => 'Dingo\Api\Auth\Provider\JWT',
    ],
```

10) Update config/jwt.php
```javascript
'user' => \Sunnydevbox\Recoveryhub\Models\User::class,
```
11) Update config/auth.php
```javascript
'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => \Sunnydevbox\Recoveryhub\Models\User::class,
        ],
],
```

8) Clear all cache
```sh
$ php artisan twcore:optimize
```

# Server Configuration
Coming soon

# Queue Configuration
1) Install supervisor
    > sudo yum install supervisor
    
    or
    > sudo apt-get install supervisor

2) Follow instructions on how to build conf file
    https://laravel.com/docs/5.7/queues#supervisor-configuration
    




