<?php

namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;

class ProfileQuestionService
{
    public function answer($data)
    {
        $data = $this->sanitizeRequest($data);

        $result = $this->rpoUserProfileQuestionAnswer->updateOrCreate(
            ['question_id' => $data['question_id'], 'user_id' => $data['user_id']], $data);
        
        return $result;
    }


    public function sanitizeRequest($requesData)
    {
        $user = JWTAuth::parseToken()->authenticate();

        $data = [
            'question_id' => $requesData['question_id'], 
            'user_id' => $user->id, // Get current Patient object
            'answer' => serialize([
                'value' => $requesData['answer']
            ])
        ];

        return $data;
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion\ProfileQuestionRepository $rpoProfileQuestion,
        \Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion\UserProfileQuestionAnswerRepository $rpoUserProfileQuestionAnswer
    ) {
        $this->rpoProfileQuestion = $rpoProfileQuestion;
        $this->rpoUserProfileQuestionAnswer = $rpoUserProfileQuestionAnswer;
    }
}
