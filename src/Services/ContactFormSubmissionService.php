<?php
namespace Sunnydevbox\Recoveryhub\Services;

class ContactFormSubmissionService
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\ContactFormSubmission\ContactFormSubmissionRepository $repository,
        \Sunnydevbox\Recoveryhub\Validators\ContactFormSubmissionValidator $validator
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function store($data)
    {
        if(empty($data)){
            return collect();
        }

        $attibutes = [
            'subject' => 'New Notification',
            'form_data' => $data
        ];

        isset($data['message']) ? $attibutes['contents'] = $data['message'] : '';
        isset($data['from']) ? $attibutes['from'] = $data['from'] : '';
        
        $result = $this->repository->create($attibutes);
        return $result;
    }

}