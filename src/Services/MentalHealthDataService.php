<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Auth;

class MentalHealthDataService
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\MentalHealthData\MentalHealthDataRepository $mentalHealthDataRepository,
        \Sunnydevbox\Recoveryhub\Validators\MentalHealthDataValidator $mentalHealthDataValidator,
        \Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $userRepository
    ) {
        $this->mentalHealthDataRepository = $mentalHealthDataRepository;
        $this->mentalHealthDataValidator = $mentalHealthDataValidator;
        $this->userRepository = $userRepository;
    }

    public function store(array $data)
    {
        $mentalData = $this->createMentalHealthData($data);
        $userId = $mentalData->user_id;
        $userInfo = $this->updateUser($data, $userId);
        $mentalData['generalInformation'] = $this->userRepository->formatGeneralInformation($userInfo->getAllMeta()->toArray(), true);
        return $mentalData;
    }

    public function update(array $data, $id)
    {
        $mentalData = $this->updateMentalHealthData($data, $id);

        $userId = $mentalData->user_id;
        $userInfo = $this->updateUser($data, $userId);

        $mentalData['generalInformation'] = $this->userRepository->formatGeneralInformation($userInfo->getAllMeta()->toArray(), true);
        return $mentalData;
    }

    public function updateByUserId(array $data, $id)
    {
        $mentalData = $this->updateMentalHealthData($data, $id);
        $userId = $mentalData->user_id;
        $userInfo = $this->updateUser($data, $userId);

        $mentalData['generalInformation'] = $this->userRepository->formatGeneralInformation($userInfo->getAllMeta()->toArray(), true);
        return $mentalData;
    }

    public function updateUser(array $data, $userId)
    {
        $generalInformation = $this->userRepository->formatGeneralInformation($data['generalInformation']);
        $result = $this->userRepository->update($generalInformation, $userId);

        return $result;

    }

    public function createMentalHealthData(array $data)
    {
        $patientData = $this->mentalHealthDataRepository->formatAttributes($data);

        $mentalHealthData = [
            'user_id' => Auth::user()->id,
            'patient_data' => $patientData
        ];

        $patientData = $this->mentalHealthDataRepository->updateOrCreate(
            [
                'user_id' => Auth::user()->id
            ],
            $mentalHealthData
        );

        return $patientData;
    }

    public function updateMentalHealthData(array $data, $id)
    {
        $patientData = $this->mentalHealthDataRepository->formatAttributes($data);
        $mentalHealthData = [
            'user_id' => Auth::user()->id,
            'patient_data' => $patientData
        ];

        $result = $this->mentalHealthDataRepository->update($mentalHealthData, $id);

        return $result;
    }

    public function getByUserId($id)
    {
        $userData = $this->userRepository->find($id);
        $patientData = $this->mentalHealthDataRepository->getByUserId($id);
        if ($patientData) {
            $patientData["generalInformation"] = $this->userRepository->formatGeneralInformation($userData->getAllMeta()->toArray(), true);
        }
        return $patientData;
    }

    public function show($id)
    {
        $patientData = $this->mentalHealthDataRepository->find($id);
        $userData = $this->userRepository->find($patientData->user_id);
        if ($userData) {
            $patientData["generalInformation"] = $this->userRepository->formatGeneralInformation($userData->getAllMeta()->toArray(), true);
        }
        return $patientData;
    }

}