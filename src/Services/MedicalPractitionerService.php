<?php

namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;

class MedicalPractitionerService
{
    public function store($data)
    {
        // CHECK IF Current user is the owner of the event
        
        $user = $this->rpoMedicalPractitioner->create($data);

        if (isset($data['doctorRole'])) {
            $user->assignRole($data['doctorRole']);
        }

        // SET TO ACTIVE
        $user->status = 'active';
        $user->is_verified = \Carbon\Carbon::now();
        $user->save();
        return $user;
    }

    public function updateRoles($doctorRole, $user){
    
        // LOOP TO BE REMOVED AFTER FIXING REMOVE 
        foreach($user->roles as $role){
            $user->removeRole($role);
        }

        $newRole = strtolower($doctorRole);

        if (!$user->hasRole($newRole)) {
            $currentRole = ( trim(strtolower($newRole)) == 'medical-practitioner') ? 'psychologist' : 'medical-practitioner'; 
            $user->removeRole($currentRole);
            $user->assignRole($newRole);
        }
         
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\MedicalPractitioner\MedicalPractitionerRepository $rpoMedicalPractitioner
    ) {
        $this->rpoMedicalPractitioner = $rpoMedicalPractitioner;
    }
}
