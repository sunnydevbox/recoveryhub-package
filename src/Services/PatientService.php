<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\Recoveryhub\Events\EventSendEmailEvent;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class PatientService
{
    public function store($data)
    {
        

        try {
            if ($data['role'] == 'in-patient') {
                  
                // 1. Generate fake unique email
                $faker = \Faker\Factory::create();
                $dummyEmail = "DUMMY.{$faker->userName}@DUMMY.{$faker->domainName}";

                $data['email'] = $dummyEmail;

                if (isset($this->validator) && $this->validator) {
                    $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
                }

                // 2. Store
                $user = $this->repository->create($data);

                // 3. Update with temp unique email
                $user->email = "inpatient_{$user->id}@recoveryhub.com";
                $user->status = 'inactive';
                $user->update();

                // 4. assign role
                $user->assignRole(['in-patient']);

                return $user;
            } else {
                $data['status'] = 'inactive';
                // dd($data);
                if (isset($this->validator) && $this->validator) {
                	$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
                }
                
                $user = $this->repository->create( $data );

                $user->assignRole(['patient']);
                
                return $user;
            }

        } catch (ValidatorException $e) {
            throw $e;
        }
    }    

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Patient\PatientRepository $repository,
        \Sunnydevbox\Recoveryhub\Validators\PatientValidator $validator
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
    }
}