<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\TWCore\Services\SupplementDataService as SuppdataService;
use Sunnydevbox\Recoveryhub\Events\EventSendEmailEvent;

class ReportService extends SuppdataService
{
    protected $configPDF = [
        'size' => 'A6',    
    ];


    public function generate($data)
    {
        $reportType = isset($data['reportType']) ? $data['reportType']  : 'pdf';
        
        $result = $this->reportFactory->build($data['type'], $data);

        if($result['template'] == 'abstract-reports') {
            $this->configPDF['size'] = 'A4';
        }

        return $this->{$reportType}($result['data'], $result['template']);
    }

    public function pdf($data, $template)
    {
        $patientAddress = [];
        $patient = $data['notes']->event->bookings->patient;

        if ( $patient->perm_no_street ) { array_push($patientAddress, $patient->perm_no_street); }
        if ( $patient->perm_barangay ) { array_push($patientAddress, $patient->perm_barangay); }
        if ( $patient->perm_province ) { array_push($patientAddress, $patient->perm_province); }
        if ( $patient->perm_city_municipality ) { array_push($patientAddress, $patient->perm_city_municipality); }
        if ( $patient->perm_zip_code ) { array_push($patientAddress, $patient->perm_zip_code); }

        $data['patientAddress'] = implode (", ", $patientAddress);
        
        $pdf = \PDF::loadView('recoveryhub::pdf.' . $template, $data)
            ->setPaper($this->configPDF['size'])
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0);

        $id = isset($data['id']) ? $data['id'] : uniqid('un-');
        
        $location = config('recoveryhub.prescription_pdf_location') . $template . '-' . $id . '.pdf';
        $pdf->save($location, true);
        
        \Log::info('report_gen_' . $template . '_' . $id);

        return $location;
    }

    public function setConfig($params = [])
    {
       array_merge($this->configPDF, $params);

       return $this;
    }

    public function sendEmailPdf($data)
    {
        $resEvent = $this->eventRepository->skipCriteria()->find($data["event_id"]);
        $attachment = $this->generate($data);

        event(new EventSendEmailEvent($resEvent, $data, $attachment));

        return $data;
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Factories\ReportFactory $reportFactory,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $eventRepository
    )
    {
        $this->reportFactory = $reportFactory;
        $this->eventRepository = $eventRepository;
    }
}