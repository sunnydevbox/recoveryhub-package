<?php
namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;
use Sunnydevbox\Recoveryhub\Repositories\Tokbox\TokboxCallbackRepository as rpoTokboxCallback;

class TokboxService
{
    private $rpoTokboxCallback;
    private $rpoEventNote;

    public function sanitizeRequest($requesData)
    {
        $data = [
            'session_id'    => $requesData->get('sessionId', null),
            'project_id'    => $requesData->get('projectId', null),
            'event'         => $requesData->get('event', null),
            'reason'        => $requesData->get('reason', null),
            'timestamp'      => $requesData->get('timestamp', null),
            'reason'        => $requesData->get('reason', null),
            'connection'    => $requesData->get('connection', null),
            'stream'        => $requesData->get('stream', null),
        ];

        return $data;
    }

    public function store($data)
	{
		$result = $this->rpoTokboxCallback->create( $data );
        
        return $result;
	}

    public function __construct(
        rpoTokboxCallback $rpoTokboxCallback
    ) {
        $this->rpoTokboxCallback = $rpoTokboxCallback;
    }
}
