<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Illuminate\Support\Facades\Storage;
use Sunnydevbox\Recoveryhub\Events\EventPrescriptionCreatedEvent;
use Illuminate\Support\Facades\View;
use JWTAuth;
use File;
use Response;
use Sunnydevbox\Recoveryhub\Events\SendLifePortPrescriptionEvent;


class EventPrescriptionService
{
    private $rpoEvent;
    private $rpoEventNote;
    protected $filename;

    public function store($data)
    {
        $this->rpoEvent->skipCriteria()->find($data['event_id']);

        $prescription = $this->rpoEventPrescription->updateOrCreate(
            [
                'event_id' => $data['event_id'],
            ],
            $data
        );
        $this->eventPrescriptionItemService->attachItems($prescription, $data['items']);

        $result = $this->rpoEventPrescription->with('items')->find($prescription->id);

        return $result;
    }

    public function update($data, $id)
    {
        $result = $this->store($data);

        return $this->rpoEventPrescription->with('items')->find($id);
    }

    public function preview($data)
    {
        // $prescription = $this->rpoEventPrescription->makeModel()->where('event_id', $data['event_id'])->first();
        $event = $this->rpoEvent->with(['bookings.patient', 'doctor', 'notes', 'prescriptions.items.medicine.generic'])->find($data['event_id']);
        $pdf = $this->generatePrescriptionPDF($event);
        $file = File::get($pdf);
        $response = Response::make($file, 200);
        $response->header('Content-Type', 'application/pdf');
        return $response;
    }

    public function generatePrescriptionPDF($event, $items = null)
    {
        $itemsWithS2 = collect([]);

        $itemWithoutS2 = collect([]);

        $prescribe = collect([]);

        $event->prescriptions->each(function ($prescription) use (&$itemsWithS2, &$itemWithoutS2, &$prescribe) {
                $prescribe = $prescription;
                $itemsWithS2 = $prescription->items()
                                    ->whereHas('s2')
                                    ->with(['medicine'])
                                    ->get();

                $itemWithoutS2 = $prescription->items()
                                    ->whereDoesntHave('s2')
                                    ->with(['medicine'])
                                    ->get();
        });

        
        
        $pages = [];

        if($itemsWithS2->count()) {
            foreach( $itemsWithS2 as $item ) {
                $dataWithS2 = $this->getFormatedItems(collect([$item]), $event, $prescribe, true);
                $pages[] = View::make('recoveryhub::pdf.prescription', $dataWithS2);
            }
        }
        
        if($itemWithoutS2->count()) {
            $dataWithoutS2 = $this->getFormatedItems($itemWithoutS2, $event, $prescribe, false);
            $pages[] = View::make('recoveryhub::pdf.prescription', $dataWithoutS2);
        }
        
        if(!$itemsWithS2->count() && !$itemWithoutS2->count()) {
            $empty = $this->getFormatedItems([], $event, $prescribe, false);
            $pages[] = View::make('recoveryhub::pdf.prescription', $empty);
        }
        
        $pdf = \PDF::loadView('recoveryhub::pdf.multiple-prescription', ['pages' => $pages])
            ->setPaper('A6', '')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ;

        $file = $this->generateFileName($event);
        $pdf->save($file, true);
        return $file;
        
    }

    private function getFormatedItems($items, $doctorEvent, $prescription, $s2)
    {
        $patient = $doctorEvent->bookings->patient;
        $doctor = $doctorEvent->doctor;
        $patientAddress = [];

        if ( $patient->perm_no_street ) { array_push($patientAddress, $patient->perm_no_street); }
        if ( $patient->perm_barangay ) { array_push($patientAddress, $patient->perm_barangay); }
        if ( $patient->perm_province ) { array_push($patientAddress, $patient->perm_province); }
        if ( $patient->perm_city_municipality ) { array_push($patientAddress, $patient->perm_city_municipality); }
        if ( $patient->perm_zip_code ) { array_push($patientAddress, $patient->perm_zip_code); }
        
        $doctorSignature = (isset($doctor->signature)) ? $doctor->signature : null;

        return $data = [
            'hasS2'         => $s2,
            'patient'       => $doctorEvent->bookings->patient,
            'patientAddress'=> implode (", ", $patientAddress),
            'doctor'        => $doctor,
            'doctorSignature' => $doctorSignature,
            'items'         => $items,
            'notes'         => $doctorEvent->notes,
            'prescription'  => $prescription,
            'date'          => isset($doctorEvent->notes['notes']['nextMeeting']) ? $doctorEvent->notes['notes']['nextMeeting'] : '',
        ];
    }

    public function generateFileName($doctorEvent)
    {
        if ($this->filename) {
            $filename = $this->filename;
        } else {
            $filename = 'prescription-' . $doctorEvent->id;
        }
        
        $file = config('recoveryhub.prescription_pdf_location') . $filename . '.pdf';

        return $file;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    public function sendMedication($data)
    {
        $user = JWTAuth::parseToken()->authenticate();
 
        $event = $this->rpoEvent->with(['bookings.patient', 'doctor', 'notes', 'prescriptions.items.medicine.generic'])->find($data['eventId']);

        event(new EventPrescriptionCreatedEvent(
            $event
        ));
        return $event;
    }

    public function updatePrescriptionFlag($request)
    {
        
        if ( !isset($request['flag']) || !isset($request['prescription_id']) ) {
            throw new \Exception("Missing parameter FLAG|ID");
        }

        $eventPrescription = $this->rpoEventPrescription->with(['items.medicine', 'event.bookings.patient', 'event.doctor'])->findWhere(['event_id' => $request['prescription_id']])->first();

        if ( $request['flag'] ) {

            switch ($request['flag']) {
                case 'deliver-to-lifeport' :
                    $eventPrescription->deliverToLifePort();
                    event(new SendLifePortPrescriptionEvent($eventPrescription));
                break;
                default :
                    $eventPrescriptionRpo->sendToEmail();
                break;
            }
            
        }

        return $eventPrescription;
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionRepository $rpoEventPrescription,
        \Sunnydevbox\Recoveryhub\Services\EventPrescriptionItemService $eventPrescriptionItemService,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $rpoEvent,
        \Sunnydevbox\Recoveryhub\Repositories\Feedback\EventFeedbackRepository $eventFeedback
    ) {
        $this->rpoEventPrescription = $rpoEventPrescription;
        $this->eventPrescriptionItemService = $eventPrescriptionItemService;
        $this->rpoEvent = $rpoEvent;
        $this->eventFeedback = $eventFeedback;
    }
}
