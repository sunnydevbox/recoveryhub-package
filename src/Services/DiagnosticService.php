<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\TWCore\Services\SupplementDataService as SuppdataService;

class DiagnosticService extends SuppdataService
{

    public function getList($data)
    {
        $listType = isset($data['type']) ? $data['type'] : 'request';

        $result = $this->diagnosticFactory->build($listType);
        return $result;
    }

    public function __construct(\Sunnydevbox\Recoveryhub\Factories\DiagnosticFactory $diagnosticFactory)
    {
        $this->diagnosticFactory = $diagnosticFactory;
    }
}