<?php
namespace Sunnydevbox\Recoveryhub\Services\Gateways\Altpay;

use Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\Traits\CheckoutFormTrait;
use Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\Traits\CoreTrait;
use Sunnydevbox\TWCart\Core\PaymentGateway;
use Amsgames\LaravelShop\Exceptions\CheckoutException;
use Amsgames\LaravelShop\Exceptions\GatewayException;
use Auth;
use Dingo\Api\Routing\Helpers;
use Sunnydevbox\Recoveryhub\Services\AppointmentService;

class AltpayGateway extends PaymentGateway
{
    use CoreTrait;
    use CheckoutFormTrait;
    use Helpers;

    private $userId = '';
    private $password = '';
    private $entityId = '';


    private $mode = 'test'; // test / live
    private $version = 'v1';

    protected $postData = [];

    public function __construct($id = '')
    {
        parent::__construct($id);
        $this->mode = env('ALTPAY', 'test');
        $this->version = env('ALTPAY_VERSION', 'v1');

        $this->setAuthentication();   
    }

    /**
     * Called by shop to charge order's amount.
     *
     * @param Order $order Order.
     *
     * @return bool
     */
    public function onCharge($order)
    {
        // Set the order to pending.
        $this->statusCode = 'pending';
        
        if ($order->total <= 0) {
            $this->detail = 'Order total price is 0.00; no external transactions required.; ';
            $this->transactionId = uniqid();
            $this->onCallbackSuccess($order);
            return true;
        } else {
            $this->detail = 'Order total price is ' . $order->total . '; no external transactions required.; ';
            $this->transactionId = uniqid();
            $this->onCallbackSuccess($order);
            return true;
        }

        return true;
    }


    /**
     * Called on cart checkout.
     *
     * @param Order $order Order.
     */
    public function onCheckout($cart)
    {
        if ($cart->items->count() <= 0) {
            throw new \Exception('cart_is_empty');
        }

       
        return false;
    }

    /**
     * Called on callback.
     *
     * @param Order $order Order.
     * @param mixed $data  Request input from callback.
     *
     * @return bool
     */
    public function onCallbackSuccess($order, $data = null)
    {
        $this->statusCode     = 'completed';

        $this->detail         .= 'Successful callback; ';
        
        parent::onCallbackSuccess($order, $data);
     
        // create the booking
        $appointmentService = app(\Sunnydevbox\Recoveryhub\Services\AppointmentService::class);

        foreach($order->items as $item) {
            $appointmentService->placeAppointment($item->object);
        }
    }

    /**
     * Called on callback.
     *
     * @param Order $order Order.
     * @param mixed $data  Request input from callback.
     *
     * @return bool
     */
    public function onCallbackFail($order, $data = null)
    {
        $this->detail       = 'failed callback';
        $this->statusCode   = 'failed';
    }


}