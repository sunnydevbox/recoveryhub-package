<?php
namespace Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\Traits;

trait CheckoutFormTrait
{
    public function getCheckoutId($data = [])
    {
        if (!isset($data['amount']) || !isset($data['currency']) || !isset($data['paymentType'])) {
            throw new \Exception('Missing paramters: amount, currency, paymentType');
        }

        $d['amount'] = $data['amount'];
        $d['currency'] = $data['currency'];
        $d['paymentType'] = $data['paymentType'];
        // $data['merchantTransactionId=Test001]
        
        $this->setPostData($d);

        $url = $this->endpoint('checkouts');
        $data =  $this->postData();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        
        $responseData = json_decode($responseData);

        return $responseData->id;
    }


    public function getPaymentStatus($checkoutId)
    {
        $url = $this->endpoint('checkouts/'.$checkoutId.'/payment');
        $url .= '?'.$this->postData();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);

        $responseData = json_decode($responseData);
        return $responseData->result;
    }
}