<?php
namespace Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\Traits;

trait CoreTrait
{
    public function setAuthentication()
    {
        $this->setPostData([
            'authentication.userId'    => '8a829417636ee0060163809ff0081b92',
            'authentication.password'  => 'cmj7B5JQxK',
            'authentication.entityId'  => '8a829417636ee006016380a12d2c1b96',
        ]);
    }

    public function endpoint($endpoint)
    {
        return $this->baseUrl() . $endpoint;
    }

    public function setPostData($data = [])
    {
        if (!empty($data)) {
            $this->postData += $data; 
        }
    }

    public function postData($string = true)
    {
        if ($string) {
            $queryString = [];
            foreach($this->postData as $key => $value) {
                $queryString[] = $key . '=' . $value;
            }

            return join('&', $queryString);
        }

        return $this->postData;
    }

    public function baseUrl()
    {
        //  https://test.oppwa.com/
        // https://oppwa.com/

        if ($this->mode == 'test') {
            $url = 'https://test.oppwa.com/';
        } else {
            $url = 'https://oppwa.com/';
        }

        return $url . $this->version . '/';
    }

}