<?php
namespace Sunnydevbox\Recoveryhub\Services\Gateways\Altpay\Traits;

use Dingo\Api\Http\Request;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository;
use Sunnydevbox\Recoveryhub\Repositories\RateCharge\RateChargeRepository;

trait AltpayControllerTrait
{
    public function getCheckouId(
        Request $request, 
        EventRepository $rpoEvent,
        RateChargeRepository $rpoRateCharge
    ) {
        $rpoEvent->skipCriteria();
        $event = $rpoEvent->with(['bookings.patient'])->find($request->get('event_id'));
        $rate = $rpoRateCharge->getCurrent($event->doctor->id, $event->start_at->dayOfWeek);

        // CHANGE THIS TO DYNAMIC
        $d = [
            'amount'        => $rate,
            'currency'      => 'PHP',
            'paymentType'   => 'DB',
            'first_name'    => $event->bookings[0]->patient->first_name,
            'last_name'     => 'Test',
            'address_1'     => 'Test',
            'address_2'     => 'Test',
            'city'          => 'Test',
            'state'         => 'MM',
            'postcode'      => '1370',
            'country'       => 'PH',
            'email'         => $event->bookings[0]->patient->email,
            'ip'            => $request->ip(),
        ];

        $checkoutId = $this->gateway->getCheckoutId($d);

        return $this->response->array([
            'checkoutId' => $checkoutId,
        ]);
    }

    public function getPaymentStatus($checkoutId)
    {
        $result = $this->gateway->getPaymentStatus($checkoutId);
        
        return $this->response->array([
            'result' => $result,
        ]);
    }
}
