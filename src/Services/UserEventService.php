<?php
namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;

class UserEventService
{
    public function destroy($id)
    {
        try {
            $event = $this->repository->makeModel()->find($id);
            
            if (!$event) {
                throw new \Exception('invalid_schedule', 400);
            }
            
            if ($event->isOpen()) {
                $event->delete();
                return $event;
            } else {
                throw new \Exception('cannot_delete_event', 400);
            }


        } catch(\Exception $e) {
            throw $e;
        }
	}

    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $repository
    ) {
        $this->repository = $repository;
    }
}
