<?php
namespace Sunnydevbox\Recoveryhub\Services;
use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
// use Sunnydevbox\Recoveryhub\Events\CheckoutPrescriptionEvent;
use Sunnydevbox\Recoveryhub\Events\CheckoutPrescriptionEvent;
use JWTAuth;
use \Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use \Sunnydevbox\Recoveryhub\Criteria\MyAppointmentDateRangeCriteria;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DB;

class AppointmentService
{
    public function myAppointments($data)
    {
        $limit = (isset($data['limit']) && is_numeric($data['limit'])) ? $data['limit'] : config('repository.pagination.limit', 15);
        

        if ($limit == 0) {
            $result = $this->rpoAppointment
                        ->popCriteria(DateRangeCriteria::class)
                        ->pushCriteria(MyAppointmentDateRangeCriteria::class)
                        ->with(['bookable'])
                        ->all();

            return $result;
		} else {
            $result = $this->rpoAppointment
                        ->popCriteria(DateRangeCriteria::class)
                        ->pushCriteria(MyAppointmentDateRangeCriteria::class)
                        // ->with(['bookable'])
                        ->paginate($limit)
                        ;
    
            return $result;
		}
    }

    public function myMarkedDates($year)
    {
        $start = Carbon::now()->startOfYear()->format('Y-m-d');
        $end = Carbon::now()->endOfYear()->format('Y-m-d');
        
        $result = $this->myAppointments([
            'limit' => 0,
            'dateRange' => 'events.start_at;' . $start . ';>=|events.end_at;' . $end .';<=',
            'sortDate' => 'asc',
            'with'  => 'event',
        ]);

        $grouped = $result->mapToGroups(function ($item, $key) {
            
            return [$item->bookable->start_at->format('Y-m-d') => [
                'status' => $item->bookable->status,
                'id'    => $item->id
            ]];
        });
        
        return $grouped;
    }

    public function placeAppointment($data)
    {
        
        $result = $data;

        $d = $data;
        $e = config('recoveryhub.models.event');
        $e = new $e;

        if ($data instanceof $e) {
            $d = [
                'event_id' => $data->id,
            ];
            
            $result = $this->rpoAppointment->create($d);            
            event(new AppointmentCreatedEvent($result));
        } 
        // else {

        //     echo 'prescription';
        //     dd($data);
        //     event( new CheckoutPrescriptionEvent($data) ); 
            
            // $result = $data;
        // }

        return $result;
    }


    public function cancel($id)
    {
        try {
            
            $appointment = $this->rpoAppointment->skipCriteria()->find($id);

            $result = DB::transaction(function() use ($appointment) {

                // 1) CHANGE THE EVENT status back to OPEN
                $appointment->event->update([
                    'status' => $appointment->event::STATUS_OPEN
                ]);
                // 2) DELETE THE booking
                $appointment->delete();

                return true;
            });        
        } catch (Exception $e) {
          throw $e;
        } 
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository $rpoAppointment
    ) {
        $this->rpoAppointment = $rpoAppointment;
    }
}