<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\TWCore\Services\SupplementDataService as SuppdataService;

class SupplementDataService extends SuppdataService
{
    public function d($d)
    {
        switch($d) {
            case 'medicine-generic-names':
                
            
                return $this->rpoMedicineGenericName
                            ->with(['medicines' => function($query) {
                                $query->where('is_active', true);
                                $query->select('id', 'generic_name_id', 'name');
                            }])
                            ->orderBy('name', 'asc')->all()->toArray();
            break;

            case 'medicines':

                $dosages = $this->rpoMedicineInventory->makeModel()
                            ->select('id', 'name', 'dosage', 'generic_name_id')
                            ->with(['generic'])
                            ->orderBy('name', 'asc')
                            ->orderBy('dosage', 'asc')
                            ->get(); 

                $dosageList = [];
                foreach($dosages as $dosage) {
                    $key = strtolower((strlen($dosage->name)) ? $dosage->name : $dosage->generic->name);
                    // echo $dosage->id . ' - ' . ' - ' . $key . ' - ' . $dosage->dosage . '<br>';
                    $dosageList[$key][] = $dosage->dosage;
                }

                // dd($dosageList);

                $medicines = $this->rpoMedicineInventory->makeModel()
                    ->select('id', 'name', 'unit', 'dosage', 'generic_name_id')
                    ->with(['generic'])
                    ->orderBy('name', 'asc')
                    ->where('is_active', true)
                    // ->groupBy('name')
                    ->get();

                
                foreach($medicines as $medicine) {
                    $key = strtolower((strlen($medicine->name)) ? $medicine->name : $medicine->generic->name);
                    $medicine->dosages = $dosageList[$key];
                }

                $keyed = $medicines->mapWithKeys(function ($item) {
                    return [$item['id'] => $item];
                });
                
                return $keyed->all();


                // return $medicines->toArray();
            break;

            

            default:
                return [];
            break;
        }
    }

    public function __construct(
        \Sunnydevbox\TWUser\Repositories\Role\RoleRepository $rpoRole,
        \Sunnydevbox\TWUser\Repositories\Permission\PermissionRepository $rpoPermission,

        \Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineInventoryRepository $rpoMedicineInventory,
        \Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineGenericNameRepository $rpoMedicineGenericName
    ) {
        $this->rpoMedicineGenericName           = $rpoMedicineGenericName;
        $this->rpoMedicineInventory             = $rpoMedicineInventory;
        
    }
}