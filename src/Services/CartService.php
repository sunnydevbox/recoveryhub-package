<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\Recoveryhub\Traits\CartTrait;
use Sunnydevbox\TWCart\Services\CartService AS ExtendCartService;
use Sunnydevbox\Recoveryhub\Events\CheckoutPrescriptionEvent;
use Sunnydevbox\Recoveryhub\Events\TransactionStatusChangeEvent;
use Shop;
use Exception;
use Sunnydevbox\Recoveryhub\Models\Cart;
use Amsgames\LaravelShop\Exceptions\CheckoutException;

class CartService extends ExtendCartService
{
    use CartTrait;

    public function cart()
    {
        return Cart::current();
    }

    public function addToCart($data)
    {
        try {
            if ($event = $this->eventRepository->skipCriteria()->find($data['event_id'])) {
                
                // CHECK quantity left? 


                // - CHECK if this event is still available
                if ($event->isExpired()) {
                    throw new Exception('event_time_passed', 400);
                }
                
                if (!$event->isOpen()) {
                    throw new Exception('event_booked', 400);   
                }
                
                // ALWAYS qty 1 for Events
                $quantity = 1;
                $item = $this->addItem($event, $quantity, true);

                if ($item) {
                    return $item;
                }
            }
        } catch(Exception $e) {
            throw $e;
        }
    }

    

    // public function updateTransactionPr($id)
    // {
    //     $res = $this->cartTransactionRepository->find($id);

    //     dd($res);
    // }

    public function checkout()
    {
        if (!$this->cart()->items->count()) {
            throw new \Exception('order_empty');
            return false;
        }

        // if cart total == 0 then use internal gateway onLY
        if ($this->cart()->total == 0) {
            Shop::setGateway('internal');
        }

        return Shop::checkout(); 
    }

    public function placeOrder($data = [])
    {
        if (!$this->cart()->items->count()) {
            throw new \Exception('order_empty');
            return false;
        }

        // Check if any if all of the appointments are still available at this moment
        // then block if they still are availabel
        try {
            $hasBooked = false;
            // else throw error
            $this->cart()->items->each(function($item) use(&$hasBooked) {
                if (get_class($item->object) == \Sunnydevbox\Recoveryhub\Models\Event::class) {
                    if ($item->object->status != $item->object::STATUS_OPEN) {
                        $hasBooked = true;
                    }
                }
            });

            if ($hasBooked) {
                throw new \Exception('has_booked', 400);
            }
        
            
            $this->checkout();
            $exception = Shop::exception();

            if ($exception) {
                // dd('Shop::checkout();', $exception);
                throw $exception;
            }
            $order = Shop::placeOrder();

            if ($order->hasFailed) {
                $exception = Shop::exception();
                throw new Exception('Order not processed', 400); // ->getMessage(); // echos: error
            }

            // NOW start blocking the events if the order status is PENDING
            if ($order->isPending) {
                $order->items->each(function($item) use(&$hasBooked) {
                    if (get_class($item->object) == config('recoveryhub.models.event')) {
                        $item->object->update(['status' => $item->object::STATUS_RESERVED]);
                        $this->appointmentService->placeAppointment($item->object);
                    }
                });
                
                $transaction = $order->transactions[0];
                // update table with transaction number
                $this->prescriptionTransactionService->addTransactionNumber($order, $transaction->transaction_id);
            } else {
                $transaction = $order->transactions[0];
            }
            // call event to send email to lifeport and patient
            // event( new CheckoutPrescriptionEvent( $order) ); 

            return [
                'transaction_id'    => $transaction->transaction_id, 
                'gateway'           => $transaction->gateway,
                'detail'            => $transaction->detail,
                'token'             => $transaction->token,
                'date'              => $transaction->created_at,
                'status'            => $order->statusCode,
                // THIS IS TEMPORARY
            ];
        } catch(CheckoutException $e) {
            throw $e;
        } catch(Exception $e) {
            throw $e;
        }
    }

    public function statusEvent($request, $id)
    {
        $user;
        $res = $this->cartTransactionRepository->with('order')->find($id);

        if ( $res ) {
            $user = $this->userRepository->find($res->order->user_id);

            $data = [
                'transaction'   => $res,
                'user'          => $user,
                'request'       => $request
            ];

            event( new TransactionStatusChangeEvent( $data ) );
        }
        // echo 'here';
        return $res;

    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Services\EventPrescriptionItemService $prescriptionItemService,
        \Sunnydevbox\Recoveryhub\Services\AppointmentService $appointmentService,
        \Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService $prescriptionTransactionService,
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartTransactionRepository $cartTransactionRepository,
        \Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $userRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $eventRepository
    ) {
        $this->prescriptionService = $prescriptionItemService;
        $this->cartTransactionRepository = $cartTransactionRepository;
        $this->prescriptionTransactionService = $prescriptionTransactionService;
        $this->userRepository = $userRepository;
        $this->appointmentService = $appointmentService;
        $this->eventRepository = $eventRepository;
    }
}