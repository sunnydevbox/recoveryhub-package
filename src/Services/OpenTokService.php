<?php

namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;
use Sunnydevbox\Recoveryhub\Repositories\Tokbox\OpenTokRepository;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository;
use Sunnydevbox\Recoveryhub\Repositories\User\UserRepository;
use \Sunnydevbox\Recoveryhub\Validators\OpenTokValidator;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use \Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class OpenTokService
{
    public function store($data)
    {
        try {

            $this->validator->with($data)->passesOrFail('REGISTER_EVENT');

            $result = $this->registerEvent($data['event_id']);
            return $result;

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {
            return [
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ];
        }
    }

    
    public function registerEvent($eventId)
    {
        $session = $this->createSession();
        
        $data = [
            'session'   => $session->getSessionId(),
            'event_id'  => $eventId,
        ];

        $result = $this->repository->create( $data );

        return $result;
    }

    public function getSessionByEvent($eventId)
    {
        // VERIFY first that the event is valid
        $event = $this->rpoEvent->skipCriteria()->with(['bookings'])->find($eventId);
        // 1) CHeck if event has a booking
        if (!$event->isBooked()) {
            throw new Exception('event_missing_booking', 400);
        }
        // 2) CHECK if requestor is authorized;

        $result = $this->repository->findWhere(['event_id' => $eventId])->first();

        if (!$result) {
            $result = $this->registerEvent($eventId);
            
        }

        // $result->token = $this->generateToken($result->session);;
        return $result;
    }

    public function getSessionBySession($sessionId)
    {
        $result = $this->repository->findWhere(['session' => $sessionId])->first();

        return $result;
    }


    public function generateToken($sessionId)
    {
        $token = $this->opentok->generateToken($sessionId);

        return $token;
    }

    public function generateSession($eventId)
    {   
        if ($opentok = $this->getSessionByEvent($eventId)) {
            $session = $this->createSession();

            $opentok->session = $session->getSessionId();
            $opentok->update();

            return $opentok->fresh();
        } else {
            // NOT FOUND
            // 404

            throw new \Exception('not found', 400);
        }

        // return $token;
    }

    public function regenerateToken($eventId)
    {
        /**
         * CHECKPOINTS:
         * 
         * 1. check if token is currently being used
         * 2. ???
         */

        if ($opentok = $this->getSessionByEvent($eventId)) {

            // GRAB the session/session id first
            $sessionId = $opentok->session;
        
            if (!$sessionId) {
                $session = $this->createSession();    
                $sessionid = $session->getSessionId();
            }

            $token = $this->generateToken($sessionId);


            $opentok->update(['session' => $sessionId]);

            $response = $opentok->fresh();//->toArray();
            // dd($response);
            $response['token'] = $token;
            return $response;
        } else {
            // NOT FOUND
            // 404

            throw new \Exception('not_found', 400);
        }
    }

    public function createSession()
    {        
        $session = $this->opentok->createSession([
            'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
        ]);
        
        return $session;
    }

    public function setStatusByUserType($sessionId, $data = [])
    {
        $opentok = $this->getSessionBySession($sessionId);
        return $opentok;
        // dd($opentok);
    }

    public function setStatus($eventId, $userId, $type)
    {
        $status = null;
        switch($type) {
            case 'PUBLISHER: DIALED-IN':
            case 'READY':
                $status = 1;
            break;

            case 'PUBLISHER: destroyed':
                $status = 0;
            break;
        }
        
        if (!is_null($status)) {
            $user = $this->userRepository->find($userId);
            $column =  ($user->hasRole('patient')) ? 'attendee_status' : 'host_status';
            
            $ot = $this->repository->findWhere(['event_id' => $eventId])->first();
            $ot->{$column} = $status;
            $ot->save();
        }
    }

    public function __construct(
        OpenTokValidator $validator,
        OpenTokRepository $repository,
        EventRepository $rpoEvent,
        UserRepository $userRepository
    ) {
        $this->validator = $validator;
        $this->repository = $repository;
        $this->rpoEvent = $rpoEvent;
        $this->userRepository = $userRepository;
        
        $this->opentok = new OpenTok(config('opentok.API_KEY'), config('opentok.API_SECRET'));
    }
}
