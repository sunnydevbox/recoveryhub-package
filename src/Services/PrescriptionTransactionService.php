<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Sunnydevbox\Recoveryhub\Events\PrescriptionTransactionEvent;

// use Sunnydevbox\TWCart\Models\Cart;
use Shop;
// use \Sunnydevbox\TWCart\Services\CartService;


class PrescriptionTransactionService
{

    private $rpo;
    private $rpoMedicine;
    private $rpoEvent;
    private $rpoEventPrescription;
    private $rpoEventPrescriptionItem;
    private $cartService;

    public function store($data)
    {

        $tempPrescriptionItem = $this->rpoEventPrescriptionItem->with('medicine')->find($data['prescription_item_id']);
 
        $prescriptionTransaction = $this->rpo->create($data);

        $prescriptionTransaction->sku = $this->generatePRSku($prescriptionTransaction);
        $prescriptionTransaction->price = $tempPrescriptionItem->medicine->price;
        $prescriptionTransaction->save();

        // $data['sku'] = 

        $cartItem = $this->cartService->addItem($prescriptionTransaction, $data['quantity_ordered'], true);

        $prescriptionTransaction->cart_id = $cartItem->id;
        $prescriptionTransaction->save();

        

        if ( $prescriptionTransaction ) {

            event( new PrescriptionTransactionEvent( $prescriptionTransaction ) );
        }

        return $prescriptionTransaction;
    }

    public function generatePRSku($prescription)
    {
        return 'SKU-PR-' . $prescription->id;
    }

    public function updateTotalQty($item)
    {

        if (!$item){
            return;
        }
        // get total qty
        $total_qty_purchased = $total_qty_purchased_complete = $total_qty_purchased_pending = 0;
        // dd($item);
        $pendingTransactions = $this->rpo->findWhere([   
            'prescription_item_id' => $item->prescription_item_id,
            'status' => 'pending'
        ]);

        $completeTransactions = $this->rpo->findWhere([   
            'prescription_item_id' => $item->prescription_item_id,
            'status' => 'complete'
        ]);
        
        $processingTransactions = $this->rpo->findWhere([   
            'prescription_item_id' => $item->prescription_item_id,
            'status' => 'processing'
        ]);

        $preparedTransactions = $this->rpo->findWhere([   
            'prescription_item_id' => $item->prescription_item_id,
            'status' => 'prepared'
        ]);


        $tempPrescriptionItem = $this->rpoEventPrescriptionItem->with('medicine')->find($item->prescription_item_id);
            
        if ( $tempPrescriptionItem ) {
            // dd($pendingTransactions);
            $tempPrescriptionItem->total_qty_purchased = (  $pendingTransactions->sum('quantity_ordered') + 
                                                            $completeTransactions->sum('quantity_ordered') +
                                                            $processingTransactions->sum('quantity_ordered') +
                                                            $preparedTransactions->sum('quantity_ordered')
                                                        );
            $tempPrescriptionItem->total_qty_purchased_complete = $completeTransactions->sum('quantity_ordered');
            $tempPrescriptionItem->total_qty_purchased_pending = $pendingTransactions->sum('quantity_ordered');
   
            $tempPrescriptionItem->save();
            // dd($tempPrescriptionItem->total_qty_purchased);

        }
  
 
        return $tempPrescriptionItem;
    }

    // public function updateCheckoutPrescription($data)
    // {
        // $data->complete();
        // echo 'called service to update prescription transaction';
        // dd($data->item);
        // $pendingTransactions = $this->rpo->findWhere([   
        //     'prescription_item_id' => $data->item->id,
        //     'status' => 'pending'
        // ]);

        // foreach($data as $transaction){
        //     // $transaction->complete();

        //     foreach($transaction->transactions as $pendingTransaction){
        //         // echo '$pendingTransaction';
        //         // dd($pendingTransaction->scopePendingStatus());
        //         if ($pendingTransaction->status == 'pending'){
        //             $pendingTransaction->complete();
        //             $this->updateTotalQty($pendingTransaction);
        //         }
        //     }

        // }

    // }

    public function getPrescription($data)
    {
        $prescriptions;
        $eventPrescription;
        $event;

        if (is_object($data)) {
            $prescriptions = $this->rpoEventPrescriptionItem->with('medicine')->find($data->prescription_item_id);
        } else {

            $prescriptions = $this->rpoEventPrescriptionItem->with('medicine')->find($data);

        }

        return $prescriptions;
    }

    public function getEventPrescription($data)
    {
        $prescriptions;
        $eventPrescription;
        $event;

        if (is_object($data)) {
            $prescriptions = $this->rpoEventPrescriptionItem->with('medicine')->find($data->prescription_item_id);
        } else {

            $prescriptions = $this->rpoEventPrescriptionItem->find($data);

            $eventPrescription = $this->rpoEventPrescription->with( ['items', 'event'] )->find($prescriptions->event_prescription_id);

        }

        return $eventPrescription;
    }

    public function getOrderedItems($prescriptionItem)
    {
        return $this->rpoEventPrescriptionItem->with( ['transactions', 'medicine'] )->findWhere([
            'event_prescription_id' => $prescriptionItem->event_prescription_id
        ]);
    }

    public function updateTransactionStatus($id, $status)
    {
        $result;

        $prescription = $this->rpo->find($id);
        
        if ( $prescription ) {
            switch($status){
                case 'pending' :
                    $prescription->pending();
                break;
                case 'processing' :
                    $prescription->processing();
                break;
                case 'prepared' :
                    $prescription->prepared();
                break;
                case 'complete' :
                    $prescription->complete();
                break;
            }
        }
        return $prescription;
    }

    public function addTransactionNumber($order, $transactionNumber)
    {
        // dd('prescription here', $order);

        // $ordersData = null;

        if ( $order ) {

            foreach( $order->items as $item ) {
                
                $prescription = $this->fetchPrescription($item->reference_id);

                if ( $prescription ) {

                    $prescription->addTransactionNumber($transactionNumber);
                    
                    $prescriptionItem = $this->rpoEventPrescriptionItem->with('medicine')->find($prescription->prescription_item_id);

                }

            }

        }
    }

    public function fetchPrescription($id)
    {
        try {
            $prescription = $this->rpo->makeModel()->findOrFail($id);
            
            return $prescription;

        } catch (ModelNotFoundException $e) {
            // $flagException = false;
            return null;
        }
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\PrescriptionTransaction\PrescriptionTransactionRepository $rpo,
        \Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineInventoryRepository $rpoMedicine,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionItemRepository $rpoEventPrescriptionItem,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionRepository $rpoEventPrescription,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $rpoEvent,
        \Sunnydevbox\TWCart\Services\CartService $cartService
    ) {
        $this->rpo = $rpo;
        $this->rpoEvent = $rpoEvent;
        $this->rpoEventPrescription = $rpoEventPrescription;
        $this->rpoEventPrescriptionItem = $rpoEventPrescriptionItem;
        $this->rpoMedicine = $rpoMedicine;
        $this->cartService = $cartService;
    }
}
