<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\Recoveryhub\Traits\CartTrait;
use Sunnydevbox\TWCart\Services\CartService AS ExtendCartService;
use Sunnydevbox\Recoveryhub\Events\CheckoutPrescriptionEvent;
use Sunnydevbox\Recoveryhub\Events\TransactionStatusChangeEvent;

use Shop;

use Sunnydevbox\Recoveryhub\Models\Cart;

class CartTransactionService
{

    public function __construct(
        \Sunnydevbox\Recoveryhub\Services\EventPrescriptionItemService $prescriptionItemService,
        \Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService $prescriptionTransactionService,
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartTransactionRepository $cartTransactionRepository,
        \Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $userRepository
    ) {
        $this->prescriptionService = $prescriptionItemService;
        $this->cartTransactionRepository = $cartTransactionRepository;
        $this->repository = $cartTransactionRepository;
        $this->prescriptionTransactionService = $prescriptionTransactionService;
        $this->userRepository = $userRepository;
    }

    public function updateTransaction($txnid, $data)
    {
        $transaction = $this->cartTransactionRepository->with(['order'])->getByTxnid($txnid);

        if ($transaction) {
            switch(strtoupper($data['status'])) {
                case 'F':
                case 'U':
                    $status = 'failed';
                break;

                case 'S':
                    $status = 'completed';
                break;
                
                case 'P':
                    $status = 'pending';
                break;

                case 'R':
                    $status = 'refund';
                break;

                case 'K':
                    $status = 'chargeback';
                break;

                case 'V': // Void
                    $status = 'canceled';
                break;

                case 'A':
                    $status = 'authorized';
                break;

                case 'U':
                default:
                    $status = 'failed';
                break;
            }

            $previousStatusCode = $transaction->order->statusCode;
            \Log::info('Order status chagned from '. $previousStatusCode . ' to ' . $status);
            $transaction->order->statusCode = $status;
            $transaction->order->save();

            
            if ($transaction->gateway == 'dragonpay') {
                
                $details = $transaction->detail;
                $key = 'callback_' . date('Y-m-d_H:i:s');
                

                $details[$key] = $data;
                
                $transaction->detail = $details;
                $transaction->save();
            }
            // $id, $statusCode, $previousStatusCode
            // event( new \Amsgames\LaravelShop\Events\OrderStatusChanged($transaction->order->id, $transaction->order->statusCode, $previousStatusCode ) );
        }


        // dd($transaction->order);
    }

    public function placeOrder($data = [])
    {
        $this->checkout();
        
        $order = Shop::placeOrder();
        
        if ($order->hasFailed) {
            $exception = Shop::exception();
            echo $exception->getMessage(); // echos: error
        }

        $transaction = $order->transactions[0];
        
        // update table with transaction number
        $this->prescriptionTransactionService->addTransactionNumber($order, $transaction->transaction_id);
        // call event to send email to lifeport and patient
        event( new CheckoutPrescriptionEvent( $order) ); 

        return [
            'transaction_id'    => $transaction->transaction_id, 
            'gateway'           => $transaction->gateway,
            'detail'            => $transaction->detail,
            'token'             => $transaction->token,
            'date'              => $transaction->created_at,
            'status'            => $order->statusCode,

            // THIS IS TEMPORARY
        ];
    }

    public function statusEvent($request, $id)
    {
        $user;
        $res = $this->cartTransactionRepository->with('order')->find($id);

        if ( $res ) {
            $user = $this->userRepository->find($res->order->user_id);

            $data = [
                'transaction'   => $res,
                'user'          => $user,
                'request'       => $request
            ];

            event( new TransactionStatusChangeEvent( $data ) );
        }
        // echo 'here';
        return $res;

    }


    
}