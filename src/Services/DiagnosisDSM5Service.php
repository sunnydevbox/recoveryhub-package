<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\Recoveryhub\Models\DiagnosisDSM5;

class DiagnosisDSM5Service
{
    public function fix()
    {
        DiagnosisDSM5::fixTree();
    }
}