<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\Recoveryhub\Events\PrescriptionTransactionEvent;


class EventPrescriptionItemService
{

    public $rpoPrescriptionTransactionRepository;


    public function attachItems($eventPrescription, $items = [])
    {
        $eventPrescription->items()->delete();
        $items = collect($items)->map(function($item, $key) use ($eventPrescription) {
            if (isset($item['medicine_id']) &&  $this->rpoMedicineInventory->find($item['medicine_id'])) {

                $eventPrescription->items()->save($this->rpoEventPrescriptionItem->makeModel()->fill($item));
            }            
        });

        return $items;
    }

    public function checkAvailableQuantity($prescriptionItemId = null)
    {
        $item = $this->rpoEventPrescriptionItem->find($prescriptionItemId);
        dd($item->toArray());
    }

    public function addToCart($prescriptionItemId = null, $quantity = 0)
    {
        try {
            $item = $this->rpoEventPrescriptionItem->find($prescriptionItemId);
            
            if ($item->has_available && $item->available >= $quantity) {
                // dd($item->available >= $quantity);
                $cartService = app(\Sunnydevbox\Recoveryhub\Services\CartService::class);
                $cartService->cart()->add($item, $quantity);

                /// UPDATE
                $this->updatePendingPurchaseQuantity($item, $quantity);

                return $item;
            } else {
                throw new \Exception('invalid_quantity', 400);
            }
            
            // return $this->response->item($result, $this->transformer);

        } catch(\Exception $e) {
            throw $e;
        }
    }

    public function updatePendingPurchaseQuantity($prescriptionItemId = null, $quantity = null)
    {
        if (is_numeric($quantity)) {
            $prescriptionItemId->total_qty_purchased_pending += $quantity;
            $prescriptionItemId->update();
        }
    }

    public function updateItem($object)
    {

        if ($object) {

            $prescriptionTransaction = $this->rpoPrescriptionTransactionRepository->findWhere( ['prescription_item_id' => $object['resPrescription']->prescription_item_id] )->first();

            if ( !$prescriptionTransaction ) {
                // echo 'test'; dd($object);
                // $prescriptionTransaction = $this->rpoPrescriptionTransactionRepository->findWhere( ['prescription_item_id' => $object['presId']] );
                $res = $this->rpoEventPrescriptionItem->find($object['resPrescription']->prescription_item_id);
                // $res->total_qty_purchased_pending = 0;
                // $res->update();
                $prescriptionT = $this->rpoPrescriptionTransactionRepository->findWhere( ['prescription_item_id' => $res->id] );

                if ( $res ) {
                    // echo 'update'; dd($prescriptionT);
                    $res->total_qty_purchased_pending = 0;
                    $res->total_qty_purchased_complete = 0;
                    $res->total_qty_purchased = 0;
                    $res->save();
                }
                // echo 'pass'; dd(0);
            } else {
                // echo 'else'; dd(0);
                event( new PrescriptionTransactionEvent( $prescriptionTransaction ) );
            }

            // event( new PrescriptionTransactionEvent( $prescriptionTransaction ) );
        }
    }

    public function getPrescriptionId($data)
    {
        if ($data) {
            $prescriptionTransaction = $this->rpoPrescriptionTransactionRepository->findWhere( ['cart_id' => $data['id']] )->first();

        }

        return $prescriptionTransaction;
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionItemRepository $rpoEventPrescriptionItem,
        \Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineInventoryRepository $rpoMedicineInventory,
        \Sunnydevbox\Recoveryhub\Repositories\PrescriptionTransaction\PrescriptionTransactionRepository $rpoPrescriptionTransactionRepository
    ) {
        $this->rpoEventPrescriptionItem = $rpoEventPrescriptionItem;
        $this->rpoMedicineInventory = $rpoMedicineInventory;
        $this->rpoPrescriptionTransactionRepository = $rpoPrescriptionTransactionRepository;
    }
}
