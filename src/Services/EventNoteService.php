<?php

namespace Sunnydevbox\Recoveryhub\Services;

use JWTAuth;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository as rpoEvent;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventNoteRepository as rpoEventNote;
use File;
use Response;

class EventNoteService
{
    private $rpoEvent;
    private $rpoEventNote;

    public function store($request)
    {
        // CHECK IF Current user is the owner of the event
        
        $user = JWTAuth::parseToken()->authenticate();
        $notes = $this->rpoEventNote->makeModel()->where('event_id', $request->get('event_id'))->first();
        
        // CHECK IF the note exists or not
        if($notes) {
             // UPDATE
            // $result = $this->rpoEventNote->update($request->only(['event_id', 'notes']), $event->note->id);

            $notes->notes = $request->get('notes');
            $notes->save();

            return $notes;
            
        } else {
            // STORE new record
            $result = $this->rpoEventNote->create($request->only(['event_id', 'notes']));
        }

        return $result;
    }

    public function index($request)
    {
        $limit = $request->get('limit') ? $request->get('limit') : config('repository.pagination.limit', 15);
        
        $notes = $this->rpoEventNote->paginate($limit);

        return $notes;
    }

    public function __construct(
        rpoEvent $rpoEvent ,
        rpoEventNote $rpoEventNote
    ) {
        $this->rpoEvent = $rpoEvent;
        $this->rpoEventNote = $rpoEventNote;
    }
}
