<?php
namespace Sunnydevbox\Recoveryhub\Services;

use Sunnydevbox\TWCart\Services\CouponService as OriginalCouponService;
use Sunnydevbox\TWCart\Factories\PaymentFactory;
use Shop;
use Auth;
use \Carbon\Carbon;
use Exception;

class CouponService extends OriginalCouponService
{
    public function remove($code, $Model = null)
    {
        try {
            $couponCode = $this->get($code);
            
            if (!$Model) {
                $Model = Auth::user()->cart;
            }

            // dd($Model->coupons);

            $Model->coupons->map(function($coupon) use ($couponCode, $Model) {
                if ($couponCode->id == $coupon->coupon->id) {
                    return $coupon->delete();
                }
            });

            // throw new Exception('coupon_not_removed', 400);
        } catch(Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
    
    public function apply($code, $cart = null)
    {   
        if (!$cart) {
            $cart = Auth::user()->cart;
        }

        if (!$cart) {
            throw new Exception('cart_is_empty', 400);
        }

        $couponCode = $this->get($code);
        $couponProcessor = null;

        if ($couponCode->hasLimitReached()) {
            throw new Exception('coupon_limit_reached', 400);
        }

        if ($couponCode->processor) {
            try {
                $couponProcessor = app($couponCode->processor);
            } catch (Exception $e) {
                dd($e);
            }
        }

        if ($couponProcessor) {
            $couponProcessor->coupon($couponCode);
            $couponProcessor->cart($cart);

           if (!$couponProcessor->verify()) {
               throw new Exception('not_allowed', 400);
           }
        }


        // APPLY only if the cart is not empty 
        // AND coupon has not been applied yet
        if (
            $cart->items->count() && 
            !$cart->coupons->where('coupon_id', $couponCode->id)->first()
        ) {

            $appliedCoupon = $this->rpoAppliedCoupon->firstOrCreate([
                'coupon_link_id' => $cart->id,
                'coupon_link_type' => get_class($cart),
                'coupon_id' => $couponCode->id
            ]);
            
            return $appliedCoupon;
        }
    }

    public function switchToOrder($code, $Order = null)
    {
        try {
            $couponCode = $this->get($code);
            $Cart = Auth::user()->cart;

            Auth::user()->cart->coupons->map(function($coupon) use ($code, $Order) {
                if ($code->id == $coupon->coupon->id) {
                    $coupon->update([
                        'coupon_link_id' => $Order->id,
                        'coupon_link_type' => config('shop.order'),
                    ]);
                }
            });
        } catch(Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Coupon\CouponRepository $rpoCoupon,
        \Sunnydevbox\Recoveryhub\Repositories\Coupon\AppliedCouponRepository $rpoAppliedCoupon
    ) {
        $this->rpoCoupon = $rpoCoupon;
        $this->rpoAppliedCoupon = $rpoAppliedCoupon;
    }
}