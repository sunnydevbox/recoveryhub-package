<?php

namespace Sunnydevbox\Recoveryhub;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        // EVENT PRESCRIPTION CREATED
        'Sunnydevbox\Recoveryhub\Events\EventPrescriptionCreatedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendEventPrescriptionEmailListener',
        ],


        // DOCTOR ACCOUNT CREATED
        'Sunnydevbox\Recoveryhub\Events\DoctorAccountCreatedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\DoctorSetDefaultChargeValues',
        ],

        // PATIENT ACCOUNT CREATED
        'Sunnydevbox\Recoveryhub\Events\PatientAccountCreatedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\CreateMentalHealthDataListener',
        ],

        // EVENT CREATED
        'Sunnydevbox\Recoveryhub\Events\EventCreatedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SetEventOpenStatus',
            'Sunnydevbox\Recoveryhub\Listeners\SendNewEventMailEventListener',
        ],

        // EVENT FEEDBACK EMAIL TO RECOVERY INFO
        'Sunnydevbox\Recoveryhub\Events\EventFeedbackEmailInfoEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\EventFeedbackEmailInfoListener',
        ],

        // APPOINTMENT SET
        'Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent' => [
            // NOt applicable since the status depends on the response of the payment gatway
            // 'Sunnydevbox\Recoveryhub\Listeners\SetEventBookedStatus',
            
            'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentEmailNotificationToDoctorListener',
            'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentEmailNotificationToPatientListener',
        ],

        //APPOINTMENT CANCELLED
        'Sunnydevbox\Recoveryhub\Events\AppointmentDeletedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentCancelEmailNotificationToDoctorListener',
            'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentCancelEmailNotificationToPatientListener',
        ],

        // APPOINTMENT/CALL COMPLETED
        'Sunnydevbox\Recoveryhub\Events\AppointmentCompletedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SetEventCompletedStatus',
            
            // SEND email to both parties that the call has completed
            // 'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentEmailNotificationToDoctorListener',
            // 'Sunnydevbox\Recoveryhub\Listeners\SendAppointmentEmailNotificationToPatientListener',
        ],

        'Sunnydevbox\Recoveryhub\Events\EventReminderEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendNewEventMailEventListener',
        ],

        'Sunnydevbox\Recoveryhub\Events\ContactFormSubmittedEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendContactFormEmailListener',
        ],

        'Sunnydevbox\Recoveryhub\Events\EventSendEmailEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendEmailTypeEventListener',
        ],

        // User signed up for an account
        'Sunnydevbox\Recoveryhub\Events\UserRegisteredEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendActivationCodeEventListener',
        ],

        // USER Verified his email address
        'Sunnydevbox\Recoveryhub\Events\UserVerifiedEmailEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\VerifiedAccount',
            'Sunnydevbox\Recoveryhub\Listeners\SendEmailVerificationConfirmation',
        ],

        // PRESCRIPTION LISTENER    
        'Sunnydevbox\Recoveryhub\Events\PrescriptionTransactionEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\PrescriptionTransactionListener',
        ],

        // DELIVER TO LIFEPORT EVENT    
        'Sunnydevbox\Recoveryhub\Events\SendLifePortPrescriptionEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\SendLifePortPrescriptionListener',
        ],

        // CHECKOUT PRESCRIPTION   
        'Sunnydevbox\Recoveryhub\Events\CheckoutPrescriptionEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\CheckoutNotifyLifePortListener',
            // 'Sunnydevbox\Recoveryhub\Listeners\CheckoutNotifyPatientListener',
            // 'Sunnydevbox\Recoveryhub\Listeners\CheckoutPrescriptionListener',
        ],

        // TRANSACTION STATUS UPDATE   
        'Sunnydevbox\Recoveryhub\Events\TransactionStatusChangeEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\TransactionStatusChangeListener'
        ],

        'Amsgames\LaravelShop\Events\OrderStatusChanged' => [
            'Sunnydevbox\Recoveryhub\Listeners\PurchaseStatusChangeListener',
        ],
        
        'Sunnydevbox\Recoveryhub\Events\DragonpayTxStatusCheckEvent' => [
            'Sunnydevbox\Recoveryhub\Listeners\DragonpayTxStatusParserListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
