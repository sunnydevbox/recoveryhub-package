<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class StaffCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        if ( $user ) {

            if ($user->hasRole(['admin'])) {
                $model = $model->whereHas('roles', function($query) {
                    $query->where('name', 'staff');
                //  'nurse'
                });
            }

        }

        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
