<?php
namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use JWTAuth;
use Dingo\Api\Http\Request;
use Illuminate\Validation\ValidationException;

class AdminUserCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
      
        $filter = request()->get(config('repository.criteria.params.filter', 'filter'), null);
        $orderBy = request()->get(config('repository.criteria.params.orderBy', 'orderBy'), null);
        $sortedBy = request()->get(config('repository.criteria.params.sortedBy', 'sortedBy'), 'asc');
        $with = request()->get(config('repository.criteria.params.with', 'with'), null);
        $fieldsSearchable = $repository->getFieldsSearchable();
        $search = request()->get(config('repository.criteria.params.search', 'search'), null);
        $searchFields = request()->get(config('repository.criteria.params.searchFields', 'searchFields'), null);

        
        
        if ($search) {

            if (isset($filter) && !empty($filter)) {
                if (is_string($filter)) {
                    $filter = explode(';', $filter);
                }
                $filter = collect($filter)->map(function($item) {
                    $orderBySplits = explode('.', $item);
                    return 'users.'.$item;
                })->all();
                
                $model = $model->select($filter);
            } else {
                $model = $model->select('users.*');
            }
    
            $model = $model->leftJoin('meta', function($query) use ($repository, $search) {
                $query->on('meta.metable_id', '=', 'users.id');
            });

            $searchTerms = explode(';',$search);
            
            foreach($searchTerms as $searchTerm) {
                if (strlen($searchTerm) == 0) continue;

                if (is_numeric(strpos($searchTerm, ':'))) {
                    $searchFields = is_array($searchFields) || is_null($searchFields) ? $searchFields : explode(';', $searchFields);
                    $fields = $this->parserFieldsSearch($fieldsSearchable, $searchFields);
                    $isFirstField = true;
                    $searchData = $this->parserSearchData($search);
                    $search = $this->parserSearchValue($search);
                    $modelForceAndWhere =  'and';

                    $model = $model->where(function ($query) use ($fields, $search, $searchData, $isFirstField, $modelForceAndWhere) {
                        /** @var Builder $query */
                        foreach ($fields as $field => $condition) {

                            if (is_numeric($field)) {
                                $field = $condition;
                                $condition = "=";
                            }

                            $value = null;

                            $condition = trim(strtolower($condition));

                            if (isset($searchData[$field])) {
                                switch($condition) {
                                    case 'like%':
                                        $value = "{$searchData[$field]}%";
                                        $condition = 'like';
                                        break;

                                    case '%like':
                                        $value = "%{$searchData[$field]}";
                                        $condition = 'like';
                                        break;

                                    default:
                                        $value = ($condition == "like" || $condition == "ilike") ? "%{$searchData[$field]}%" : $searchData[$field];
                                    break;
                                }

                                // $value = ($condition == "like" || $condition == "ilike") ? "%{$searchData[$field]}%" : $searchData[$field];
                            } else {
                                if (!is_null($search)) {
                                    switch($condition) {
                                        case 'like%':
                                            $value = "{$search}%";
                                            $condition = 'like';
                                            break;

                                        case '%like':
                                            $value = "%{$search}";
                                            $condition = 'like';
                                            break;

                                        default:
                                            $value = ($condition == "like" || $condition == "ilike") ? "%{$search}%" : $search;
                                        break;
                                    }
                                    //$value = ($condition == "like" || $condition == "ilike") ? "%{$search}%" : $search;
                                }
                            }

                            $relation = null;
                            if(stripos($field, '.')) {
                                $explode = explode('.', $field);
                                $field = array_pop($explode);
                                $relation = implode('.', $explode);
                            }
                            $modelTableName = $query->getModel()->getTable();
                            if ( $isFirstField || $modelForceAndWhere ) {
                                if (!is_null($value)) {
                                    if(!is_null($relation)) {
                                        $query->whereHas($relation, function($query) use($field,$condition,$value) {
                                            $query->where($field,$condition,$value);
                                        });
                                    } else {
                                        $query->where($modelTableName.'.'.$field,$condition,$value);
                                    }
                                    $isFirstField = false;
                                }
                            } else {
                                if (!is_null($value)) {
                                    if(!is_null($relation)) {
                                        $query->orWhereHas($relation, function($query) use($field,$condition,$value) {
                                            $query->where($field,$condition,$value);
                                        });
                                    } else {
                                        $query->orWhere($modelTableName.'.'.$field, $condition, $value);
                                    }
                                }
                            }
                        }
                    });

                } else {
                    $model = $model->where('meta.metable_type', '=', (get_class($repository->makeModel())))
                        ->where(function($query) use ($searchTerm) {
                            $query
                                ->where('meta.key', '=', 'last_name')
                                ->where('meta.value', 'LIKE', "%{$searchTerm}%")
                                ;
                            })
                            ->orWhere(function($query) use ($searchTerm) {
                                $query->where('meta.key', '=', 'first_name')
                                    ->where('meta.value', 'LIKE', "%{$searchTerm}%");
                        });
                    ;
                }
            }
        }
// dd(9);

        // if ($orderBy) {
        //     $orderBys = explode(';', $orderBy);

        //     foreach($orderBys as $column)  {
        //         if ($repository->makeModel()->_meta()->contains($column)) {
        //             //$model = $model->where('meta.key', '=', $column);
        //         }
        //     }

        //     $model = $model->orderBy('meta.value', $sortedBy);
        // }

        // if ($with) {
        //     $with = explode(';', $with);
        //     $model = $model->with($with, function($query) {
        //         $query->select('id');
        //     });
        // }

        
        return $model;
    }


        /**
     * @param $search
     *
     * @return array
     */
    protected function parserSearchData($search)
    {
        $searchData = [];

        if (stripos($search, ':')) {
            $fields = explode(';', $search);

            foreach ($fields as $row) {
                try {
                    list($field, $value) = explode(':', $row);
                    $searchData[$field] = $value;
                } catch (\Exception $e) {
                    //Surround offset error
                }
            }
        }

        return $searchData;
    }

    /**
     * @param $search
     *
     * @return null
     */
    protected function parserSearchValue($search)
    {

        if (stripos($search, ';') || stripos($search, ':')) {
            $values = explode(';', $search);
            foreach ($values as $value) {
                $s = explode(':', $value);
                if (count($s) == 1) {
                    return $s[0];
                }
            }

            return null;
        }

        return $search;
    }


    protected function parserFieldsSearch(array $fields = [], array $searchFields = null)
    {
        if (!is_null($searchFields) && count($searchFields)) {
            $acceptedConditions = config('repository.criteria.acceptedConditions', [
                '=',
                'like'
            ]);
            $originalFields = $fields;
            $fields = [];

            foreach ($searchFields as $index => $field) {
                $field_parts = explode(':', $field);
                $temporaryIndex = array_search($field_parts[0], $originalFields);

                if (count($field_parts) == 2) {
                    if (in_array($field_parts[1], $acceptedConditions)) {
                        unset($originalFields[$temporaryIndex]);
                        $field = $field_parts[0];
                        $condition = $field_parts[1];
                        $originalFields[$field] = $condition;
                        $searchFields[$index] = $field;
                    }
                }
            }

            foreach ($originalFields as $field => $condition) {
                if (is_numeric($field)) {
                    $field = $condition;
                    $condition = "=";
                }
                if (in_array($field, $searchFields)) {
                    $fields[$field] = $condition;
                }
            }

            if (count($fields) == 0) {
                throw new \Exception(trans('repository::criteria.fields_not_accepted', ['field' => implode(',', $searchFields)]));
            }

        }

        return $fields;
    }

}