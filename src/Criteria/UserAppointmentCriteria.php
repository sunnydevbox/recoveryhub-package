<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Illuminate\Validation\ValidationException;
use \Symfony\Component\HttpKernel\Exception\HttpException;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserAppointmentCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user->hasRole(['patient'])) {
            abort(400, 'You are not permitted to perform this action');
        }

        $model = $model->where('bookings.assignable_id', '=', $user->id);
        
        // $model = $model->join('events', 'events.id', '=', 'bookings.bookable_id')
        //             ->join('users', 'users.id', '=', 'events.assignable_id');

        // $model = $model
        //             ->select('bookings.*')
        //             ->with(['event.doctor','patient']);

            // join('events', 'events.id', '=', 'bookings.bookable_id')
            //          ->join('users', 'users.id', '=', 'events.assignable_id');
        /**
         * ?groupByDoctor=1 
         */        
        if ($groupBy = request()->get('groupByDoctor')) {
            /**
             * 
                select u.email, b.id as BookID
                from bookings b
                join events e on (e.id = b.bookable_id)
                join users u on (u.id = e.assignable_id)
                where b.assignable_id=3
                group by u.email
             */
            
            $model = $model->groupBy('users.id');
            /** 
             * IMPORTANT:
             * Add the event status 'Booked' as one
             * of the conditions
             */
        }

        /**
         * ?sortDoctor=columnName|direction
         * ? 
         */        
        if ($sortDoctor = request()->get('sortDoctor')) {
            /**
             * 
                select u.email, b.id as BookID
                from bookings b
                join events e on (e.id = b.bookable_id)
                join users u on (u.id = e.assignable_id)
                where b.assignable_id=3
                group by u.email
             */ 

            $split = explode('|', $sortDoctor);
            $column = $split[0];
            $direction = isset($split[1]) ? $split[1] : 'asc';

            $i = false;
            if (strtolower($column) == 'recent') {
                $i = true;
                $column = 'events.start_at';
            } else if (in_array(strtolower($column), ['first_name', 'last_name'])) {
                // GET FROM META TABLE
                $model = $model->join('meta', 'meta.metable_id', '=', 'users.id')
                            ->where('meta.key', $column)
                            ->orderBy('meta.value', $direction);
            } else {
                $i = true;
                $column = 'users.' . $column;
            }

            if ($i) {
                $model = $model->orderBy($column, $direction);
            }
        }

        if ($sortDate = request()->get('sortDate')) {
            $model = $model->orderBy('events.start_at', $sortDate);
        }

        /**
         * Overrides REquestCriteria's filter
         * as long as this is pushCriteria() right after RequestCritieria
         */
        if ($filter = request()->get('filter')) {        
            $model = $model->select('*');
        }

        // print_r($model->getBindings());
        // dd($model->toSql());
        return $model;
    }
}
