<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use \Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class MyPatientsCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user->hasRole(['medical-practitioner','psychologist'])) {
            abort(400, 'not_permitted');
        }

        $model = $model->whereHas('bookings.event_v2', function($query) use ($user) {
                        $query->where('assignable_id', $user->id);
                    });


        if ($searchTerm = request()->get('search')) {
            $searchTerm = trim($searchTerm);
            
            $searchTerms = explode(';',$searchTerm);
            foreach($searchTerms as $searchTerm) {
                if (is_numeric(strpos($searchTerm, ':'))) {
                    // dd($searchTerm);
                } else {
                    $model = $model->whereHas('meta', function($query) use ($searchTerm) {
                        $query->where(function($query) use ($searchTerm) {
                            
                            $query->where('key', 'first_name')
                                    ->where('value', 'LIKE', '%' . $searchTerm . '%');
                        })
                        ->orWhere(function($query) use ($searchTerm) {
                            $query->where('key', 'last_name')
                                    ->where('value', 'LIKE', '%' . $searchTerm . '%');
                        });
                        
                    });
                }
            }
        }

        if ($orderBy = request()->get('orderBy')) {
            $orderBy = strtolower(trim($orderBy));

            $direction = 'asc';
            if ($direction = request()->get('sortedBy')) {
                $direction = strtolower(trim($direction));
            }

            if ($orderBy == 'name') {

                
                $model->join('meta', 'meta.metable_id', '=', 'users.id')
                    ->where('meta.metable_type', config('auth.providers.users.model'))
                    ->where('key', 'first_name')
                    ->orderBy('value', $direction)
                    ->select(['users.*'])
                    ;
            }
        }
        $model->isActive();
        //dd($model->getBindings(), $model->toSql());
        return $model;
    }
}