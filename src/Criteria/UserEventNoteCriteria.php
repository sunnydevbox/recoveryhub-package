<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventNoteCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserEventNoteCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if (!$user->hasRole(['medical-practitioner', 'psychologist'])) {
            abort(400, 'not_permitted');
        }
        

        // OVERRIDE THE filter to handle
        // 'id'.
        $filter = app('request')->get(config('repository.criteria.params.filter', 'filter'), null);

        if (isset($filter) && !empty($filter)) {
            if (is_string($filter)) {
                $filter = explode(';', $filter);
            }
            
            if (is_numeric($index = array_search('id', $filter))) {
                $filter[$index] = 'event_notes.id';   
            }

            if (is_numeric($index = array_search('updated_at', $filter))) {
                $filter[$index] = 'event_notes.updated_at';   
            }

            if (is_numeric($index = array_search('created_at', $filter))) {
                $filter[$index] = 'event_notes.created_at';   
            }
            
            $model = $model->select($filter);
        } else {
            $model = $model->select('event_notes.*');
        }

        
        $model = $model->join('events', 'events.id', '=', 'event_notes.event_id');

        $model = $model->where('events.assignable_id','=', $user->id );
     
        
        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
