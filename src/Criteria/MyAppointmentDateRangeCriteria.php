<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use \Carbon\Carbon;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class MyAppointmentDateRangeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // dateRange=columnname:2016-01-01:>=|2017-12-30
        // dateRange=columnname;2016-01-01:>=|2017-12-30
        $dateRange = request()->get('dateRange');

        // DATE RANGE
        if ($dateRange) {
            $dateRanges = explode('|', $dateRange);
            $date1 = $dateRanges[0];
            $date2 = isset($dateRanges[1]) ? $dateRanges[1] : null;


            $model = $model->leftJoin('events', 'events.id', '=', 'bookings.bookable_id')
            ->where(function($query) use ($dateRanges) {

                foreach($dateRanges as $dateRange) {
                    $operator = '>=';
                    $valueOperator = explode(';', $dateRange);
                    $column = $valueOperator[0];
                    $value = $valueOperator[1];
                    
                    if (isset($valueOperator[2]) && strlen($valueOperator[2]) > 0) {
                        $operator = $valueOperator[2];
                    }

                    if ($parts = explode('.', $column)) {
                        $last = array_pop($parts);
                        $parts = array(implode('_', $parts), $last);
                        $relationship = $parts[0];
                        $column = $parts[1];

                        /**
                         * BECAUSE polymorphic conditions is not applicable...
                         */

                        $query->whereDate('events.'. $column, $operator, $value);

                    } else {
                        $query->where($column, $operator, $value);
                    }
                }
            });

        
        }   
        //  dd($model->toSql(), $model->getBindings());
        return $model;
    }
}
