<?php
namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use JWTAuth;
use Dingo\Api\Http\Request;
use Illuminate\Validation\ValidationException;

class PatientCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        /** PARAMS:
         * user_id: numeric id|'current's
         * 
         */

        if (request()->get('user_id')) {
            if (is_numeric(request()->get('user_id'))) {
                $user_id = request()->get('user_id');
                
            } else {
                $user = JWTAuth::parseToken()->authenticate();
        
                if (!$user->hasRole('medical-practitioner')) {
                    throw new ValidationException('access_not_permitted');
                } else {
                    $user_id = $user->id;
                }
            }

            $model = $model->where('id', '=',  $user_id);
        }

        
        $model = $model->role(['patient', 'in-patient',]);
        return $model;
    }
}