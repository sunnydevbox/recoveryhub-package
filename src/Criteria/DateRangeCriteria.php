<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use \Carbon\Carbon;
use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class DateRangeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // dateRange=columnname:2016-01-01:>=|2017-12-30
        // dateRange=columnname;2016-01-01:>=|2017-12-30
        $dateRange = request()->get('dateRange');

        if ((preg_match('/user\/appointments/', request()->path()))) {
            $model = $model->leftJoin('events', 'events.id', '=', 'bookings.bookable_id');
        }
        //dd($model->first());
        // DATE RANGE
        if ($dateRange) {
            $dateRanges = explode('|', $dateRange);
            $date1 = $dateRanges[0];
            // dd($dateRanges); 
            $date2 = isset($dateRanges[1]) ? $dateRanges[1] : null;
        
            $this->process($model, $date1);

            if ($date2) {
                $this->setSecondDate($model, $date2); 
            }

            // dd($date1, $date2);
        }   
        // dd($model->getBindings(), $model->toSql());
        return $model;
    }

    private function process(&$model, $date)
    {
        $operator = '>=';
        
        $segments = explode(':', $date);

        $column = $segments[0];
        $value = $segments[1];
        // $value = str_replace('/', ':', $value);
        $value = Carbon::parse(str_replace('/', ':', $value));

        $operator = isset($segments[2]) ? $segments[2] : $operator;
        $model = $model->where($column, $operator, $value);
    }

    private function setSecondDate(&$model, $date)
    {
        $operator = '>=';
        
        $segments = explode(':', $date);
        $column = $segments[0];
        $value = $segments[1];
        $value = Carbon::parse(str_replace('/', ':', $value));
        
        // $segments[1] = str_replace('/', ':', $segments[1]);
        // $value = Carbon::parse($segments[1])->addHours(23)->addMinutes(59)->addSeconds(59);
        $operator = isset($segments[2]) ? $segments[2] : $operator;

        $model = $model->where($column, $operator, $value);
    }
}