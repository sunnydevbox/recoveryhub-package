<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class HasFilter implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( request()->get('filters') ) {

            $filters = explode(';', request()->get('filters'));
            
            $model = $model->where(function($query) use ($filters) {
                foreach ($filters as $filter) {
                    $parts = explode(':', $filter);
                    $value = $parts[1];
                    $operator = isset($parts[2]) ? $parts[2] : '=';
                    $position = strrpos ($parts[0], '.');
                   
                    
                    if (is_numeric($position)) {
                        $relationship = substr($parts[0], 0, $position);
                        $column = substr($parts[0], $position+1);
                        
                        // $query->whereHas($relationship, function($query) use ($column, $operator, $value) {
                        //     $query->where($column, $operator, $value);
                        // });

                    } else {
                        $column = $parts[0];
                        $query->where($column,$operator, $value);
                    }
                    
                }

            });

        }

        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
