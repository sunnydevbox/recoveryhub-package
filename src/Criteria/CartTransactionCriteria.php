<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use Dingo\Api\Http\Request;
use Auth;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class CartTransactionCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        // User has to be authenticated
        if (Auth::check()) {

            $user = Auth::user();
            $model = $model->select(['cart_transactions.*']);
  
            if ( $user->hasRole('patient') ) {
                $model = $model->whereUser($user->id);
            }

            // $direction = 'asc';

            if ( $orderBy = request()->get('orderBy') ) {
                $orderBy = strtolower(trim($orderBy));
    
                $direction = request()->get('sortedBy') ? strtolower( trim( request()->get('sortedBy') ) ) : 'asc';

                $model = $model->orderBy($orderBy, $direction);
            }
            
            return $model;
        } 
        
        abort(401);
    }
}
