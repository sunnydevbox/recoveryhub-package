<?php
namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
use JWTAuth;
use Dingo\Api\Http\Request;
use Illuminate\Validation\ValidationException;

class ProfileQuestionsCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model
            ->where('profile_questions.status', 1);

        

        $patient_id = app('request')->get('patient_id', null);

        if (is_null($patient_id)) {
            $user = JWTAuth::parseToken()->authenticate();
            if ($user->hasRole('patient')) {
                $patient_id = $user->id;
            }
        }

        if (!is_null($patient_id)) {
            $model = $model
                ->with(['answers' => function($query) use ($patient_id) {
                    $query->where('user_id', $patient_id);
                }]); 
        }

        // print_r($model->getBindings()); dd($model->toSql());
        return $model;
    }
}