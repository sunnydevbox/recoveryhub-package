<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEvIsMyAppointmentCriteriaentCriteria.
 *
 * @package namespace App\Criteria;
 */
class IsMyAppointmentCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $isMyAppointment = request()->get('isMyAppointment');
        if ($isMyAppointment) { 
            $user = JWTAuth::parseToken()->authenticate();
            // dd($user->id);
            if ($user->hasRole(['patient'])) {
                // $model = $model->whereHas()
                $model = $model
                    ->with(['bookings'])
                    ->whereHas('bookings.patient', function($query) use ($user) {
                        $query->where('assignable_id', $user->id);
                    });
            }
        }
         
        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
