<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class AsDoctor implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        $model = $model->where('assignable_id', $user->id);
        
        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
