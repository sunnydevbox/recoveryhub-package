<?php

namespace Sunnydevbox\Recoveryhub\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use JWTAuth;

use Dingo\Api\Http\Request;

//use Illuminate\Http\Request;

/**
 * Class UserEventCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserEventCriteria implements CriteriaInterface
{
    protected $request;

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = JWTAuth::parseToken()->authenticate();
        
        if ( request()->get('doctorId') ) {

            $model = $model->where('assignable_id','=', request()->get('doctorId') );

        } else {

            if (!$user->hasRole(['medical-practitioner', 'admin', 'psychologist'])) {
                abort(400, 'not_permitted');
            }
            
            if ($user->hasRole(['medical-practitioner'])) {
                $model = $model->where('assignable_id','=', $user->id );
            }

        }
        $model = $model->has('cartItem.order');
        if ($has = request()->get('has')) {
            $has = explode(';', $has);
            
            foreach($has as $h) {

                if (preg_match('/bookingsWith:/', $h)) {
                    $bookingsWith = explode(':', $h);
                    $bookedWithId = $bookingsWith[1];

                    $model = $model->whereHas('bookings', function($query) use ($bookedWithId) {
                        $query->where('assignable_id', $bookedWithId);
                    });
                } else if ($h == 'bookings') {
                    $model = $model->has('bookings');
                } else if ($h == 'cartItem.order') {
                    $model = $model->has('cartItem.order');
                    // $model = $model->whereHas('cartItem', function($query){
                    //     $query->whereNotNull('cart_items.order_id')
                    //         ->whereNull('cart_items.cart_id');    
                       
                        
                    // }); 


                    // $model = $model->where(function($query) {
                    //     $query->whereHas('cartItem.order');
                    //     // $query->whereNotNull('cart_items.order_id');
                    //     // $query->isOrdered();
                    // });
                }
            }
            
            // if (gettype(array_search('bookings', $has)) == 'integer'
            // ) {
            //     $model = $model->has('bookings');
            // }

        }

        // print_r($model->getBindings());
        // dd($model->toSql());

        return $model;
    }
}
