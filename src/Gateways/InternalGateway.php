<?php
namespace Sunnydevbox\Recoveryhub\Gateways;

use Sunnydevbox\TWCart\Core\PaymentGateway;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\Dragonpay;
use Amsgames\LaravelShop\Exceptions\CheckoutException;
use Amsgames\LaravelShop\Exceptions\GatewayException;
use Amsgames\LaravelShop\Exceptions\ShopException;
use Exception;

class InternalGateway extends PaymentGateway
{    
    // public function onCallbackSuccess($order, $data = null)
    // {
    //     $order->items->each(function($item, $key) use ($appointmentService) {
    //         // THE Booking has already been "RESERVED"
    //         // SO switch that to "BOOKED"
    //         $item->object->update([
    //             'status' => $item->object::STATUS_BOOKED
    //         ]);
    //     });

    //     $this->statusCode     = 'completed';
    //     $this->detail         = 'successful callback';
    //     $this->transactionId  = $data->transactionId;
    // }

    public function onCharge($order)
    {
        $appointmentService = app(\Sunnydevbox\Recoveryhub\Services\AppointmentService::class);


        // Just convert the status to approved
        $this->statusCode = $this->status = 'completed';
        $this->detail = '';

        /** ! THIS should be in a listener 
         * Convert RESERVED to BOOKED
        */
        
        $order->items->each(function($item, $key) use ($appointmentService) {
            if (!$item->object->bookings) {
                $appointment = $appointmentService->placeAppointment($item->object);
            }

            // THE Booking has already been "RESERVED"
            // SO switch that to "BOOKED"
            // MAKE sure it's an EVENT model
            if (get_class($item->object) == config('recoveryhub.models.event')) {
                $item->object->update(['status' => $item->object::STATUS_BOOKED]);
            }
        });

        return true;
    }
}