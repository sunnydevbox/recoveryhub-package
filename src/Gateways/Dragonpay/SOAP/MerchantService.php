<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay\SOAP;

use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\Dragonpay;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\AbstractBaseDragonPay;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\SOAP\SOAPActionInterface;

class MerchantService extends AbstractBaseDragonPay implements SOAPActionInterface
{
    public $isSoap = true;
    protected $dragonpay = null;

    public function serviceEndpoint()
    {
        return 'DragonPayWebService/MerchantService.asmx?wsdl';
    }

    public function params()
    {
        return [
            'merchantid' => $this->dragonpay->getMerchantId(),
            'password' => $this->dragonpay->getMerchantKey(),
            'amount' => -1000
        ];
    }


    public function __construct(
        Dragonpay $dragonpay
    ) {
        $this->dragonpay = $dragonpay;
    }
}
