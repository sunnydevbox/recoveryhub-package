<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay\SOAP;

interface SOAPActionInterface
{
    public function serviceEndpoint();
    // public function op();
    // public function txnid();
    public function params();
}