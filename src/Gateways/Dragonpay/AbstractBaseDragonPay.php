<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay;

use GuzzleHttp\Client;
use SoapClient;

abstract class AbstractBaseDragonPay
{
    public function _call($interface, $queyrString = [])
    {
        if (isset($this->isSoap) && $this->isSoap) {
            // FIX THISSS!
            dd(1);
            $url = config('recoveryhub.dragonpay_api_url') . $this->serviceEndpoint();
            $soapClient = new SoapClient($url);

            $processors = $soapClient->GetAvailableProcessors($this->params());
            if (property_exists($processors, 'GetAvailableProcessorsResult')) {
                dd(1);
                return $processors->GetAvailableProcessorsResult->ProcessorInfo;

            }

            dd(2);

            // dd($soapClient->GetAvailableProcessors($this->params())->GetAvailableProcessorsResult);

        } else {
            
            if ($op = $this->op()) {
                $accountCreds['op'] = $op;
            }

            if ($params = $this->params()) {
                
                // $accountCreds = array_merge($accountCreds, $params);
                // $digest = $accountCreds;
                // unset($digest['merchantpwd']);
                // $digest['merchantpwd'] = $interface->getMerchantKey();

                // $forDigest = implode(':', $digest);
                
                // $accountCreds['digest'] = sha1($forDigest);
                // $accountCreds['email'] = ($accountCreds['email']);    
                // dd($params);

                $qstring = http_build_query($params);
            }
        
            
            $url = config('recoveryhub.dragonpay_api_url') . $this->serviceEndpoint()  . '?' . $qstring;
            return $url;
            dd($url);
            $client = new Client();

            

            $response = $client->request('GET', $url, ['query' => $params ]);
            $code =  $response->getStatusCode();
            // "200"
            $header = $response->getHeader('content-type')[0];
            // 'application/json; charset=utf8'
            $body = $response->getBody();
            // echo $url;
            // dd($params, $url, $code, $header, $body);
            return $url;
        }
    }


    public function digest($data = [])
    {
        $digest = implode(':', $data);
        
        // echo $digest;
        $hashed = sha1($digest);

        return $hashed;
    }
}