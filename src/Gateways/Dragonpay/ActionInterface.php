<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay;

interface ActionInterface
{
    public function serviceEndpoint();
    public function op();
    public function txnid();
    public function params();
}