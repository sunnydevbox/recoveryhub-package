<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay;

use Ixudra\Curl\CurlService;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\AbstractBaseDragonPay;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\ActionInterface;

class CheckTransaction extends AbstractBaseDragonPay implements ActionInterface
{
    protected $txnid = null;

    public function txnid()
    {
        return $this->txnid;
    }

    public function op()
    {
        return 'GETSTATUS';
    }

    public function serviceEndpoint()
    {
        return 'MerchantRequest.aspx';
    }

    public function params()
    {
        return null;
    }

    public function __construct($txnid)
    {
        $this->txnid = $txnid;
    }

}