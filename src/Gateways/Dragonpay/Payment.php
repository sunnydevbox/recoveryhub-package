<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay;

use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\AbstractBaseDragonPay;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\ActionInterface;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\Dragonpay;

class Payment extends AbstractBaseDragonPay implements ActionInterface
{
    protected $txnid = null;
    protected $dragonpay = null;

    public function txnid()
    {
        return $this->txnid;
    }

    public function op()
    {
        return null;
    }

    public function serviceEndpoint()
    {
        return 'Pay.aspx';
    }

    public function params()
    {
        $digestParamsFields = ['amount','ccy','description','email'];

        
        $forDigest = [
            'merchantid' => $this->dragonpay->getMerchantId(),
            'txnid' => $this->txnid(),
        ];

        $forDigest = array_merge($forDigest, collect($this->params)->only($digestParamsFields)->all());

        // $digest will be SHA1'd
        $forDigest['merchantpwd'] = $this->dragonpay->getMerchantKey();
        $data = $forDigest;
        $data['digest'] = $this->digest($forDigest);

        $data = array_merge($data, collect($this->params)->except($digestParamsFields)->all());

        // dd($data);
        return $data;
    }

    public function __construct(
        Dragonpay $dragonpay,
        $txnid,
        $params = []
    ) {
        $this->dragonpay = $dragonpay;
        $this->txnid = $txnid;

        // VALIDATE $params email, ccy, amount, description,

        $this->params = $params;
    }

}