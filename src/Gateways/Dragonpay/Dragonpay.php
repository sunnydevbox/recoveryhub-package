<?php
namespace Sunnydevbox\Recoveryhub\Gateways\Dragonpay;

use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\SOAP\MerchantService;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\CheckTransaction;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\Payment;

class Dragonpay
{
    protected $merchantId;
    protected $merchantKey;

    public function checkStatus($txnid)
    {
        $transaction = new CheckTransaction($txnid);

        return $this->action($transaction);
    }

    public function payment($txnid, $params =[])
    {
        $transaction = new Payment($this, $txnid, $params);

        return $this->action($transaction);
    }

    public function action($interface)
    {
        return $interface->_call($this);
    }


    public function getMerchantId()
    {
        return $this->merchantId;
    }

    public function getMerchantKey()
    {
        return $this->merchantKey;
    }

    public function merchantService()
    {
        $m = new MerchantService($this);

        return $this->action($m);
    }


    public function __construct(
        $merchantId = null,
        $merchantKey = null
    ) {
        $this->merchantId = $merchantId;
        $this->merchantKey = $merchantKey;
    }
}