<?php
namespace Sunnydevbox\Recoveryhub\Gateways;

use Sunnydevbox\TWCart\Core\PaymentGateway;
use Sunnydevbox\Recoveryhub\Gateways\Dragonpay\Dragonpay;
use Amsgames\LaravelShop\Exceptions\CheckoutException;
use Amsgames\LaravelShop\Exceptions\GatewayException;
use Amsgames\LaravelShop\Exceptions\ShopException;
use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
use Exception;
use Carbon\Carbon;
use Sunnydevbox\Recoveryhub\Models\Appointment;

class DragonpayGateway extends PaymentGateway
{    
    var $apiUrl = null;
    var $merchantId = null;
    var $merchantKey = null;
    var $dragonpay = null;

    public function onCallbackSuccess($order, $data = null)
    {    
        $appointmentService = app(\Sunnydevbox\Recoveryhub\Services\AppointmentService::class);
        $cartTransactionService = app(\Sunnydevbox\Recoveryhub\Services\CartTransactionService::class);

        
        $token = $data['param1'];
        $detail = serialize(explode(':', $data['message']));
        $gw = 'dragonpay';
        $txnid = $data['txnid'];
        
        $transaction = $cartTransactionService->repository->with(['order'])->getByTxnid($txnid);
        
        if (!$transaction) {
            throw new Exception('invalid_transaction_id', 400);
        }
        $order = $transaction->order;

        \Log::info('callback from Dragonpay', [
            'params' => $data,
        ]);

        try {

            $order->placeTransaction($gw, $txnid, $detail, $token);
            $cartTransactionService->updateTransaction($txnid, $data);
            
            $action = 'default';

            switch(strtoupper($data['status'])) {
                case 'S':
                    $action = 'book';
                break;

                case 'F':
                case 'U':
                    $action = 'unbook';
                break;
            }

            // \Log::info('cpay', [
            //     'params' => $data,
            //     'action' => $action,
            // ]);

            if ($action == 'book') {
                $order->items->each(function($item, $key) use ($appointmentService) {
                    if (is_numeric(strpos($item->class, 'Models\\Event'))) {
                        // THE Booking has already been "RESERVED"
                        // SO switch that to "BOOKED"
                        if($item->object->isReserved()) {
                            $item->object->update([
                                'status' => $item->object::STATUS_BOOKED
                            ]);

                            event(new AppointmentCreatedEvent($item->object->bookings));
                        }
                    }
                });
            } else if ($action == 'unbook') {
                $order->items->each(function($item, $key) use ($appointmentService) {

                    if (is_numeric(strpos($item->class, 'Models\\Event'))) {
                        // 1) DELETE THE APPOINTMENT
                        if ($item->object->bookings) {
                            $item->object->bookings->delete();
                        }
                        
                        // 2) SWITCH EVENT status from 'RESERVED' to 'OPEN'
                        $item->object->update([
                            'status' => $item->object::STATUS_OPEN
                        ]);
                    }
                });
            } else {
                
            }

            
            $messageCode = $message = 'ok';
            $data = $transaction->order;
            $statusCode = 200;

            return $transaction->order;

        } catch(\Exception $e) {

            switch($e->getMessage()) {
                case 'already_booked':
                    $data = $transaction->order;
                    $messageCode = $e->getMessage();
                break;
            }
        }

        return true;
    }

    public function onCallbackSuccess_CLI_Dragonpay_check_status($order, $data = null)
    {    
        
        $appointmentService = app(\Sunnydevbox\Recoveryhub\Services\AppointmentService::class);
        $cartTransactionService = app(\Sunnydevbox\Recoveryhub\Services\CartTransactionService::class);

        $token = $data['param1'];
        $detail = serialize(explode(':', $data['message']));
        $gw = 'dragonpay';
        $txnid = $data['txnid'];
        
        $transaction = $cartTransactionService->repository->with(['order'])->getByTxnid($txnid);
        
        if (!$transaction) {
            throw new Exception('invalid_transaction_id', 400);
        }
        $order = $transaction->order;

        \Log::info('Response from Dragonpay status check', [
            'params' => $data,
        ]);

        try {
            
            $order->placeTransaction($gw, $txnid, $detail, $token);
            $cartTransactionService->updateTransaction($txnid, $data);
            
            $order->items->each(function($item, $key) use ($appointmentService) {
                if (is_numeric(strpos($item->class, 'Models\\Event'))) {
                    // 1) DELETE THE APPOINTMENT
                    if ($item->object->bookings) {
                        \Log::info('going to delete booking');

                        /**
                         * Deleting this booking this way because 
                         * ->delete() triggers the AppointmentObserver 
                         * which has it's own email to send.
                         */
                        /*
                         * Eager loaded $item->object->bookings
                         * does not allow fillable()
                         */
                        $app = Appointment::find($item->object->bookings->id);
                        $app->deleted_at = Carbon::now();
                        $app->save();
                    }
                    
                    
                }
            });
            
            $messageCode = $message = 'ok';
            $data = $transaction->order;
            $statusCode = 200;

            return $transaction->order;

        } catch(\Exception $e) {

        }

        return true;
    }

    public function onCheckout($cart)
    {
        // DO any validations of the cart items here
        $billingInfoFields = [
            'firstName', 'lastName', 'middleName', 
            'address', 'city', 'state', 'country', 
            'zip', 'telno',
        ];

        if (!$cart->items->count()) {
            throw new \Exception('order_empty');
            return false;
        }

        try {
            $requireBillingInfo = false;
            //  CHECK BILLING INFO
            if (request()->get('type') == 'cc') {
                $requireBillingInfo = true;
            }
            
            if ($requireBillingInfo && $billingInfo = collect(request()->get('billingInfo'))) {
                $missingBillingInfoFields = [];
                foreach($billingInfoFields as $key) {
                    if (!$billingInfo->has($key)) {
                        $missingBillingInfoFields[] = $key;
                    }
                }

                if (!empty($missingBillingInfoFields)) {
                    throw new CheckoutException('Invalid fields ('. join(', ' , $missingBillingInfoFields).')', 1);
                }
            }
        } catch (Exception $e) {
            throw new CheckoutException($e->getMessage());


        }
        
        return true;
    }


    public function onCharge($order)
    {
        if (!$order->items->count()) {
            throw new \Exception('order_empty');
            return false;
        }

        $paymentChannel = [];
        $apiData = [];
        $proceed = false;
        $requireBillingInfo = false;

        switch(request()->get('type')) {
            case 'gcash': 
                $paymentChannel['mode'] = 128;
                $paymentChannel['procid'] = 'GCSH';
                $proceed = true;
            break;

            case 'cc':
                $paymentChannel['mode'] = 64;
                $paymentChannel['procid'] = 'CC';
                $proceed = true;
                $requireBillingInfo = true;
            break;

            case 'otc-nb':
                if (config('recoveryhub.dragonpay_mode') == 'TEST') {
                    // Bogus Bank and Bogus OTC are in 
                    // modes 1 and 2 (3). If you want 1, 2, and 4, 
                    // then put 7.
                    $paymentChannel['mode'] = 7;
                } else {
                    $paymentChannel['mode'] = 4;
                    // $paymentChannel['procid'] = 'BAYD';
                    // BAYD, LBC, SMR, CEBL, MLH, RDS, ECPY 
                }

                $proceed = true;
            break;

            case 'bol':
                $paymentChannel['mode'] = 1;
                // $paymentChannel['procid'] = 'BOL';
                $proceed = true;
                $requireBillingInfo = true;
            break;
        }

        $apiURL = config('recoveryhub.dragonpay_rest_api_url');
        
        if ($proceed) {
            // START processing payment
            $apiData = array_merge($apiData, [
                'Amount' => number_format($order->total, 2, '.', ''),
                'Currency' => 'PHP',
                'Description' => 'Cebu Metropsych payment',
                'Email' => \Auth::user()->email,
                'Param1' => $this->token,
            ]);

            // SHOULD BE FROM REQUEST
            if ($billingInfo = request()->get('billingInfo')) {                
                $apiData['BillingDetails'] = [
                    'FirstName' => $billingInfo['firstName'],
                    'MiddleName' => $billingInfo['middleName'],
                    'LastName' => $billingInfo['lastName'],
                    'Address1' => $billingInfo['address'],
                    'Address2' => $billingInfo['address2'],
                    'City' => $billingInfo['city'],
                    'State' => $billingInfo['state'],
                    'Country' => $billingInfo['country'],
                    'ZipCode' => $billingInfo['zip'],
                    'TelNo' => $billingInfo['telno'],
                    'Email' => \Auth::user()->email
                ];
            }
            
            $client = new \GuzzleHttp\Client();
            $request = $client->post(
                "{$apiURL}{$this->transactionId}/post" , 
                [
                    'form_params' => $apiData,
                    'auth' => [
                        config('recoveryhub.dragonpay_merchant_id'),
                        config('recoveryhub.dragonpay_merchant_key')
                    ]
                ])
            ;

            $response = json_decode($request->getBody(), true);

            $this->statusCode = 'pending';
            $this->detail = [
                'dragonpayResponse' => $response,
                'url' => $response['Url'] . '&' . http_build_query($paymentChannel),
                'billingInfo' => request()->get('billingInfo', []),
                'apiData' => $apiData,
            ];

            return true;
        }

        $this->detail[] = 'Payment method';
        return false;
    }

    public function __construct($id = '')
    {
        parent::__construct($id);

        $this->merchantId = config('recoveryhub.dragonpay_merchant_id');
        $this->merchantKey = config('recoveryhub.dragonpay_merchant_key');
        $this->apiUrl = config('recoveryhub.dragonpay_api_url');

        $this->dragonpay = new Dragonpay(config('recoveryhub.dragonpay_merchant_id'), config('recoveryhub.dragonpay_merchant_key'));
    }
}