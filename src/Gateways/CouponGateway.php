<?php
namespace Sunnydevbox\Recoveryhub\Gateways;

use Sunnydevbox\TWCart\Services\Gateways\CouponGateway as CouponGatewayOriginal;
use Sunnydevbox\Recoveryhub\Services\AppointmentService;

class CouponGateway extends CouponGatewayOriginal
{
    public function onCallbackSuccess($order, $data = null)
    {
        parent::onCallbackSuccess($order, $data);
     
        // create the booking
        $appointmentService = app(\Sunnydevbox\Recoveryhub\Services\AppointmentService::class);

        foreach($order->items as $item) {
            $appointmentService->placeAppointment($item->object);
        }
    }
} 