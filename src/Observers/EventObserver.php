<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Models\Event;

class EventObserver
{
   // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(Event $event)
    {
        $event->sku = 'SKU-Ev-' . $event->id;
        $event->tax = 0.12;
        $event->save();
    } 
}