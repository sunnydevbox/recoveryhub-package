<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Models\EventFeedback;
use Sunnydevbox\Recoveryhub\Events\EventPrescriptionCreatedEvent;

class EventFeedbackObserver
{
   // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(EventFeedback $eventFeedback)
    {
        // ASSUMING the an EVENt will always have ONLY 1 prescription
        //foreach($eventFeedback->event->prescriptions as $prescription) {
            event(new EventPrescriptionCreatedEvent(
                $eventFeedback->event
                    ->prescriptions()
                    ->with(['items.medicine', 'event.bookings.patient', 'event.doctor'])
                    ->first()
            ));
        //}
        
    } 
}