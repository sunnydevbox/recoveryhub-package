<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Events\PatientAccountCreatedEvent;
use Sunnydevbox\Recoveryhub\Models\User;

class PatientObserver
{
   // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $patient)
    {
        event(new PatientAccountCreatedEvent($patient));
    }
}