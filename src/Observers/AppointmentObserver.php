<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Models\Appointment;
use Sunnydevbox\Recoveryhub\Events\AppointmentDeletedEvent;

class AppointmentObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created()
    {
        
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting()
    {
        
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(Appointment $appointment)
    {
        event(new AppointmentDeletedEvent($appointment));
    }

    public function updated() 
    {

    }

    public function saving() 
    {
        
    }
}