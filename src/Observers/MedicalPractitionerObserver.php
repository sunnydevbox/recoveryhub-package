<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Events\DoctorAccountCreatedEvent;
use Sunnydevbox\Recoveryhub\Models\MedicalPractitioner;

class MedicalPractitionerObserver
{
   // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(MedicalPractitioner $medicalPractitioner)
    {   
        event(new DoctorAccountCreatedEvent($medicalPractitioner));
    }
}