<?php
namespace Sunnydevbox\Recoveryhub\Observers;

use Sunnydevbox\Recoveryhub\Models\ContactFormSubmission;
use Sunnydevbox\Recoveryhub\Events\ContactFormSubmittedEvent;

class ContactFormSubmissionObserver
{
    // creating, created, updating, updated, saving, saved, deleting, deleted,  restoring, restored

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(ContactFormSubmission $contactFormSubmission)
    {
        event(new ContactFormSubmittedEvent($contactFormSubmission));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleting(ContactFormSubmission $contactFormSubmission)
    {
        // event(new \Sunnydevbox\TWPim\Events\EmployeeDeletingEvent($employee));
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  Employee  $employee
     * @return void
     */
    public function deleted(ContactFormSubmission $contactFormSubmission)
    {
    
    }

    public function updated(ContactFormSubmission $contactFormSubmission) 
    {

    }

    public function saving(ContactFormSubmission $contactFormSubmission) 
    {
        
    }
}