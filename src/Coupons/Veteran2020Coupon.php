<?php
namespace Sunnydevbox\Recoveryhub\Coupons;

class Veteran2020Coupon extends AbstractCoupon
{
    public function verify()
    {
        
        /**
         * Meralco's rule:
         * - applicable to specific doctors
         * 
         */

        // Allowed doctors
        $doctors = collect([
            266,
            267,
            // 1358,
        ]);

        $status = true;
        
        switch(strtolower($this->coupon->code)) {
            case 'rhvet-3451-1':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-5t69-2':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-c690-3':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-rh68-4':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-j4sf-5':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-qpr8-6':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-91ks-7':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-pl7c-8':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-fx40-9':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
            case 'rhvet-zsq8-10':
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;

            default: 
                $doctors = collect([
                    267, 266, 270, 268
                ]);
            break;
        }

        $this->cart->items->each(function($item) use ($doctors, &$status) {

            if ($this->coupon->item_type == 'both' || $this->coupon->item_type == 'appointment') {
            
                preg_match('/(Models\\\Event)/', $item->class, $matches);
            
            } else if ($this->coupon->item_type == 'both' || $this->coupon->item_type == 'medicine') {
            
                // preg_match('/(Models\\\Event)/', $item->class, $matches);
            
            }
            
            
            if (count($matches)) {
                $doctorId = $item->object->doctor->id;

                // CHeck if doctor in cart item is allowed
                if ($doctors->contains($doctorId)) {
                        
                } else {
                    $status *= false;
                }
            }

            // dd($item->class);
        });

        if (count($doctors)) {

        }

        return $status ? true : false;
        // dd('verify', $status);
    }

    public function processor()
    {

        dd($this->cart);
    }
}