<?php
namespace Sunnydevbox\Recoveryhub\Coupons;

abstract class AbstractCoupon
{

    protected $expiration;
    protected $name;
    protected $description;
    protected $cart;
    protected $coupon;

    /**
     * Does the calculation.
     */
    abstract public function processor();

    /**
     * Checks if coupon is applicable to ALL items.
     */
    abstract public function verify();

    public function cart($cart)
    {
        $this->cart = $cart;
    }

    public function coupon($coupon)
    {
        $this->coupon = $coupon;
    }
}