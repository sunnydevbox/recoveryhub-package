<?php
namespace Sunnydevbox\Recoveryhub\Coupons;

class MeralcoCoupon extends AbstractCoupon
{
    public function verify()
    {
        /**
         * Meralco's rule:
         * - applicable to specific doctors
         * 
         */

        // Allowed doctors
        $doctors = collect([
            266,
            267,
            // 1358,
        ]);

        $status = true;


        $this->cart->items->each(function($item) use ($doctors, &$status) {

            if ($this->coupon->item_type == 'both' || $this->coupon->item_type == 'appointment') {
            
                preg_match('/(Models\\\Event)/', $item->class, $matches);
            
            } else if ($this->coupon->item_type == 'both' || $this->coupon->item_type == 'medicine') {
            
                // preg_match('/(Models\\\Event)/', $item->class, $matches);
            
            }
            
            
            if (count($matches)) {
                $doctorId = $item->object->doctor->id;

                // CHeck if doctor in cart item is allowed
                if ($doctors->contains($doctorId)) {
                    
                } else {
                    $status *= false;
                }
            }

            // dd($item->class);
        });

        if (count($doctors)) {

        }

        return $status ? true : false;
        // dd('verify', $status);
    }

    public function processor()
    {

        dd($this->cart);
    }
}