<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWBookings\Http\Controllers\API\V1\BookingController;
use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class UserAppointmentController extends BookingController
{
	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\AppointmentValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\UserAppointmentTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\AppointmentService $service
	) {
        $this->repository 	= $repository;
        $this->validator  	= $validator;
		$this->transformer 	= $transformer;
		$this->service 		= $service;
	}


	public function cancel(Request $request, $id)
	{
		$message = '';
		$code = null;
		$data = [];

		try {
			$result = $this->service->cancel($id);
			$code = 200;
			$message = 'ok';			
		} catch (ModelNotFoundException $e) {
			$message = 'invalid_booking_id';
			$code = 400;
		} catch (Exception $e) {
			$message = $e->getMessage();
			$code = $e->getCode();
		}


		return response()->json([
			'message' => $message,
			'code' => $code,
		], $code);
	}

	public function store(Request $request)
	{
		 try {
           	//$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
		   	$result = $this->service->placeAppointment($request->all());
						
			return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}
}