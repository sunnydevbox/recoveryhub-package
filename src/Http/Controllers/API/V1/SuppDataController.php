<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Dingo\Api\Http\Request;

/**
 * Supplementary data.
 *
 * @Resource("Supplementary Data", uri="/suppdata")
 */
class SuppDataController extends Controller
{
    /**
     * Data list
     *
     * Returns data based upon request
     *
     * @Get("/")
     * @Versions({"v1"})
     * 
     * @Transaction({
     *      @Response(200),
     * })
     * 
     * @Parameters({
     *      @Parameter("data", type="string", required=true, description="Separate items by semicolon (;)", example="medicines;medicine-generic-names")
     * })
     */
    public function index(Request $request)
    {
        return $this->service->_suppData($request);
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Services\SupplementDataService $supplementDataService
    ) {
        $this->service = $supplementDataService;
    }
}