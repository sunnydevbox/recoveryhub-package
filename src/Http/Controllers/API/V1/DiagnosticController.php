<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Dingo\Api\Http\Request;

class DiagnosticController extends Controller
{


    public function __construct(
        \Sunnydevbox\Recoveryhub\Services\DiagnosticService $diagnosticService
    ) {
        $this->service = $diagnosticService;
    }

    public function getList(Request $request)
    {
        return $this->service->getList($request->all());
    }

}