<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

// use Sunnydevbox\TWCart\Http\Controllers\CartController as CCartController;
use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

/**
 * User resource representation.
 *
 * @Resource("Cart", uri="/cart")
 */
class CartItemController extends APIBaseController
{
    public function index(Request $request)
    {
        if ($request->has('csv')) {
     
            $limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);
		
            if ($limit == 0) {
                $result = $this->repository->all();

                return $this->response()
                        ->collection($result, $this->transformer)
                        ->withHeader('Content-Range', $result->count());
            } else {
                // $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria::class);
                $result = $this->repository->paginate($limit);
                $list = [
                    ['Consultation Date', 'Doctor', 'Patient', 'Transaction #', 'Transaction Date', 'Order Status', 'Coupon Applied', 'Coupon Discount Value', 'Professional Fee', 'VAT', 'Withholding', 'Admin Fee', 'Net Income'],
                ];

                $fp = fopen('/var/www/html/recoveryhub/file.csv', 'w');

            
                foreach ($result->items() as $row) {
                    // dd($row->toArray());
                    $list[] = [
                        ($row->sku . ' - ') . (($row->object) ? $row->object->start_at->format('M d, Y') . ' - ' . $row->object->start_at->format('h:ia') . '-'. $row->object->end_at->format('h:ia') : ''),
                        ($row->object) ? 'Dr. ' .$row->object->doctor->first_name . ' ' . $row->object->doctor->last_name : '',
                        ($row->object && $row->object->bookings && $row->object->bookings->patient) ? $row->object->bookings->patient->first_name . ' ' . $row->object->bookings->patient->last_name : '',
                        ('Order# ' . ($row->order) ? $row->order->id : '###')  . ' - TXN# ' . (($row->order && count($row->order->transactions)) ? $row->order->transactions[0]->transaction_id : '###'),
                        $row->order->created_at->format('M d, Y') . ' ' . $row->order->created_at->format('h:ia'),
                        $row->order->statusCode,
                        ($row->order && count($row->order->coupons)) ? $row->order->coupons[0]->coupon->code : '-none-',
                        ($row->order && count($row->order->coupons)) ? ($row->order->coupons[0]->coupon->value) ? $row->order->coupons[0]->coupon->value : ($row->order->coupons[0]->coupon->discount) ? '%'. $row->order->coupons[0]->coupon->discount : '-none-' : '-none-', 
                        'Php '. number_format($row->professional_fee, 2),
                        'Php '. number_format($row->tax_amount, 2),
                        'Php '. number_format($row->withholding, 2),
                        'Php '. number_format($row->admin_fee, 2),
                        'Php '. number_format($row->net_income, 2),
                    ];
                }

                foreach ($list as $fields) {
                    fputcsv($fp, $fields);
                }
                // dd($list);
                

                fclose($fp);

                dd($result->total());
                // return $this->response
                //     ->paginator($result, $this->transformer)
                //     ->withHeader('Content-Range', $result->total());
            }
        } else {
            return parent::index($request);
        }

    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartItemRepository $repository,
        \Sunnydevbox\Recoveryhub\Transformers\CartItemTransformer $transformer
    ) {
        // $this->cartService = $cartService;
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\HasScope::class);
        $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\HasCriteria::class);
        $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\HasFilter::class);
        $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\CartItemCriteria::class);
        
        // $this->cartTransactionService = $cartTransactionService;
    }
}