<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

use Auth;

class PatientController extends APIBaseController
{

	public function myPatients(Request $request)
	{
		$filtered = $this->repository->getCriteria()->reject(function ($value, $key) {
			if (strpos(strtolower(get_class($value)), 'requestcriteria')) {
				$this->repository->popCriteria($value);
				return true;
			}

			return false;
		});
		
		$this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\MyPatientsCriteria::class);
		return $this->index($request);
	}

	public function store(Request $request)
	{
		$statusCode = 400;
		$data = [
			'status_code'   => $statusCode,
			'message' => []
		];
		
		try {
			
		    $result = $this->service->store( $request->all() );

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {
			$data = [
				'status_code'   => 400,
				'message' => $e->getMessageBag()
			];
		}
		

		return response()->json($data, 400); 
	}

	public function index(Request $request)
	{	
		$currentUser = Auth::user();

		if (!$currentUser->hasRole(['medical-practitioner', 'psychologist'])) {
			$this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\AdminUserCriteria::class);
		}

		return parent::index($request);
	}

    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Patient\PatientRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\PatientValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\PatientTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\PatientService $service
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;
		$this->repository->pushCriteria(\Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria::class);
	}
	
}