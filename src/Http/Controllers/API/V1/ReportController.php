<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Sunnydevbox\Recoveryhub\Services\EventNoteService;
use File;
use Response;

class ReportController extends Controller
{
	public function generate(Request $request)
	{
		$location = $this->reportService->generate($request->all());
		$file = File::get($location);
		$response = Response::make($file, 200);
		$type = File::mimeType($location);
		$response->header("Content-Type", $type);
		return $response;
	}

	public function sendEmail(Request $request)
	{
		$res = $this->reportService->sendEmailPdf($request->all());

		return $res;
	}

	public function __construct(\Sunnydevbox\Recoveryhub\Services\ReportService $reportService) {
        $this->reportService = $reportService;
	}	
}