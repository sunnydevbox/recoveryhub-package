<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Services\ContactFormSubmissionService;
use Dingo\Api\Http\Request;

class ContactFormSubmissionController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\ContactFormSubmission\ContactFormSubmissionRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\ContactFormSubmissionValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\ContactFormSubmissionTransformer $transformer,
		ContactFormSubmissionService $service
	) {
        $this->repository = $repository;
        $this->validator = $validator;
	$this->transformer = $transformer;
	$this->service = $service;
	}

	public function store(Request $request)
	{
		$result = $this->service->store($request->all());
		return $this->response->item($result, $this->transformer);
	}
}