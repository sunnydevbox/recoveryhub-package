<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class MedicineGenericNameController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineGenericNameRepository $repository, 
		\Sunnydevbox\Recoveryhub\Transformers\MedicineGenericNameTransformer $transformer
	) {
		$this->repository = $repository;
		$this->transformer = $transformer;
	}
}