<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class UserProfileQuestionAnswerController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion\UserProfileQuestionAnswerRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\UserProfileQuestionAnswerValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\UserProfileQuestionAnswerTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}
}