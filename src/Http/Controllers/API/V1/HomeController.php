<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class HomeController extends APIBaseController
{
	public function index(Request $request)
	{
		\Log::info('API HOME', ['headers' => $request->header()]);
	}

    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository $repository, 
		\Sunnydevbox\Recoveryhub\Transformers\UserAppointmentTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\AppointmentService $service,
		\Sunnydevbox\Recoveryhub\Transformers\MyAppointmentTransformer $myAppointmentTransformer
	) {
		$this->repository = $repository;
		$this->transformer = $transformer;
		$this->service = $service;
		$this->myAppointmentTransformer = $myAppointmentTransformer;
	}
}