<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class PrescriptionTransactionController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\PrescriptionTransaction\PrescriptionTransactionRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\PrescriptionTransactionValidator $validator,
		\Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService $service,
		\Sunnydevbox\Recoveryhub\Transformers\PrescriptionTransactionTransformer $transformer
	) {
		// dd(1);
        $this->repository = $repository;
        $this->validator  = $validator;
				$this->transformer = $transformer;
				$this->service = $service;
	}

	public function store(Request $request)
	{
		return $this->service->store($request->all());
		// return 
	}

    // public function update(Request $request, $id)
    // {
    //     $result = $this->service->update($request->all(), $id);
    //     return $this->response->item($result, $this->transformer);
		// }

	public function updateStatus($id, $status)
	{

		return $this->service->updateTransactionStatus($id, $status);

	}
		
}