<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Dingo\Api\Http\Request;
use Sunnydevbox\TWCart\Http\Controllers\API\V1\CartController as CCartController;

/**
 * User resource representation.
 *
 * @Resource("Cart", uri="/cart")
 */
class CartController extends CCartController
{
    public function getContent()
    {   
        $data = $this->cartService->content2();

        return $this->response->array($data);
    }

    public function deleteCartItem(Request $request)
    {
        // dd('this');
        $result = $this->cartService->removeItem($request->all());

        return $this->response->noContent();
    }

    public function checkout(Request $request)
    {
        $this->cartService->checkout();
        // $this->cartService
        //     ->setGateway($request->get('gateway'))
        //     ->checkout($request->all());

        return $this->response->noContent();
    }

    public function placeOrder(Request $request)
    {
        try {
            if (isset($this->cartPaymentValidator) && $this->cartPaymentValidator) {
                $billingInfo = $request->get('billingInfo');
				$this->cartPaymentValidator->with($billingInfo)->passesOrFail('BILLING_INFO');
            }

            $validDragonpayGateways = [
                'gcash', 'cc', 'otc-nb', 'bol',
            ];
            
            $gateway = null;
           
            if (
                $request->get('gateway') == 'dp' 
                && gettype(array_search($request->get('type'), $validDragonpayGateways)) == 'integer' 
            ) {
                $gateway = 'dragonpay';
            }

            $result = $this->cartService
                ->setGateway($gateway)
                ->placeOrder($request->all());
            $transaction = $this->cartTransactionService->repository->getByTxnid($result['transaction_id']);

            return $this->response->array([
                'transaction_id' => $transaction->transaction_id,
                'gateway' => $transaction->gateway,
                'detail' => $transaction->detail,
                'token' => $transaction->token,
                'date' => $transaction->created_at,
                'status' => $transaction->status,
            ]);
        } catch(ValidatorException $e) {

            /** TEMPORARY.
             * Front-end shouldbe able to handle multi-dimensional array
            */
            $errors = [];

            foreach($e->getMessageBag()->toArray() as $field => $messages) {                
                
                $errors = array_merge($errors, $messages);
            }
            $message = join(' ', $errors);
            

            return response()->json([
                'message' =>  $message,
                'code' => 400,
            ], 400);

        } catch(\Exception $e) {
            $code  = ($e->getCode()) ? $e->getCode() : 400;
            return response()->json([
                'message' => $e->getMessage(),
                'code' => $code,
            ], $code);
        }
    }

    public function __construct(
        \Sunnydevbox\TWCart\Transformers\CartTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\CartService $cartService,
        \Sunnydevbox\Recoveryhub\Services\CartTransactionService $cartTransactionService,
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartRepository $repository,
        \Sunnydevbox\Recoveryhub\Validators\CartPaymentValidator $cartPaymentValidator
    ) {
        $this->cartService = $cartService;
        $this->transformer = $transformer;
        $this->repository = $repository;
        $this->cartTransactionService = $cartTransactionService;
        $this->cartPaymentValidator = $cartPaymentValidator;
    }
}