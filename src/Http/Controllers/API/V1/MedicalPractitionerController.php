<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use \Prettus\Validator\Exceptions\ValidatorException;
use \Prettus\Validator\Contracts\ValidatorInterface;

class MedicalPractitionerController extends APIBaseController
{
	public function myDoctors(Request $request)
	{
		$filtered = $this->repository->getCriteria()->reject(function ($value, $key) {
			if (strpos(strtolower(get_class($value)), 'requestcriteria')) {
				$this->repository->popCriteria($value);
				return true;
			}

			return false;
		});

		$this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\MyDoctorsCriteria::class);
		return $this->index($request);
	}

    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\MedicalPractitioner\MedicalPractitionerRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\UserValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\MedicalPractitionerTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\MedicalPractitionerService $service
	) {
		$this->repository = $repository;
		$this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;
	}

	public function store(Request $request)
	{
		$params = collect($request->all());
		$rules = ValidatorInterface::RULE_CREATE;

		if ($params->has('doctorRole')) {
			$rules = 'CREATE_WITH_DOCTOR_ROLE';
		}

		try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($params->all())->passesOrFail($rules);
			}

		    $result = $this->service->store($params->all());

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function index(Request $request)
	{

		$this->repository->popCriteria($this->repository->getCriteria()->first(function($value, $key){
			return (strpos(get_class($value), '\Request'));
		}));

		// dd($this->repository->getCriteria());
		
		return parent::index($request);
	}

	public function update(Request $request, $id)
	{

		try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
			}

			if ($request->get('password')){
				$result = $this->repository->update($request->all(), $id);
			} else {
				$result = $this->repository->update($request->except(['password']), $id);

				if ( $request->get('doctorRole')  ) 
				$this->service->updateRoles($request->get('doctorRole'), $result);

			}
           
            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}
}