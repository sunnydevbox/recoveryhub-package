<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;
use Dingo\Api\Http\Request;

class ProfileQuestionController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion\ProfileQuestionRepository $repository, 
        \Sunnydevbox\Recoveryhub\Validators\ProfileQuestionValidator $validator,
        \Sunnydevbox\Recoveryhub\Transformers\ProfileQuestionTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\ProfileQuestionService $service
    ) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->service = $service;
    }


    public function answer(Request $request)
    {
        $this->service->answer($request->all());
        return $this->response->noContent();
    }
}