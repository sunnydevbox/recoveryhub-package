<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use \Prettus\Validator\Exceptions\ValidatorException;
use Dingo\Api\Routing\Helpers;
use \Prettus\Validator\Contracts\ValidatorInterface;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;


class RateChargeController extends APIBaseController
{
    public function getByDoctor(Request $request, $id = null)
    {
        if (!$id) {
            abort(400, 'invalid_id');
        }

        $rates = $this->repository->getByDoctor($id, $request->get('day', null));
        
        if (!$rates) {
            
            return $this->updateByDoctor($request, $id);
        }

        return $this->response->item($rates, $this->transformer);
    }

    public function updateByDoctor(Request $request, $id = null)
    {   
        if (!$id) {
            abort(400, 'invalid_id');
        }

        try {
            if (isset($this->validator) && $this->validator) {
                $this->validator
                    ->with($request->all())
                    ->passesOrFail(ValidatorInterface::RULE_UPDATE);
            }

            $data = $request->intersect([
                'default', 'use_default', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday',
            ]);
            
            if ($request->has('use_default')) {
                $data['use_default'] = $request->input('use_default');
            }

            $rates = $this->repository->updateByDoctor($id, $data);

            return $this->response->noContent();
        }  catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }   
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\RateCharge\RateChargeRepository $repository,
        \Sunnydevbox\Recoveryhub\Transformers\RateChargeTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Validators\RateChargeValidator $validator
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->validator = $validator;
    }
}
