<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Sunnydevbox\Recoveryhub\Models\DiagnosisDSM5;

class DiagnosisDSM5Controller extends Controller
{
    public function fix() 
    {
        $this->service->fix();
    }

    public function root()
    {
//         $node = new DiagnosisDSM5(['label' => 'test']);
// $node->save(); // Saved as root
        $result = DiagnosisDSM5::withDepth()->having('depth', '=', 0)->get();

        return response()->json($result);
    }

    public function ancestors(Request $request)
    {
        /*
        $result = Category::ancestorsOf($id);
    $result = Category::ancestorsAndSelf($id);
    $result = Category::descendantsOf($id);
    $result = Category::descendantsAndSelf($id);
        */
        $result = DiagnosisDSM5::ancestorsOf($request->get('id'));

        return response()->json($result);
    }

    public function descendants(Request $request)
    {
        $result = DiagnosisDSM5::where('parent_id',$request->get('id'))
                ->get()
                ->toArray();

        return response()->json($result);
    }

    public function getDescendantsByLabel(Request $request)
    {
        $dsm5 = DiagnosisDSM5::where('label', $request->get('name'))
                    ->select('id', 'label')
                    ->first()
                    ->toArray();

        $descendants = DiagnosisDSM5::select('id', 'label')->ancestorsOf($dsm5['id'])->toArray();

        return response()->json([
            'source' => $dsm5,
            'descendants' => $descendants,
        ]);
    }


    public function __construct(
        // \Sunnydevbox\Recoveryhub\Repositories\Coupon\CouponRepository $repository,
		// \Sunnydevbox\Recoveryhub\Transformers\CouponTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\DiagnosisDSM5Service $service
    ) {
        // $this->repository = $repository;
        // $this->transformer = $transformer;
        $this->service = $service;
    }
}