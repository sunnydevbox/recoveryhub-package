<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

/**
 * Event Prescription
 *
 * @Resource("Event Prescription", uri="/event-prescriptions")
 */
class EventPrescriptionController extends APIBaseController
{
	/**
	 * Store
	 *
	 * Save prescription
	 *
	 * @Post("/")
	 * @Versions({"v1"})
	 * 
	 * @Transaction({
	 *      @Request({
	 *			"event_id": 1,
	 *			"notes": "Do not forget to take your medicines",
	 *			"items": {
	 *				{
	 *					"medicine_id": 1, 
	 *					"qty_tabs_dispensed": "2",
	 *					"dosage": "500mg",
	 *					"instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
	 *				}
	 *			}
	 * 		}),
	 *      @Response(200, body={
	 *			"data": {
	 *    			"event_id": 1,
	 *    			"notes": "Do not forget to take your medicines",
	 *   	 		"created_at": "2018-09-16 19:48:52",
	 *    			"id": 8,
	 * 				"items": {
	 *					{
	 *						"id": 27,
	 *           			"event_prescription_id": 8,
	 *           			"medicine_id": 1,
	 *           			"qty_tabs_dispensed": "2",
	 *           			"dosage": "500mg",
	 *           			"instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
	 *					}
	 *				}
	 * 			}
	 * 		}),
	 *  	@Response(400, body={"message": "MESSAGE_HERE","status_code": "400"}),
	 * 		@Response(500, body={"message": "MESSAGE_HERE","status_code": "500"}),
	 * })
	 * 
	 */
	public function store(Request $request)
	{
		$result = $this->service->store($request->all());

		return $this->response->item($result, $this->transformer);
	}

	public function preview(Request $request)
	{
		// dd($request);
		$result = $this->service->preview($request->all());
		return $result;
	}

	public function previewGet($event_id)
	{
		// echo 'here';
		// dd($event_id);
		$data = [
			'event_id' => $event_id
		];
		$result = $this->service->preview($data);
		return $result;
	}

	/**
	 * Update
	 *
	 * Update a prescription
	 *
	 * @Put("/{id}")
	 * @Versions({"v1"})
	 * 
	 * @Transaction({
	 *      @Request({
	 *			"event_id": 1,
	 *			"notes": "Do not forget to take your medicines",
	 *			"items": {
	 *				{
	 *					"medicine_id": 1, 
	 *					"qty_tabs_dispensed": "2",
	 *					"dosage": "500mg",
	 *					"instructions": "Twice daily. 1x morning after breakfast; 1x evening after supper"
	 *				}
	 *			}
	 * 		}),
	 *      @Response(200),
	 *  	@Response(400, body={"message": "MESSAGE_HERE","status_code": "400"}),
	 * 		@Response(500, body={"message": "MESSAGE_HERE","status_code": "500"}),
	 * })
	 * @Parameters({
	 *      @Parameter("id", type="integer", required=true, description="Event Prescription ID")
	 * })
	 */
	public function update(Request $request, $id)
	{
		// $result = parent::update($request, $id);
		$result = $this->service->update($request->all(), $id);

		return $this->response->noContent();
	}

	/**
	 * UPDATE FLAG STATUS ENUM
	 * @uri : event-prescription/flag/
	 * @param prescription_id, flag
	 * @flag value ( "send-to-email", "deliver-to-lifeport" ) 
	 * EX.
	 * {
	 * 	"prescription_id" : 1,
	 *  "flag" : "send-to-email" 
	 * }
	 */
	public function flagUpdate(Request $request)
	{
		return $this->service->updatePrescriptionFlag($request->all());
	}

	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionRepository $repository,
		\Sunnydevbox\Recoveryhub\Transformers\EventPrescriptionTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\EventPrescriptionService $service
	) {
		$this->repository = $repository;
		$this->transformer = $transformer;
		$this->service = $service;
	}
}