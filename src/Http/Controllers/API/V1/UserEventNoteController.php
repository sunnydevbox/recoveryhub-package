<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Services\EventNoteService;

class UserEventNoteController extends APIBaseController
{
	public function store(Request $request)
	{
		try {
			$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
 
			$result = $this->service->store($request);

			// Using show method so that ?filter can be used
			// return $this->show($result->id);
 
			return $this->response->item($result, $this->transformer);
 
		 } catch (ValidatorException $e) {
 
			 return response()->json([
				 'status_code'   => 400,
				 'message' =>$e->getMessageBag()
			 ], 400);
		 }
	}

	public function index(Request $request)
	{
		$result = $this->service->index($request);
		
		return $this->response
			->paginator($result, $this->transformer)
			->withHeader('Content-Range', $result->total());
	}

	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Event\EventNoteRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\EventNoteValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\EventNoteTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\EventNoteService $service
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;
	}	
}