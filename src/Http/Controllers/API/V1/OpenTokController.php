<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Services\EventNoteService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Sunnydevbox\Recoveryhub\Events\OpentokSessionStatus;
use Sunnydevbox\Recoveryhub\Models\OpentokLog;

class OpenTokController extends APIBaseController
{

	public function store(Request $request)
	{
		$result = $this->service->store($request->all());
	}

	public function generateToken(Request $request)
	{
		$result = $this->service->regenerateToken($request->get('event_id'));
		return $this->response->item($result, $this->transformer);
	}


	public function generateSession(Request $request)
	{
		$result = $this->service->generateSession($request->get('event_id'));
		
		return $this->response->item($result, $this->transformer);
	}


	public function getSessionByEvent(Request $request)
	{
		try {
			$result = $this->service->getSessionByEvent($request->get('event_id'));
			return $this->response->item($result, $this->transformer);
		} catch(ModelNotFoundException $e) {

			$statusCode = 400;
			$response = [
				'status' => 400,
				'message' => 'Invalid ID',
			];
		} catch(\Exception $e) {
			$statusCode = 500;
			$response = [
				'status' => 500,
				'message' => $e->getMessage(),
			];
		} 

		return response()->json($response, $statusCode);		
	}

	public function setStatusByUserType(Request $request, $sessionId)
	{
		try {
			// echo $sessionId;
			$result = $this->service->setStatusByUserType($sessionId, $request->all());
			event(new OpentokSessionStatus($result));

		} catch(ModelNotFoundException $e) {

			$statusCode = 400;
			$response = [
				'status' => 400,
				'message' => 'Invalid ID',
			];
		} catch(\Exception $e) {
			$statusCode = 500;
			$response = [
				'status' => 500,
				'message' => $e->getMessage(),
			];
		} 
	}

	public function log(Request $request)
	{
		$this->service->setStatus($request->get('event_id'), $request->get('user_id'), $request->get('name'));
		$log = OpentokLog::create($request->all());
		return response()->json([]);
	}


	public function redis(Request $request, $sessionId)
	{
		$result = $this->service->setStatusByUserType($sessionId);
		event(new \Sunnydevbox\Recoveryhub\Events\OpentokSessionStatus($result));
	}
	
	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Tokbox\OpenTokRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\TokboxCallbackValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\TokboxCallbackTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\OpenTokService $service
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;
	}	
}