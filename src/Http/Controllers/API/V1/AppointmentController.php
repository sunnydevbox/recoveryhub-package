<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class AppointmentController extends APIBaseController
{
	public function my(Request $request)
	{
		$result = $this->service->myAppointments($request->all());
		$limit = is_numeric($request->get('limit')) ? $request->get('limit') : config('repository.pagination.limit', 15);

		if ($limit == 0) {
			return $this->response()
				->collection($result, $this->myAppointmentTransformer)
				->withHeader('Content-Range', $result->count());

		} else {
			return $this->response
					->paginator($result, $this->myAppointmentTransformer)
					->withHeader('Content-Range', $result->total());
		}
	}

	public function myMarkedDates(Request $request)
	{
		$result = $this->service->myMarkedDates($request->get('year'));
		
		return response()->json(['data' => $result]);
	}

    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository $repository, 
		\Sunnydevbox\Recoveryhub\Transformers\UserAppointmentTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\AppointmentService $service,
		\Sunnydevbox\Recoveryhub\Transformers\MyAppointmentTransformer $myAppointmentTransformer
	) {
		$this->repository = $repository;
		$this->transformer = $transformer;
		$this->service = $service;
		$this->myAppointmentTransformer = $myAppointmentTransformer;
	}
}