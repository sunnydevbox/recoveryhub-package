<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCart\Http\Controllers\API\V1\CouponController as CCouponController;
use Dingo\Api\Http\Request;

/**
 * User resource representation.
 *
 * @Resource("Coupon", uri="/shop/coupons")
 */
class CouponController extends CCouponController
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Coupon\CouponRepository $repository,
		\Sunnydevbox\Recoveryhub\Transformers\CouponTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\CouponService $service
    ) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->service = $service;
    }
}