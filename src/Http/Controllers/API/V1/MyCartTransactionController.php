<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class MyCartTransactionController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Cart\CartTransactionRepository $repository,
		\Sunnydevbox\Recoveryhub\Transformers\CartTransactionTransformer $transformer
	) {
        $this->repository = $repository;
        // $this->validator  = $validator;
        $this->transformer = $transformer;
	}
}