<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

// use Sunnydevbox\TWCart\Http\Controllers\CartController as CCartController;
use Dingo\Api\Http\Request;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;

/**
 * User resource representation.
 *
 * @Resource("Cart", uri="/cart")
 */
class CartOrderController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartOrderRepository $repository,
        \Sunnydevbox\Recoveryhub\Transformers\CartOrderTransformer $transformer
    ) {
        // $this->cartService = $cartService;
        $this->transformer = $transformer;
        $this->repository = $repository;
        // $this->cartTransactionService = $cartTransactionService;
    }
}