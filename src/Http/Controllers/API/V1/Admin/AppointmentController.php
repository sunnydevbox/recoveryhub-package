<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\Admin;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class AppointmentController extends APIBaseController
{
    public function index(Request $request)
    {
        // $this->repository->skipCriteria();
        // dd($this->repository->getCriteria());
        // dd($request->all());
        return parent::index($request);
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository $repository,
        \Sunnydevbox\Recoveryhub\Transformers\Admin\AppointmentTransformer $transformer
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;  
        $this->repository->skipCriteria();
    }
}