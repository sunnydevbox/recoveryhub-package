<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1\Admin;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class StaffController extends APIBaseController
{

    public function show($id)
    {
        return parent::show($id);
    }



    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $repository,
        \Sunnydevbox\Recoveryhub\Transformers\UserTransformer $transformer
    ) {
        $this->transformer = $transformer;
        $this->repository = $repository;  
        // $this->repository->skipCriteria();

        $this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\StaffCriteria::class);
    }
}