<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Illuminate\Http\Request ;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Services\EventNoteService;

class TokboxController extends APIBaseController
{

	public function store1(Request $request)
	{
		try {
			$sanitizedData = $this->service->sanitizeRequest($request);

			$this->validator->with($sanitizedData)->passesOrFail(ValidatorInterface::RULE_CREATE);
 
			$result = $this->service->store( $sanitizedData );

			return $this->response->item($result, $this->transformer);
 
		 } catch (ValidatorException $e) {
 
			 return response()->json([
				 'status_code'   => 400,
				 'message' =>$e->getMessageBag()
			 ], 400);
		 }
		
	}


	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Tokbox\TokboxCallbackRepository $repository, 
		\Sunnydevbox\Recoveryhub\Validators\TokboxCallbackValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\TokboxCallbackTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\TokboxService $service
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;
	}	
}