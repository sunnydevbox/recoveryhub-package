<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

class MentalHealthDataController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\MentalHealthData\MentalHealthDataRepository $repository,
        \Sunnydevbox\Recoveryhub\Validators\MentalHealthDataValidator $validator,
        \Sunnydevbox\Recoveryhub\Transformers\MentalHealthDataTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\MentalHealthDataService $service
    ) {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->service = $service;
    }

    public function show($id)
    {
        $result = $this->service->show($id);
        if (!$result && isset($this->return_messages)) {
            return $this->response->error('object_not_found', 404);
        }
        return $this->response->item($result, $this->transformer);
    }

    public function store(Request $request)
    {
        $result = $this->service->store($request->all());
        return $this->response->item($result, $this->transformer);
    }

    public function update(Request $request, $id)
    {
        $result = $this->service->update($request->all(), $id);
        return $this->response->item($result, $this->transformer);
    }

    public function getByUserId($userId)
    {
        $result = $this->service->getByUserId($userId);

        if (!$result) { //} && isset($this->return_messages)) {
            return $this->response->error('object_not_found', 404);
        }

        return $this->response->item($result, $this->transformer);
    }

    public function updateByUserId(Request $request, $id)
    {
        $result = $this->service->updateByUserId($request->all(), $id);
        return $this->response->item($result, $this->transformer);
    }
}