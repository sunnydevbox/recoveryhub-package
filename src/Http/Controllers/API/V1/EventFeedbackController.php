<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

use Sunnydevbox\Recoveryhub\Events\EventFeedbackEmailInfoEvent;

class EventFeedbackController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Feedback\EventFeedbackRepository $repository, 
        \Sunnydevbox\Recoveryhub\Transformers\EventFeedbackTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\EventPrescriptionService $prescriptionService
	) {
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->prescriptionService = $prescriptionService;
    }

    public function getByEventRole($event_id, $role)
    {
        $result = $this->repository->getByEventRole($event_id, $role);
        return $result;
        return $this->response->item($result, $this->transformer);

    }

    public function getByRole($role)
    {
        $result = $this->repository->getByRole($role);
        return $this->response->item($result, $this->transformer);
        
    }

    public function sendMedication(Request $request)
    {
        $result = $this->prescriptionService->sendMedication($request->all());
        return $result;
    }

    public function getByIdRole($id, $role)
    {
        $result = $this->repository->getByIdRole($id, $role);
        return $this->response->item($result, $this->transformer);
    }

    public function store(Request $request)
	{
		 try {
			
			if (isset($this->validator) && $this->validator) {
				$this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			}
			
            $result = $this->repository->create( $request->all() );
            // echo 'this';
            // dd($result);

            // CALL EVENT TO EMAIL INFO
            event(new EventFeedbackEmailInfoEvent($result));

            return $this->response->item($result, $this->transformer);

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}
    
}