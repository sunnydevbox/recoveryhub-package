<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class MedicineInventoryController extends APIBaseController
{
    public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Medicine\MedicineInventoryRepository $repository, 
		\Sunnydevbox\Recoveryhub\Transformers\MedicineInventoryTransformer $transformer
	) {
		$this->repository = $repository;
		$this->transformer = $transformer;
	}
}