<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;
use Sunnydevbox\TWUser\Http\Controllers\API\V1\UserController as ExtendUserController;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Sunnydevbox\Recoveryhub\Events\UserRegisteredEvent;
use Prettus\Validator\Contracts\ValidatorInterface;
use JWTAuth;
use Sunnydevbox\Recoveryhub\Events\UserVerifiedEmailEvent;

class UserController extends ExtendUserController
{
	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\User\UserRepository $repository, 
		\Sunnydevbox\TWUser\Validators\UserValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\UserTransformer $transformer
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
	}

	public function uploadPicture(Request $request, $id)
	{
		$picture = $this->repository->uploadPicture($id, $request->file('picture'));		
		return $this->response()->created(null, ['picture' => $picture]);
	}

	public function uploadSignature(Request $request, $id)
	{
		$signature = $this->repository->uploadSignature($id, $request->file('signature'));
		return $this->response()->created(null, ['signature' => $signature]);
	}

	public function assignStatus(Request $request, $id)
	{
		$user = $this->repository->updateStatus($request, $id);
		return $this->response()->created(null, ['user' => $user]);
	}

	public function register(Request $request)
    {  
        try {
			
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
			$result = $this->repository->register($request);
			
			// NOTIFY USER
            event(new UserRegisteredEvent($result));
           
            return $this->response->item($result, $this->transformer);
        } catch (ValidatorException $e) {
            
            return response()->json([
                'status_code'   => 400,
                'message' => $e->getMessageBag()
            ], 400);
        }
	}

	public function verifyAccount($email, $token)
	{
		$user = $this->repository->verifyAccount($email, $token);

		if (!$user) {
			abort(400, 'invalid_token');
		}

		event(new UserVerifiedEmailEvent($user));

		return $this->response->noContent();
	}

	public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {

				return response()->json(['error' => 'invalid_credentials'], 401);
			}
			
			$user = JWTAuth::authenticate($token);

			if ($user->status == "inactive") {
				return $this->response->errorUnauthorized('account_not_active');
			} else {
				if ( ($user->status != "active") ) {
					// throw error default suspended, banned, close
					return $this->response->errorUnauthorized('account_status_invalid');
				}
			}

			if ($user->is_verified) {
				$this->transformer->setMode('complete');
				$user->token = JWTAuth::getToken()->get();
				return $this->response->item($user, $this->transformer);
			} else {
				return $this->response->errorUnauthorized('not_verified');
			}

			

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }
}