<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;
use Auth;

class PaymentController extends APIBaseController
{
    public function __construct(
        \Sunnydevbox\TWCart\Services\CartService $cartService
	) {
        $this->cartService = $cartService;
	}

	public function checkout(Request $request)
    {
        $result = $this->cartService->setGateway($request->get('gateway', 'altpay'));
        if (empty($cart)) $cart = Auth::user()->cart;
        $data = [
                "amount" => $cart->total,
                "currency" => "PHP",
                "paymentType" => "DB"
            ];
        $checkoutId = $result->gateway->getCheckoutId($data);

        return $this->response->array([
            'checkoutId' => $checkoutId,
        ]);
    }
}