<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Dingo\Api\Http\Request;

/**
 * Event Prescription Item
 *
 * @Resource("Event Prescription Item", uri="/event-prescription-items")
 */
class EventPrescriptionItemController extends APIBaseController
{
	public function checkAvailableQuantity(Request $request, $id)
	{
		$this->service->checkAvailableQuantity($id);
	}

	public function addToCart(Request $request)
	{
		try { 
			$result = $this->service->addToCart($request->get('id'), $request->get('quantity'));
			
			return response()->json($result);
		} catch (\Exception $e) {
			return response()->json([
                'message' => $e->getMessage(),
                'code'  => $e->getCode(),
            ], $e->getCode());
		}
	}



	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Event\EventPrescriptionItemRepository $repository,
		\Sunnydevbox\Recoveryhub\Transformers\EventPrescriptionItemTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\EventPrescriptionItemService $service
	) {
		$this->transformer = $transformer;
		$this->repository = $repository;
		$this->service = $service;
	}	
}