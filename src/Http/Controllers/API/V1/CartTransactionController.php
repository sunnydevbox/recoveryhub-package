<?php
namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Sunnydevbox\TWCore\Http\Controllers\Controller;
use Sunnydevbox\TWCore\Http\Controllers\APIBaseController;
use Sunnydevbox\TWCart\Http\Controllers\API\V1\CartTransactionController as CCartTransactionController;
use Dingo\Api\Http\Request;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request as IlluminateRequest;

// use Sunnydevbox\Recoveryhub\Events\TransactionStatusChangeEvent;
/**
 * User's cart transactions.
 *
 * @Resource("Cart Transactions", uri="/cart/transaction")
 */
class CartTransactionController extends CCartTransactionController
{
    private $cartService;

    public function index(Request $request)
    { 

        // dd($request);
        return parent::index($request);
    }

    public function show($id)
    {
        return parent::show($id);
    }

    public function update(Request $request, $id)
	{
        
        // event( new TransactionStatusChangeEvent($result, $id) );

        // FETCH TRANSACTION AND POSSIBLE DOCTOR AND PATIENT
        $res = $this->cartService->statusEvent($request, $id);

		return parent::update($request, $id);
    }

    public function callback(Request $request)
    {
        $statusCode = 400;
        $messageCode = '';
        $message = '';
        $data = [];
        
        try {
            $cartTransactionService = app(\Sunnydevbox\Recoveryhub\Services\CartTransactionService::class);
            
            $transaction = $cartTransactionService->repository->with(['order'])->getByTxnid($request->get('txnid'));
            if (!$transaction) {
                throw new Exception('invalid_transaction_id', 400);
            }

            $statusCode = 200;
            $messageCode = 200;
            $message = 'ok';
            $data = $transaction->order;

        } catch(Exception $e) {
            if (!$e->getCode()) {
                $statusCode = 500;
                $message = $e->getMessage();
                $messageCode = 'System error';
            } else {
                $statusCode = $e->getCode();
                $message = $e->getMessage();
                $messageCode = $e->getMessage();
            }
        }

        return response()->json([
            'message'       => $message,
            'status'        => $statusCode,
            'code'          => $messageCode,
            'data'          => $data,
        ], $statusCode);
    }

    public function postback(IlluminateRequest $request, $gateway)
    {      
        $gateway = strtolower($gateway);
        $statusCode = 400;
        $messageCode = '';
        $message = '';
        $data = [];
        
        \Log::info(request()->all());
        if ($gateway == 'dragonpay') {
            
            try {
                $gw = $this->cartService->setGateway($gateway)->gateway;
                $result = $gw->onCallbackSuccess('', $request->all());

                return response('result=ok');

            } catch(Exception $e) {
                if (!$e->getCode()) {
                    $statusCode = 500;
                    $message = $e->getMessage();
                    $messageCode = 'System error';
                    // $data = $e->getTrace();
                } else {
                    $statusCode = $e->getCode();
                    $message = $e->getMessage();
                    $messageCode = $e->getMessage();
                    // $data = $e->getTrace();
                }

                return response()->json([
                    'message'       => $message,
                    'status'        => $statusCode,
                    'code'          => $messageCode,
                    'data'          => $data,
                ], $statusCode);
            }
        }
    }


    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Cart\CartTransactionRepository $repository,
        \Sunnydevbox\TWCart\Transformers\CartTransactionTransformer $transformer,
        \Sunnydevbox\Recoveryhub\Services\CartService $cartService,
        \Sunnydevbox\Recoveryhub\Services\CartTransactionService $cartTransactionService
    ) {
       
        $this->repository = $repository;
        $this->transformer = $transformer;
        $this->cartService = $cartService;
        $this->cartTransactionService = $cartTransactionService;
    }
}