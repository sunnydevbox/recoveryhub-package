<?php

namespace Sunnydevbox\Recoveryhub\Http\Controllers\API\V1;

use Dingo\Api\Http\Request;

use Sunnydevbox\TWEvents\Http\Controllers\API\V1\EventController as ExtendEventController;
use Sunnydevbox\Recoveryhub\Repositories\Appointment\AppointmentRepository;
use Sunnydevbox\Recoveryhub\Transformers\UserEventAppointmentTransformer;
use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\Recoveryhub\Events\EventCreatedEvent;
use Sunnydevbox\Recoveryhub\Criteria\AsDoctor;
use Sunnydevbox\Recoveryhub\Traits\CartEventControllerTrait;

class UserEventController extends ExtendEventController
{
	use CartEventControllerTrait;

	public function __construct(
		\Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $repository, 
		\Sunnydevbox\TWEvents\Validators\EventValidator $validator,
		\Sunnydevbox\Recoveryhub\Transformers\UserEventTransformer $transformer,
		\Sunnydevbox\Recoveryhub\Services\UserEventService $service
	) {
        $this->repository = $repository;
        $this->validator  = $validator;
		$this->transformer = $transformer;
		$this->service = $service;

		$this->repository->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\IsMyAppointmentCriteria::class);
	}

	public function getAppointments(Request $request, AppointmentRepository $rpoAppointment)
	{
		$result = $this->repository->getAppointments();

		return $this->response
			->paginator($result, new UserEventAppointmentTransformer())
			->withHeader('Content-Range', $result->total());
	}

	public function store(Request $request)
	{
		 try {
           $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

			   $result = $this->repository->create( $request->all() );
			   			
			// NOTIFY Doctor
			foreach($result as $r) {
				event(new EventCreatedEvent($r));
			}
			
			if ($result->count() == 1) {
				return $this->response->item($result[0], $this->transformer);
			} else if ($result->count() > 1) {
				return $this->response->collection($result, $this->transformer);
			} else {
				abort(400, 'No event(s) created');
			}

        } catch (ValidatorException $e) {

            return response()->json([
                'status_code'   => 400,
                'message' =>$e->getMessageBag()
            ], 400);
        }
	}

	public function setSession($id, Request $request)
	{
		if ($request->get('session_id')) {
			$this->repository->update(['session_id' => $request->get('session_id')] , $id);

			return $this->response->noContent();
		}
		
		abort(400, 'inalid_fields');
	}

	public function getSession($id)
	{
		$event = $this->repository->find($id);

		return response()->json(['session_id' => $event->getMeta('session_id', null)]);
	}

	public function index(Request $request)
	{
		if ($request->get('criteria')) {
			// dd($this->repository->getCriteria());
		} else {
			$this->repository->popCriteria($this->repository->getCriteria()->first(function($value, $key){
				return (strpos(get_class($value), '\UserEventCriteria'));
			}));
		}
		// dd($request->all());
		//if (gettype(strpos($request->get('search'), 'assignable_id')) == 'integer') {
			// REMOVE UserEventCriteria
			
		//}

		return parent::index($request);
	}

	public function show($id) 
	{
		// REMOVE UserEventCriteria
		$this->repository->popCriteria($this->repository->getCriteria()->first(function($value, $key){
			return (strpos(get_class($value), '\UserEventCriteria'));
		}));

		return parent::show($id);

	}

	public function destroy($id)
	{
		try {
			$obj = $this->service->destroy($id);
			
			return $this->response->noContent();

		} catch (\Exception $e) {

			return response()->json([
                'status_code'   => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
		}
	}
}