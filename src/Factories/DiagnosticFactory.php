<?php
namespace Sunnydevbox\Recoveryhub\Factories;

class DiagnosticFactory
{
    public function build($type)
    {
        $type = ucwords($type) . 'DiagnosticRepository';

        $object = $this->{$type}->getList();

        return $object;
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Diagnostic\RequestDiagnosticRepository $RequestDiagnosticRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Diagnostic\TestDiagnosticRepository $TestDiagnosticRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Diagnostic\TypeDiagnosticRepository $TypeDiagnosticRepository
    ) {
        $this->RequestDiagnosticRepository = $RequestDiagnosticRepository;
        $this->TestDiagnosticRepository = $TestDiagnosticRepository;
        $this->TypeDiagnosticRepository = $TypeDiagnosticRepository;
    }
}