<?php
namespace Sunnydevbox\Recoveryhub\Factories;

use Sunnydevbox\Recoveryhub\Repositories\Event\Reports\AdmittingReportRepository;

class ReportFactory
{
    public function build($type, $params = [])
    {
        $event = $this->Event->with(['bookings.patient', 'doctor', 'notes', 'prescriptions.items.medicine.generic'])->find($params['event_id']);
        switch($type) {
            case 'feedback':  
                $template = 'abstract-reports';
                $type = 'EventFeedbackRepository';
                $params['role'] = 'patient';
                $object = $this->AdmittingReportRepository->reports($params);
                $object['feedback'] = $this->{$type}->getByEventRole($params['event_id'], 'medical-practitioner');
                $object['event'] = $event;
            break;

            default:
                $template = $type . '-reports';
                $type = ucwords($type) . 'ReportRepository';
                $object = $this->{$type}->reports($params);
                $object['event'] = $event;
            break;
        }

        return [
            'data' => $object,
            'template' => $template,
        ];
    }

    public function __construct(
        \Sunnydevbox\Recoveryhub\Repositories\Event\Reports\AdmittingReportRepository $AdmittingReportRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Event\Reports\CertificateReportRepository $CertificateReportRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Event\Reports\LaboratoryReportRepository $LaboratoryReportRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Feedback\EventFeedbackRepository $EventFeedbackRepository,
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $rpoEvent
    ) {
        $this->AdmittingReportRepository = $AdmittingReportRepository;
        $this->LaboratoryReportRepository = $LaboratoryReportRepository;
        $this->CertificateReportRepository = $CertificateReportRepository;
        $this->EventFeedbackRepository = $EventFeedbackRepository;
        $this->Event = $rpoEvent;
    }
}