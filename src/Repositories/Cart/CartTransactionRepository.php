<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Cart;

use Sunnydevbox\TWCart\Repositories\Cart\CartTransactionRepository as CCartTransactionRepository;

class CartTransactionRepository extends CCartTransactionRepository
{
    protected $fieldSearchable = [
        'order_id',
        'gateway',
        'transaction_id',
        'token',
        'detail' => 'like',
        'notes' => 'like',
        'status',
    ];

    public function getByTxnid($txnid)
    {   
        return $this->skipCriteria()->findWhere(['transaction_id' => $txnid])->first();
    }

    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\Transaction';
    }

    public function boot()
    {
        // parent::boot();

        $this->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\CartTransactionCriteria::class);
    }
}