<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Cart;

use Sunnydevbox\TWCart\Repositories\Cart\CartRepository as CCartRepository;

class CartRepository extends CCartRepository
{
    public function model()
    {
        // return config('tw-cart.models.cart');
        return 'Sunnydevbox\Recoveryhub\Models\Cart';
    }
}