<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Cart;

use Sunnydevbox\TWCart\Repositories\Order\OrderRepository as COrderRepository;

class CartOrderRepository extends COrderRepository
{
    protected $fieldSearchable = [
        'user_id',
        'statusCode',
    ];
    
    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\Order::class;
    }

}