<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Cart;

use Sunnydevbox\TWCart\Repositories\Order\OrderRepository as COrderRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;

class CartItemRepository extends COrderRepository
{
    protected $fieldSearchable = [
        'user_id',
        'cart_id',
        'order_id',
        'sku',
        'shipping',
        'currency',
        'quantity',
        'class',
        'reference_id',
        'created_at',
        'updated_at'
    ];
    
    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\Item::class;
    }


}