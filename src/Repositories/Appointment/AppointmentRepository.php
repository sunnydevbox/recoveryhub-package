<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Appointment;

use Sunnydevbox\TWBookings\Repositories\Booking\BookingRepository;
use JWTAuth;
use Sunnydevbox\Recoveryhub\Criteria\UserAppointmentCriteria;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;

class AppointmentRepository extends BookingRepository 
{
    protected $allowMultipleBookings = false;
    
    protected $fieldSearchable = [
        'label',
        'start_at',
    ];

    public function model()
    {
        return config('recoveryhub.models.appointment');
    }

    public function create(array $attributes)
    {
        $this->checkIfBooked($attributes['event_id']);

        // SET CURRENT USER
        $user = JWTAuth::parseToken()->authenticate();
        $attributes['assignable_type'] = config('recoveryhub.models.user');
        $attributes['assignable_id'] = $user->id;
        $attributes['bookable_type'] = config('recoveryhub.models.event');
        $attributes['bookable_id'] = $attributes['event_id'];

        unset($attributes['event_id']);

        return parent::create($attributes);
    }


    public function update(array $attributes, $id)
    {
        $this->checkIfBooked($attributes['event_id']);

        $attributes['bookable_id'] = $attributes['event_id'];
        unset($attributes['event_id']);

        return parent::update($attributes, $id);
    }

    private function checkIfBooked($event_id)
    {
        $count = $this->model->where('bookable_id', $event_id)
                    ->count();

        if ($count && !$this->allowMultipleBookings) {
            abort(400, 'already_booked');
        }
    }

    public function allowMultipleBookings($allow = true)
    {
        $this->allowMultipleBookings = $allow;
    }

    public function boot()
    {
        parent::boot();
        
        $this->model->observe(new \Sunnydevbox\Recoveryhub\Observers\AppointmentObserver);
        $this->allowMultipleBookings(config('recoveryhub.allowMultipleBookings'));
        $this->pushCriteria(DateRangeCriteria::class);


        // !!!
        $this->pushCriteria(UserAppointmentCriteria::class);
    }
}