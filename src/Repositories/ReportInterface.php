<?php
namespace Sunnydevbox\Recoveryhub\Repositories;

interface ReportInterface
{
    public function reports($data = []);
}