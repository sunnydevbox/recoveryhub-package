<?php
namespace Sunnydevbox\Recoveryhub\Repositories\User;

use Sunnydevbox\TWUser\Repositories\User\UserRepository as ExtendUserRepository;
use Validator;
use Illuminate\Support\Facades\Storage;
use Image;

class UserRepository extends ExtendUserRepository
{
    protected $profilePictureKey = 'picture';

    protected $fieldSearchable = [
        'email',
        // 'status',
    ];

    public function updateStatus($request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);
        
        if ($validator->fails()) {
            throw new \Dingo\Api\Exception\UpdateResourceFailedException('Failed to update status', $validator->errors());

        } else {
            // UPDATE STATUS
            $result = $this->update(['status' => $request->get('status')] ,$id); 
            return $result;

        }
    }

    public function uploadPicture($id, $file)
    {
        $user = $this->find($id);

        if (!$user) {
            abort(400, 'User not found');
        }

        $attachment = $user->attach($file, [
            'key' => $this->profilePictureKey,
            'group' => 'user_avatar',
        ]);
        $attachment = $user->attach($file, [
            'key' => 'orig',
            'group' => 'user_avatar',
        ]);

        $orig = storage_path('app/'.$attachment->filepath);
            
        // dd(path($attachment->filepath));

        // GENERATE Smaller size
        // echo $orig;
        $mobileImage = Image::make($orig);
        $mobileImage->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $mobileFile = "{$mobileImage->dirname}/{$mobileImage->basename}";
        $user->attach($mobileFile, [
            'group' => 'user_avatar',
            'key' => '300',
        ]);

        $mobileImage = Image::make($orig);
        $mobileImage->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $mobileFile = "{$mobileImage->dirname}/{$mobileImage->basename}";
        $user->attach($mobileFile, [
            'group' => 'user_avatar',
            'key' => '600',
        ]);

        $mobileImage = Image::make($orig);
        $mobileImage->resize(1900, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $mobileFile = "{$mobileImage->dirname}/{$mobileImage->basename}";
        $user->attach($mobileFile, [
            'group' => 'user_avatar',
            'key' => '1900',
        ]);

        return $user->attachment($this->profilePictureKey)->url;
        // return $user->avatar;
    }

    public function uploadSignature($id, $file)
    {
        $this->profilePictureKey = 'signature';
        return $this->uploadPicture($id, $file);
    }

    public function register($request)
    {
        $user = parent::register($request);

        // LATER on assignRole might need to distinguish between patient and medical-practitioner
        $user = $user->assignRole('patient');

        // Call user with user->id to get or sync hasRole
        $user = $this->find($user->id);

        event(new \Sunnydevbox\Recoveryhub\Events\PatientAccountCreatedEvent($user));

        return $user;
    }

    public function model()
    {
        return config('recoveryhub.models.user');
    }

    public function formatGeneralInformation(array $attributes, $reverse = false)
    {
        $columns = [
            "firstName" => 'first_name',
            "middleName" => 'middle_name',
            "lastName" => 'last_name',
            "tempNoStreet" => 'address_1',
            "tempBarangay" => 'barangay',
            "tempCityMunicipality" => 'city',
            "tempProvince" => 'state',
            "tempZipCode" => 'zipcode',
            "sex" => 'sex',
            "civilStatus" => 'civil_status',
            "age" => 'age',
            "dateOfBirth" => 'date_of_birth',
            "placeOfBirth" => 'place_of_birth',
            "religion" => 'religion',
            "nationality" => 'nationality',
            "race" => 'race',
            "ethnicity" => 'ethnicity',
            "permNoStreet" => 'perm_no_street',
            "permBarangay" => 'perm_barangay',
            "permCityMunicipality" => 'perm_city_municipality',
            "permProvince" => 'perm_province',
            "permZipCode" => 'perm_zip_code',
            "landlineNumber" => 'landline_number',
            "mobileNumber" => 'mobile_number',
            "emailAddress" => 'email_address',
            "highestEducationalAttainment" => 'highest_educational_attainment',
            "occupation" => 'occupation',
            "company" => 'company',
            "companyAddressAndContactDetails" => 'company_address_and_contact_details',
            "height" => 'height',
            "weight" => 'weight',
            "voluntarilyAccept" => 'voluntarily_accept',
 
            "contactFirstName" => "contact_first_name",
            "contactLastName" => "contact_last_lame",
            "contactMiddleName" => "contact_middle_name",
            "contactMobileNo" => "contact_mobile_no",
            "contactAddress" => "contact_address",
            "contactLandLine" => "contact_land_line"
        ];

        $newAttribute = [];

        if ($reverse) {
            foreach ($columns as $key => $value) {
                $newAttribute[$key] = null;
                if (isset($attributes[$value])) {
                    $newAttribute[$key] = $attributes[$value];
                }
            }
        } else {
            foreach ($attributes as $key => $value) {

                if (isset($columns[$key])) {
                    $newAttribute[$columns[$key]] = $value;
                }

            }
        }

        return $newAttribute;
    }
}