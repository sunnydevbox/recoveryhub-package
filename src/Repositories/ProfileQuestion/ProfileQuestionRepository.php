<?php
namespace Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class ProfileQuestionRepository extends TWBaseRepository
{
    public function model()
    {
        return config('recoveryhub.models.profile-question');
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(app('\Sunnydevbox\Recoveryhub\Criteria\ProfileQuestionsCriteria'));
        

    }
}