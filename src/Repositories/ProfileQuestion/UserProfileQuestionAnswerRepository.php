<?php
namespace Sunnydevbox\Recoveryhub\Repositories\ProfileQuestion;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class UserProfileQuestionAnswerRepository extends TWBaseRepository
{
    public function model()
    {
        return config('recoveryhub.models.profile-question-answer');
    }
}