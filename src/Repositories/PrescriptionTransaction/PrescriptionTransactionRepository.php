<?php
namespace Sunnydevbox\Recoveryhub\Repositories\PrescriptionTransaction;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class PrescriptionTransactionRepository extends TWBaseRepository
{

    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\PrescriptionTransaction';
    }

    // public function model()
    // {
    //     return config('recoveryhub.models.patient');
    // }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(\Sunnydevbox\Recoveryhub\Criteria\PrescriptionTransactionCriteria::class);
    }

}