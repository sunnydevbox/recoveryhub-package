<?php
namespace Sunnydevbox\Recoveryhub\Repositories\ContactFormSubmission;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class ContactFormSubmissionRepository extends TWBaseRepository
{
    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\ContactFormSubmissionValidator::class;
    }

    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\ContactFormSubmission::class;
    }
}