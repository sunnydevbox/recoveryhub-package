<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Diagnostic;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class TestDiagnosticRepository extends TWBaseRepository
{
    public function getList()
    {
        return $this->model->all();
    }

    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\DiagnosticTest';
    }
}