<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Diagnostic;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class TypeDiagnosticRepository extends TWBaseRepository
{
    public function getList()
    {
        return $this->model->with('test', 'request')->get();
    }

    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\DiagnosticType';
    }
}