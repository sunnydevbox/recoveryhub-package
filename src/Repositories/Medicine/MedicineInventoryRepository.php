<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Medicine;

use Sunnydevbox\TWCore\Criteria\RequestCriteria;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class MedicineInventoryRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'name',
        'unit'
    ];

    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\MedicineInventoryValidator::class;
    }

    public function model()
    {
        return config('recoveryhub.models.medicine-inventory');
    }

    public function boot()
    {
        $this->pushCriteria(app('\Sunnydevbox\TWCore\Criteria\RequestCriteria'));
    }
}
