<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Medicine;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class MedicineGenericNameRepository extends TWBaseRepository
{
    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\MedicineGenericNameValidator::class;
    }

    public function model()
    {
        return config('recoveryhub.models.medicine-generic-name');
    }
}