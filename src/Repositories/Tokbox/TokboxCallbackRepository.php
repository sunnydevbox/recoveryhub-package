<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Tokbox;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class TokboxCallbackRepository extends TWBaseRepository 
{
    protected $fieldSearchable = [
        'session_id',
        'project_id',
        'event',
        'reason',
        'timestamp',
        'connection',
        'stream',
    ];

    public function model()
    {
        return config('recoveryhub.models.tokbox-callback');
    }
}