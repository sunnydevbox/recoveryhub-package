<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Tokbox;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class OpenTokRepository extends TWBaseRepository 
{


    public function _fetchObject($id)
    {
        if (!is_object($id)) {
            return $this->makeModel()->find($id);
        }

        return $id;
    }



    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\OpenTok::class;
    }
}
