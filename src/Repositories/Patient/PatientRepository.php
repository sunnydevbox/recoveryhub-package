<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Patient;

use Sunnydevbox\Recoveryhub\Repositories\User\UserRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\PatientCriteria;

class PatientRepository extends UserRepository
{
    // public function model()
    // {
    //     return config('recoveryhub.models.patient');
    // }

    public function boot()
    {
        
        $this->pushCriteria(DateRangeCriteria::class);
        $this->pushCriteria(PatientCriteria::class);

    }

}