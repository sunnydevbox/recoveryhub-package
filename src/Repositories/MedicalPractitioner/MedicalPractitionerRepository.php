<?php
namespace Sunnydevbox\Recoveryhub\Repositories\MedicalPractitioner;

use Sunnydevbox\Recoveryhub\Repositories\User\UserRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\MedicalPractitionerCriteria;

class MedicalPractitionerRepository extends UserRepository
{
    private $skipMedicalPractitionerCriteria = false;

    // public function model()
    // {
    //     return config('recoveryhub.models.medical-practitioner');
    // }
    
    public function skipMedicalPractitionerCriteria($skip = true)
    {
        $this->skipMedicalPractitionerCriteria = $skip;
    }

    public function boot()
    {
        parent::boot();

        $this->pushCriteria(DateRangeCriteria::class);
        $this->pushCriteria(MedicalPractitionerCriteria::class);
    }
}