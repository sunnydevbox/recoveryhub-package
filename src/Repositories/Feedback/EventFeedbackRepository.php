<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Feedback;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Auth;

class EventFeedbackRepository extends TWBaseRepository
{
    protected $fieldSearchable = [
        'event_id',
    ];

    public function validator()
    {
        return 'Sunnydevbox\Recoveryhub\Validators\EventFeedbackValidator';
    }

    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\EventFeedback';
    }

    public function create(array $attributes)
    {
        $attributes = $this->formatAttributes($attributes);
        return parent::create($attributes);
    }

    public function update(array $attributes, $id)
    {
        $newAttribute = $this->formatAttributes($attributes);
        return parent::update($newAttribute, $id);
    }

    private function formatAttributes( array $attributes )
    {
        $data = [
            'eventId' => 'event_id',
            'comment' => 'comment',
            'historyOfPresentIllness' => 'history_of_present_illness',
            'briefSummaryOfPositiveAspects' => 'brief_summary_of_positive_aspects',
            'symptomInformation' => 'symptom_information',
            'developmentalHistory' => 'developmental_history',
            'pastMedicalPsychiatricHistory' => 'past_medical_psychiatric_history',
            'familyHistory' => 'family_history',
            'medicationHistoryCurrentMedications' => 'medication_history_current_medications',
            'abusesTrauma' => 'abuses_trauma',
            'mentalStatusExamination' => 'mental_status_examination',
            'physicalAppearance' => 'physical_appearance',
            'behavior' => 'behavior',
            'speech' => 'speech',
            'mood' => 'mood',
            'affect' => 'affect',
            'thoughts' => 'thoughts',
            'suicidalOrHomicidalIntentPresent' => 'suicidal_or_homicidal_intent_present',
            'psychosis' => 'psychosis',
            'cognition' => 'cognition',
            'insight' => 'insight',
            'impressionAndCodes' => 'impression_and_codes',
            '1PsychiatricDiagnosis' => 'psychiatric_diagnosis',
            '2MedicalDiagnosis' => 'pedical_diagnosis',
            'selfHarmViolencePotential' => 'self_harmViolence_potential',
            'treatmentRecommendations' => 'treatment_recommendations',
            'lifestyle' => 'lifestyle',
            'recommendedMedicationChanges' => 'recommended_medication_changes',
            'discussCautionsSideEffectsForUseOfMedication' => 'discuss_cautionsSide_effects_for_use_of_medication',
            'referralsRequired' => 'referrals_required',
            'psychotherapy' => 'psychotherapy',
            'psychoeducation' => 'psycho_education',
            'chiefComplaint' =>  'chief_complaint',
            'generalData' =>  'general_data',
            'courseInTheWardOutPatient' =>  'course_in_the_ward_out_patient',
            'laboratoryExaminationResult' =>  'laboratory_examination_result',
            'psychologicalExaminationResult' =>  'psychological_examination_result',
            'diagnosis' =>  'diagnosis',
            'medication' =>  'medication',
            'recommendation' =>  'recommendation',
        ];

        $newAttribute = [];

        foreach($attributes as $key => $value) {

            if (isset($data[$key])) {
                $newAttribute[$data[$key]] = $value;
            }

        }

        $newAttribute['user_id'] = Auth::user()->id;
        
        return $newAttribute;
    }

    public function getByEventRole($event_id, string $role)
    {
        $result =  $this->model
            ->where('event_id', (int) $event_id)
            ->whereHas('user', function($query) use ($role) {
                // $query->role($role);
            })
            ->orderBy('updated_at', 'desc')
            ->first();
        return $result;
    }

    public function getByRole(string $role)
    {
        $result = $this->model
            ->with('user')
            ->where('comment','!=', null)
            ->whereHas('user', function($query) use ($role) {
                $query->role($role);
            })
            ->orderBy('updated_at', 'desc')
            ->paginate(15);
        return $result;
    }

    public function getByIdRole($id, string $role)
    {
        $result =  $this->model
        ->with('user')
        ->where('id', (int) $id)
        ->whereHas('user', function($query) use ($role) {
            $query->role($role);
        })
        ->first();
    return $result;
    }

    public function getByEventId($eventId)
    {
        $result =  $this->model
        ->with('user')
        ->where('event_id', (int) $eventId)
        ->first();
    return $result;
    }
    
}