<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Coupon;

use Sunnydevbox\TWCart\Repositories\Coupon\CouponRepository as CCouponRepository;

class CouponRepository extends CCouponRepository
{
    // public function validator()
    // {
    //     return \Sunnydevbox\TWCart\Validators\CouponValidator::class;
    // }


    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\Coupon::class;
    }
}