<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Coupon;

// use Sunnydevbox\TWCart\Repositories\Coupon\CouponRepository as CCouponRepository;

class AppliedCouponRepository extends \Sunnydevbox\TWCart\Repositories\Coupon\AppliedCouponRepository
{

    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\AppliedCouponValidator::class;
    }
    
    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\AppliedCoupon::class;
    }
}