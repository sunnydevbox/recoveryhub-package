<?php
namespace Sunnydevbox\Recoveryhub\Repositories\RateCharge;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class RateChargeRepository extends TWBaseRepository
{
    // protected $fieldSearchable = [
    //     'session_id',
    //     'project_id',
    //     'event',
    //     'reason',
    //     'timestamp',
    //     'connection',
    //     'stream',
    // ];


    public function getByDoctor($id = null, $dayOfWeek = null)
    {
        if (!$id) {
            throw new \Exception('invalid_id');
        }

        $model = $this->makeModel();
        $result = $model
                    ->current($dayOfWeek)
                    ->where('doctor_id', $id)
                    ->first()
                    ;
        return $result;
    }

    public function getCurrent($doctorId, $dayOfWeek)
    {
        $result = $this->model
            ->setDoctor($doctorId)
            ->current($dayOfWeek)
            ->first();

        if ($result) {
            if ($result['use_default'] == 1) {
                return $result['default'];
            }
            return $result['rate'];
        }
        return;
    }

    public function updateByDoctor($id, $data)
    {
        if (!$id) {
            throw new \Exception('invalid_id');
        }
        
        $data['doctor_id'] = $id;
        
        $result = $this->updateOrCreate(['doctor_id' => $id], $data);
        
        return $result;
    }

    public function setStartingRate($doctor_id)
    {
        $record = $this->findByField('doctor_id', $doctor_id)->first();
        if ($record) {
            return $record;
        }

        return $this->create([
            'doctor_id'     => $doctor_id,
            'default'       => 500,
            'use_default'   => true,
            'monday'        => 500,
            'tuesday'       => 500,
            'wednesday'     => 500,
            'thursday'      => 500,
            'friday'        => 500,
            'saturday'      => 500,
            'sunday'        => 500,
        ]);
    }



    public function model()
    {
        return \Sunnydevbox\Recoveryhub\Models\RateCharge::class;
    }
}
