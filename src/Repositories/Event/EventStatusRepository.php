<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Sunnydevbox\TWEvents\Repositories\Event\EventRepository as ExtendEventRepository;
use JWTAuth;
use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;

class EventStatusRepository
{
    public $event;
    
    public function setEvent($event)
    {
        $this->event = $event;
        return $this;
    }

    public function open()
    {
        $this->setStatus('open');
    }

    public function completed()
    {
        $this->setStatus('completed');
    }

    public function booked()
    {
        $this->setStatus('booked');
    }

    public function cancelled()
    {
        $this->setStatus('cancelled');
    }

    public function started()
    {
        $this->setStatus('started');
    }

    private function setStatus($status)
    {
        // CHECK first if the status is
        // available in the list from config file
        $statuses = collect(config('recoveryhub.status'));
        
        if ($statuses->get($status)) {
            $this->event->status = strtoupper(config('recoveryhub.status')[$status]);
            $this->event->save();
            return $this->event;
        }

        abort(400, 'Status invalid: (' . $statuses->implode(', ') . ')');
    }
}