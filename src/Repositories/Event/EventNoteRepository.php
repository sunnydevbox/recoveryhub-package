<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use JWTAuth;
use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\UserEventNoteCriteria;

class EventNoteRepository extends TWBaseRepository 
{
    public function model()
    {
        return config('recoveryhub.models.event-note');
    }

    public function boot()
    {
        parent::boot();
        
        $this->pushCriteria(DateRangeCriteria::class);
        $this->pushCriteria(UserEventNoteCriteria::class);
    }
}