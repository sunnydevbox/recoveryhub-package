<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\UserEventNoteCriteria;

class EventPrescriptionItemRepository extends TWBaseRepository 
{
    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\EventPrescriptionItemValidator::class;
    }

    public function model()
    {
        return config('recoveryhub.models.event-prescription-item');
    }

    public function boot()
    {
        parent::boot();
    }


}