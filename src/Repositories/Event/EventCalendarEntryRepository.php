<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Spatie\CalendarLinks\Link;
use Illuminate\Support\Facades\File;

class EventCalendarEntryRepository
{
    protected $appointment;
    protected $calendar;
    protected $targerUser;

    public function upcomingBookings()
    {
        $result = $this->model()
            ->where('start_date', '', '') // START DATE IS XX mins from now
            ->where('status', config('recoveryhub.status.booked')) // STATUS is
            ->get();

        return $result;
    }

    public function setAppointment($appointment)
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function generateForDoctor()
    {
        $this->targerUser = 'doctor';

        $start_at   = $this->appointment->event->start_at;
        $end_at     = $this->appointment->event->end_at;
        $patient    = $this->appointment->patient->getMeta('first_name') . ' ' . $this->appointment->patient->getMeta('last_name');

        $this->generateCalendarFile(
            'Recoveryhub - appointment',
            'Appointment with Patient ' . $patient,
            $start_at,
            $end_at
        );

        return $this;
    }

    public function generateForPatient()
    {
        $this->targerUser = 'patient';

        $start_at   = $this->appointment->event->start_at;
        $end_at     = $this->appointment->event->end_at;
        $doctor    = $this->appointment->event->doctor->getMeta('first_name') . ' ' . $this->appointment->event->doctor->getMeta('last_name');

        $this->generateCalendarFile(
            'Recoveryhub - appointment',
            'Appointment with Dr. ' . $doctor,
            $start_at,
            $end_at
        );

        return $this;
    }

    /**
     * Undocumented function
     *
     * @param string $type  ('file', 'stream')
     * @return void
     */
    public function ics($type = 'file')
    {
        switch(strtolower($type)){
            case 'stream':
                $ics = $this->calendar->ics();
            break;

            
            case 'file': 
            default:
                $file = storage_path('logs/Recoveryhub-Appointment_'. $this->targerUser .'_' .$this->appointment->event->id . '_' .$this->appointment->id .'.ics');
                $ics = str_replace('%0A', "\n", str_replace('data:text/calendar;charset=utf8,','', $this->calendar->ics()));

                $bytes_written = File::put($file, $ics);
                if ($bytes_written === false)
                {
                    die("Error writing to file");
                }

                $ics = $file;
            break;
        }

        return $ics;
    }

    public function google()
    {
        return $this->calendar->google();
    }

    public function yahoo()
    {
        return $this->calendar->yahoo();
    }


    private function generateCalendarFile(
        $label,
        $description,
        $start_at,
        $end_at,
        $address = false
    ) {

        $link = Link::create($label, $start_at, $end_at )
                        ->description($description)
                        ;

        if ($address) {
            $link = $link->address($address);
        }
        
        $this->calendar = $link;

        return $this;
    }
}