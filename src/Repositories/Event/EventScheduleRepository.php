<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Sunnydevbox\TWEvents\Repositories\Event\EventRepository as ExtendEventRepository;
use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;

class EventScheduleRepository extends ExtendEventRepository 
{
    public function upcomingBookings()
    {
        $result = $this->model()
            ->where('start_date', '', '') // START DATE IS XX mins from now
            ->where('status', config('recoveryhub.status.booked')) // STATUS is
            ->get();


        return $result;
    }
}