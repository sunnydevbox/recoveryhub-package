<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\UserEventNoteCriteria;

class EventPrescriptionRepository extends TWBaseRepository 
{
    public function validator()
    {
        return \Sunnydevbox\Recoveryhub\Validators\EventPrescriptionValidator::class;
    }

    public function model()
    {
        return config('recoveryhub.models.event-prescription');
    }

    public function boot()
    {
        parent::boot();
        $this->pushCriteria(\Sunnydevbox\TWCore\Criteria\SearchRelationshipsCriteria::Class);
    }
}