<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event;

use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\UserEventCriteria;
use Sunnydevbox\TWEvents\Repositories\Event\EventRepository as ExtendEventRepository;
use JWTAuth;
use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Exception;

class EventRepository extends ExtendEventRepository 
{
    protected $fieldSearchable = [
        'id',
        'assignable_id',
        'label',
        'start_at',
        'end_at',
        'status',
        'price',
    ];

    public function model()
    {
        return config('recoveryhub.models.event');
    }


    public function getAppointments()
    {
        /*
        select b.* 
        from events e
        left join bookings b ON b.bookable_id=e.id

        where e.assignable_id=2
        and b.id IS NOT NULL
         */
        $user = JWTAuth::parseToken()->authenticate();
        $limit = request()->get('limit') ? request()->get('limit') : config('repository.pagination.limit');

        // Add this line if using Query Builder
        $this->applyCriteria();
        
        // $result = $this
        //     ->model
        //     // ->where('assignable_id','=', $user->id )
        //     ->with(['bookings.assignable'])
        //     ->has('bookings')
            
        //     //->paginate($limit)
        //     ;

        $result = $this
            ->model
            ->with(['bookings.assignable'])
            ->has('bookings')
            ->orderBy('start_at')
            ->paginate(800)
            ;
            
        //dd($result->toSql(), $result->getBindings());
        return $result;
    }

    public function create(array $attributes)
    {
        
        // SET CURRENT USER
        $user = JWTAuth::parseToken()->authenticate();

        $attributes['assignable_type'] = config('recoveryhub.models.user');
        $attributes['assignable_id'] = $user->id;

        // BREAK down the date range
        $list = [];
        foreach($this->breakdown($attributes) as $schedule) {
            $this->validateSelectedSchedule($schedule);
            
            $attr = $attributes;
            $attr['start_at']   = $schedule['start_at'];
            $attr['end_at']     = $schedule['end_at'];
            $attr['status']     = config('recoveryhub.status.available');

            $list[] = $this->makeModel()->create($attr);
        }

        return collect($list);
    }

    public function breakdown($attributes)
    {
        $this->sanitizeRequest($attributes);

        $diff       = $attributes['start_at']->diffInHours($attributes['end_at']);
        $schedules  = [];

        foreach(range(1,$diff) as $index) {
            $start_at = $attributes['start_at']->copy();
            $end_at   = $start_at->copy()->addMinutes(config('recoveryhub.events.interval'));
            $s = [
                'start_at' => $start_at,
                'end_at'   => $end_at,
                'assignable_type' => $attributes['assignable_type'],
                'assignable_id' => $attributes['assignable_id'],
            ];

            $attributes['start_at'] = $end_at;

            if ($this->validateSelectedSchedule($s)) {
                $schedules[] = $s;    
            }
        }

        return $schedules;
    }


    public function update(array $attributes, $id)
    {
        $this->sanitizeRequest($attributes);
        return parent::update($attributes, $id);
    }

    private function sanitizeRequest(&$attributes)
    {
        if (count($attributes) == 1 && isset($attributes['session_id'])) {
            $this->validator = null;
            $attributes['session_id'] = trim($attributes['session_id']);
        } else {

            if (isset($attributes['timezone'])) {
                $timezone = $attributes['timezone'];
            } else { // BY DEFAUL use the user's stored timezone
                $user = JWTAuth::parseToken()->authenticate();
                $timezone = $user->timezone;
            }
            
            $attributes['start_at'] = Carbon::parse($attributes['start_at'], $timezone)->second(0)->setTimezone(config('app.timezone'));
            $attributes['end_at'] = Carbon::parse($attributes['end_at'], $timezone)->second(0)->setTimezone(config('app.timezone'));
        }
    }

    private function validateSelectedSchedule($attributes)
    {
        $now = Carbon::now()->setTimezone(config('app.timezone'));
        $start_at = new Carbon($attributes['start_at']);
        $end_at = new Carbon($attributes['end_at']);

         \Log::info('Creating Schedule', [
            'action' => $attributes,
        ]);


        // CHECK if dates are past dates
        if ($now->gt($start_at) || $now->gt($end_at)) {
            abort(400, 'invalid_date');
            return false;
        }

        $overlap = $this->model->where(function($query) use ($attributes) {
                        $query->where('start_at', '>=', $attributes['start_at'] )
                            ->where('end_at', '<=', $attributes['end_at'])
                            ->where('assignable_id', $attributes['assignable_id'])
                            ->where('assignable_type', $attributes['assignable_type']);
                    })->count();
                    
        if ($overlap > 0) {
            return false;
            // THERE MUST NOT BE ANY OVERLAP
            abort(400, 'invalid_start_date');
        }

        return true;
    }

    public function delete($id)
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->model->find($id);
        if ($model) {
            $deleted = $model->delete();
            
            return $deleted;
        }

        throw new Exception('invalid_id', 400);
    }


    public function boot()
    {
        parent::boot();

        $this->pushCriteria(DateRangeCriteria::class);
        
        // !!!
        $this->pushCriteria(UserEventCriteria::class);
    }
}