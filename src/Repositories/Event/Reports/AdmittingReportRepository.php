<?php
namespace Sunnydevbox\Recoveryhub\Repositories\Event\Reports;

use JWTAuth;
use Carbon\Carbon;
use \Prettus\Validator\Exceptions\ValidatorException;
use Sunnydevbox\TWCore\Repositories\TWBaseRepository;
use Sunnydevbox\Recoveryhub\Criteria\DateRangeCriteria;
use Sunnydevbox\Recoveryhub\Criteria\UserEventNoteCriteria;

use Sunnydevbox\Recoveryhub\Repositories\Event\EventNoteRepository;
use Sunnydevbox\Recoveryhub\Repositories\ReportInterface;

class AdmittingReportRepository extends EventNoteRepository implements ReportInterface
{
    public function reports($data = [])
    {
        $notes = $this->makeModel()->where('event_id', $data['event_id'])
                        ->with(['event.bookings.patient', 'event.doctor'])
                        ->first();

        $data = [
            'id' => isset($notes) ? $notes->event->id : '',
            'notes' => $notes,
        ];

        return $data;
    }
}