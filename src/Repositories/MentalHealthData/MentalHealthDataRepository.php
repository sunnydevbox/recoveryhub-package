<?php
namespace Sunnydevbox\Recoveryhub\Repositories\MentalHealthData;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class MentalHealthDataRepository extends TWBaseRepository
{
    public function model()
    {
        return 'Sunnydevbox\Recoveryhub\Models\MentalHealthData';
    }

    public function formatAttributes(array $attributes)
    {
        $dataAttr = [
            "reasonForConsultation" => [
                "patient" => null
            ],
            "familyInformation" => [
                "familyRelationship" => [
                    []
                ],
                "birthRank" => null,
                "noOfSiblings" => null,
                "noOfSupportiveSiblings" => null,
                "noOfChildren" => null,
                "noOfSupportiveChildren" => null,
                "otherRemarks" => null
            ],
            "financialStatusAndLivingArrangement" => [
                "financialSituation" => null,
                "monthlyIncome" => null,
                "dependentValue" => null,
                "financialSituationDependentOther" => null,
                "sourceOfIncome" => null,
                "sourceOfIncomeEmployedOccupation" => null,
                "sourceOfIncomeOther" => null,
                "sourceOfIncomeUnemployedReason" => null,
                "livingArrangement" => null,
                "livingArrangementUnrestrained" => null,
                "livingArrangementRestrained" => null,
                "estimatedFamilyIncome" => null
            ],
            "academicAndOccupationalPerformance" => [
                "academicPerformance" => null,
                "academicPerformanceRemarks" => null,
                "occupationalPerformance" => null,
                "occupationalPerformanceRemarks" => null
            ],
            "socialHistory" => [
                "friends" => null,
                "friendsOthers" => null,
                "leisureFreeTimeSpent" => null,
                "activities" => null,
                "otherSocialSupport" => null,
                "otherSocialSupportOthers" => null
            ],
            "religiousOrSpiritualHistory" => [
                "details" => null
            ],
            "militaryHistory" => [
                "attended" => null,
                "attendedOther" => null,
                "served" => null,
                "servedOther" => null,
                "status" => null,
                "statusOther" => null,
                "combatExperience" => null,
                "combatExperienceDetails" => null
            ],
            "legalHistory" => [
                "legalInvolvement" => [
                    []
                ]
            ],
            "otherRelevantHistory" => [
                "otherRelevantHistoryDetails" => null
            ],
            "historyOfPresentIllness" => [
                "historyOfPresentIllness" => [
                    []
                ]
            ],
            "pastPsychiatricAndMedicalIllnesses" => [
                "patientPsychHistory" => [
                    []
                ],
                "pastMedicalHistory" => [
                    []
                ]
            ],
            "familyHistoryOfPsychiatricAndMedicalIllnesses" => [
                "famPsychHistory" => [
                    []
                ],
                "famMedicalHistory" => [
                    []
                ]
            ],
            "developmentalHistory" => [
                "developmentalHistory" => null,
                "abnormalDevelopmentalHistory" => [],
                "otherSignificantEvents" => null,
                "significantPrenatalOrPerinatalEvent" => null
            ],
            "substanceUseAddictionHistory" => [
                "substanceUseOrAddictionHistory" => null
            ],
            "sexualAndObGyneHistory" => [
                "genderPreference" => null,
                "courtshipExperience" => null,
                "courtshipExperienceYesValue" => null,
                "courtshipExperienceYesDetail" => null,
                "sexualExperience" => null,
                "sexualExpDetails" => null,
                "ageOfFirstMenstruation" => null,
                "obScoreG" => null,
                "obScoreP" => null,
                "menstrualCycle" => null,
                "gynecologicalIllnesses" => null,
                "menopauseAgeOfOnset" => null,
                "otherSignificantSexualityExperience" => null,
                "hivTested" => null,
                "otherStds" => null,
                "otherStdStatus" => null,
                "significantUrologicHistory" => null
            ],
            "otherData" => [
                "treatmentSetting" => null,
                "payment" => null,
                "typeOfConsult" => null,
                "sourceOfInformation" => null,
                "sourceInfoName" => null,
                "sourceInfoRelationship" => null
            ],
            "sourceOfReferral" => [
                "otherData" => null
            ]
        ];

        $newAttribute = [];

        foreach ($dataAttr as $key => $value) {
            if (isset($attributes[$key])) {
                $newAttribute[$key] = $attributes[$key];
            } else {
                $newAttribute[$key] = $dataAttr[$key];
            }
        }

        $newAttribute['agreeToCollectionAndProcessing'] = isset($attributes['agreeToCollectionAndProcessing']) ? $attributes['agreeToCollectionAndProcessing'] : false;

        return $newAttribute;
    }

    public function getByUserId($userId)
    {
        return $result = $this->model
            ->where('user_id', $userId)
            ->first();
    }
}