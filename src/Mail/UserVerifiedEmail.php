<?php

namespace Sunnydevbox\Recoveryhub\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\Recoveryhub\Models\User;

class UserVerifiedEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = (!method_exists($this->user, 'getMeta')) ? '' : $this->user->getMeta('first_name') . ' ' . $this->user->getMeta('last_name');
        // if (

        // )
        // $name = $this->user->getMeta('first_name')
        return $this->view('recoveryhub::mail.verified')
                    ->with([
                        'name' => $name,
                    ])
                    ->subject('RecoveryHub :: Welcome to RecoveryHub!')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
