<?php

namespace Sunnydevbox\Recoveryhub\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\Recoveryhub\Models\User;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $verificationUrl = config('app.app_url')
        . 'users/verify/' 
        . urlencode($this->user->email) 
        . '/' 
        . $this->user->verification_token;

        return $this->view('recoveryhub::mail.verification')
                    ->with([
                        'user' => $this->user->getMeta('first_name'),
                        'verification_url' => $verificationUrl,
                    ])
                    ->subject('RecoveryHub :: Please verify your email')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
