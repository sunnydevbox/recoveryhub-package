<?php

namespace Sunnydevbox\Recoveryhub\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\Recoveryhub\Models\Event;

class EventReminder extends Mailable
{
    use Queueable, SerializesModels;

    public $event;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $userTimezone = $this->event->doctor->timezone;

        $date = $this->event->start_at->copy()->setTimezone($userTimezone)->format('F d, Y');
        $time = $this->event->start_at->copy()->setTimezone($userTimezone)->format('h:iA') . ' to ' . $this->event->end_at->copy()->setTimezone($userTimezone)->format('h:iA');

        return $this->view('recoveryhub::mail.event-reminder')
                    ->with([
                        'event' => $this->event,
                        'date'  => $date,
                        'time'  => $time,                  
                    ])
                    ->subject('RecoveryHub :: Appointment Reminder')
                    ->to($this->event->doctor->email, $this->event->doctor->first_name);
    }
}
