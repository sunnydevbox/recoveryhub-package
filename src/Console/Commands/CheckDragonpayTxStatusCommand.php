<?php
namespace Sunnydevbox\Recoveryhub\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;
use Sunnydevbox\Recoveryhub\Models\Event;
use Sunnydevbox\Recoveryhub\Models\Appointment;
use Carbon\Carbon;

class CheckDragonpayTxStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recoveryhub:dragonpay-status {--txnid=} }{--tx=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ReoveryHub - publish initial migration files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        // \Sunnydevbox\Recoveryhub\Repositories\Cart\CartOrderRepository $repository
        \Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository $repository
    ) {
        parent::__construct();

        $this->repository = $repository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $this->info('Running RecoveryHub-Dragonpay files...');

        $txnid = $this->option('txnid');

        if ($tx = $this->option('tx')) {
            
            $json = '{"params":{"merchantid":"CEBUMETRO","txnid":"5e81af75bf3c4","refdate":"2020-03-30T16:36:10.129","amount":3360,"currency":"PHP","description":"Cebu Metropsych payment","status":"U","email":"caparida.voiceville@gmail.com","mobileno":"","procid":"GPCS","settledate":"2020-03-30T16:36:10.129","param1":"5e81af75bf413","param2":"","message":"{\"merchantid\":\"CEBUMETRO\",\"txnid\":\"5e81af75bf3c4\",\"refdate\":\"2020-03-30T16:36:10.129\",\"amount\":3360,\"currency\":\"PHP\",\"description\":\"Cebu Metropsych payment\",\"status\":\"U\",\"email\":\"caparida.voiceville@gmail.com\",\"mobileno\":\"\",\"procid\":\"GPCS\",\"settledate\":\"2020-03-30T16:36:10.129\",\"param1\":\"5e81af75bf413\",\"param2\":\"\"}"}}';


            dd(json_decode($json));



            // Appointment::create([
            //     'bookable_id'=>   3443,
            //     'bookable_type'=>   'Sunnydevbox\Recoveryhub\Models\Event',
            //     'assignable_id'=>   266,
            //     'assignable_type'=>   'Sunnydevbox\Recoveryhub\Models\User',
            // ]);

            
            // $App= Appointment::find($tx);
            


            // $App->deleted_at = Carbon::now();
            // $App->update();

            // dd($App);

            $url =  config('recoveryhub.dragonpay_rest_api_url') . "txnid/{$tx}";            
            $client = new \GuzzleHttp\Client();

            $request = $client->request('get',
                "{$url}" , 
                [
                    'auth' => [
                        config('recoveryhub.dragonpay_merchant_id'),
                        config('recoveryhub.dragonpay_merchant_key')
                    ]
                ])
            ;

            $response = json_decode($request->getBody(), true);
            dd($response);
        }

        if ($txnid) {

            
            // $pendingOrders = $this->repository->makeModel()
            //                     // ->isPending()
            //                     ->with(['transactions', 'items.object'])
            //                     ->whereDoesntHave('items', function($query) {
                                    
            //                     })
            //                     // ->whereHas('transactions', function($query) use ($txnid) {
            //                     //     $query->where('transaction_id', $txnid);
            //                     // })
            //                     // ->whereHas('items', function($query) {
            //                     //     $query->where('class', 'Sunnydevbox\\Recoveryhub\\Models\\Event')
            //                     //     ;
                                    
            //                     // })
            //                     ->where('start_at', '>=', '2020-03-29 0:0)')
            //                     // ->where('created_at', '<=', '2020-03-26 23:59:0)')
            //                     ->limit(1)
            //                     ->orderBy('created_at', 'desc')
            //                     ->get()
                                ;
            

            /*
            $pendingOrders = $this->repository->makeModel()
                                ->select([
                                    'events.id as EID',
                                    'ci.id as CIID', 
                                    'co.id as COID',
                                    'co.statusCode'
                                ])
                                ->with(['cartItemA.orders'])
                                ->where('events.status', 'RESERVED')
                                ->where('events.start_at','>=', '2020-03-29 0:0:0')
                                ->leftJoin('cart_items as ci', 'ci.reference_id', '=', 'events.id')
                                ->leftJoin('cart_orders as co', 'co.id', '=', 'ci.order_id')
                                ->where('ci.class','Sunnydevbox\\Recoveryhub\\Models\\Event')
                                // ->whereDoesntHave('cartItem.order',function($query) {
                                //     $query->where('statusCode', 'completed');
                                // })
                                // ->where('co.statusCode', 'pending')
                                // ->with(['cartItemA.order.transactions'])
                                // ->whereHas('cartItem.order.transactions', function($query) {
                                //     $query->where('transaction_id', '5e69b1bc32acd');
                                // })
                                ->limit(3)
                                ->orderBy('events.start_at', 'desc')
                                ->get()
                                ;

                // print_r($pendingOrders->getBindings());
                // dd($pendingOrders->toSql());
                                // $pendingOrders->get()

                                ;
                */

                $pendingOrders = $this->repository->makeModel()
                                    ->with(['cartItemA.order.transactions'])
                                    ->where('status', 'RESERVED')
                                    ->where('events.start_at','>=', '2020-03-31 0:0:0')
                                    ->where('events.start_at','<=', '2020-03-31 23:59:0')
                                    ->orderBy('events.start_at', 'desc')
                                    ->orderBy('events.id', 'desc')
                                    
                                    ->limit(1)
                                    ->get()
                                    ;


            // dd($pendingOrders->toArray());

            $eventId = null;
            $newData = [];
            $pendingOrders->each(function($event) use (&$eventId, &$newData) {
                if($eventId != $event->id) {;
                    $eventId = $event->id;
                    $newData[$eventId] = [];
                    
                    $event->cartItemA->each(function($item) use (&$eventId, &$newData) {
                        // dd($item->order->statusCode);
                        // var_dump($eventId, $item->order->id, $item->order->statusCode);
                        if ($item->order->statusCode == 'completed') {

                            // DELETE THE CURRENT
                            unset($newData[$eventId]);
                            $eventId = null;
                        } else {
                            $newData[$eventId][] = $item->order;
                        }
                    });
                    
                }


                
            });

            // if ($pendingOrders->count()) {
            if (count($newData)) {
                
                // $pendingOrders->each(function($order) {
                collect($newData)->each(function($order) {
                    // dd(1);//$order);
                    
                    collect($order)->each(function($order) {
                        event(new \Sunnydevbox\Recoveryhub\Events\DragonpayTxStatusCheckEvent($order));
                    });
                    
                });
            } else {
                $this->error('Transaction '. $txnid. ' NOT FOUND or it was previously processed');
            }
        } else {
            $this->error('Transaction ID not specified: [--txnid=]');
        }

    }

    public function fire()
    {
        echo 'fire';
    }
}