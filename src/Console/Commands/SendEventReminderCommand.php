<?php
namespace Sunnydevbox\Recoveryhub\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Illuminate\Support\Facades\Artisan;


class SendEventReminderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'recoveryhub:send-event-reminder {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ReoveryHub - Send Event Reminders to doctors or patients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    { 
        $this->info('Running RecoveryHub migration files...');

        // $type = $this->option('type');
    
        // if ($action) {

        //     $method = ($action == 'run') ? '' : (($action) ? ':' . $action : '');

        //     // RUN TW BOOKINGS migration

        //     $exitCode = Artisan::call('migrate' . $method, [
        //         '--path'    => 'vendor/sunnydevbox/tw-bookings/src/Database/Migrations',
        //     ]);

        //     $this->info(app('Illuminate\Contracts\Console\Kernel')->output());
            
        //     $this->info(' ... RecoveryHub migration DONE');
        // } else {
        //     $this->error('Migration failed');
        //     $this->line('- You must specify the --action option [run/install/refresh/reset/rollback/status]');
        // }
        
    }

    public function fire()
    {
        echo 'fire';
    }
}