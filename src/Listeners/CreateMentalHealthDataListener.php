<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Sunnydevbox\Recoveryhub\Repositories\MentalHealthData\MentalHealthDataRepository;

class CreateMentalHealthDataListener
{
    public $rpoMentalHealthData;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MentalHealthDataRepository $rpoMentalHealthData)
    {
        $this->rpoMentalHealthData = $rpoMentalHealthData;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->patient->hasRole(['patient'])) {
            $patientData = $this->rpoMentalHealthData->formatAttributes([]);
            $this->rpoMentalHealthData->create([
                'user_id' => $event->patient->id,
                'patient_data' => $patientData
            ]);
        }
    }
}
