<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Mail\EventReminder;
use Sunnydevbox\Recoveryhub\Events\EventReminderEvent;

class SendEventReminderListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(EventReminderEvent $event)
    {
        \Log::info('sending_event_reminder', [
            'id' => $event->user->id,
        ]);

        // DO THE EMAIL
        Mail::to($event->event->user->email)
            ->send(new EventReminder($event->user));
    }
}
