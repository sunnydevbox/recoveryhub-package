<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Services\EventPrescriptionService;
 
class SendEventPrescriptionEmailListener 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EventPrescriptionService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {   
        
        if ($event->prescription) {
            
            $doctorEvent = $event->prescription;
            
            $fileAttachments = [];

            if ($doctorEvent->bookings) {
                
                $patient = $doctorEvent->bookings->patient;
                    
                $fileAttachments[uniqid()] = $this->service
                                            ->generatePrescriptionPDF($doctorEvent);

                // SEND MAIL
                Mail::send(
                    'recoveryhub::mail.prescription-notification',
                    ['patient' => $patient],
                    function ($message) use ($doctorEvent, $patient, $fileAttachments) {
                        $m = $message->subject('RecoveryHub :: Your prescription is ready')
                            ->to($patient->email, $patient->first_name . ' ' . $patient->last_name);
                    
                        foreach($fileAttachments as $key => $file) {
                            $m->attach($file, ['as' => 'Recoveryhub-Prescription-' . $doctorEvent->id . '-' . $key . '.pdf']);
                        }
                    }
                );
            }
        }
    }
}
