<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService;
use Sunnydevbox\Recoveryhub\Services\EventPrescriptionService;

class CheckoutNotifyLifePortListener 
{
    public $service;
    public $eventPrescriptionService;
    public $rpo;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PrescriptionTransactionService $service,
        EventPrescriptionService $eventPrescriptionService,
        \Sunnydevbox\Recoveryhub\Repositories\PrescriptionTransaction\PrescriptionTransactionRepository $rpo
    ){
        $this->service = $service;
        $this->eventPrescriptionService = $eventPrescriptionService;
        $this->rpo = $rpo;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($order)
    {   

        if ($order->item) {
            // echo 'this';

            $fileAttachments;
            $orderData;
            $patient;

            // dd($order->item->transactions[0]->transaction_id);

            foreach( $order->item->items as $item ){
                
                // $e = app($item->class);
                // $t = $this->rpo->findOrFail($item->reference_id);
                $t = $this->service->fetchPrescription($item->reference_id);

                if ( $t ) {
                    
                    $eventPrescription = $this->service->getEventPrescription($t->prescription_item_id);
                    $prescriptions = $this->service->getPrescription($t->prescription_item_id);
                    $doctorEvent = $eventPrescription->event;

                    $patient = $doctorEvent->bookings->patient;

                    $verifyUrl[] = config('app.app_url')
                                . 'admin/orders/view/'. $t->id;

                    $orderData[] = [
                        'medicineName' => $prescriptions->medicine->name,
                        'medicineUnit' => $prescriptions->medicine->unit,
                        'medicineDosage' => $prescriptions->medicine->dosage,
                        'qty' => $item->quantity,
                        'url' => config('app.app_url') . 'admin/orders/view/'. $t->id
                    ];

                    

                }
            }

            if ( $t ) {

                $fileAttachments[uniqid()] = $this->eventPrescriptionService
                                                ->generatePrescriptionPDF($doctorEvent);

                Mail::send(
                    'recoveryhub::mail.prescription-notification-lifeport',
                    ['patient' => $patient, 'order' => $orderData, 'verifyUrl' => $verifyUrl],
                    function ($message) use ($doctorEvent, $patient, $fileAttachments, $order) {
                        $m = $message->subject('RecoveryHub :: New order has been made by '. $patient->first_name .' '. $patient->last_name . ' ('.$order->item->transactions[0]->transaction_id.') ' )
                            ->to(config('recoveryhub.lifeport_email'));
                    
                        foreach($fileAttachments as $key => $file) {
                            $m->attach($file, ['as' => 'Recoveryhub-Prescription-' . $doctorEvent->id . '-' . $key . '.pdf']);
                        }
                    }
                );

                Mail::send(
                    'recoveryhub::mail.prescription-notification-patient',
                    ['patient' => $patient, 'order' => $orderData, 'verifyUrl' => $verifyUrl, 'transactionNo' => $order->item->transactions[0]->transaction_id ],
                    function ($message) use ($doctorEvent, $patient, $fileAttachments, $order) {
                        $m = $message->subject('RecoveryHub :: Thank you for the purchase! ('.$order->item->transactions[0]->transaction_id.') ')
                            ->to($patient->email, $patient->first_name . ' ' . $patient->last_name);
                    

                    }
                );

            }

        }

        
    }
}
