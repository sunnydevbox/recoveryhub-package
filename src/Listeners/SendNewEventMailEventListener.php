<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Mail\EventCreatedConfirmation;
use Sunnydevbox\Recoveryhub\Events\EventCreatedEvent;

class SendNewEventMailEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(EventCreatedEvent $event)
    {
        \Log::info('sending_new_event_confirmation', [
            'id' => $event->event->id ,
        ]);

        // DO THE EMAIL
        Mail::to($event->event->user->email)
            ->send(new EventCreatedConfirmation($event->event));
    }
}
