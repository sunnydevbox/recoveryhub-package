<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService;
use \Sunnydevbox\Recoveryhub\Services\CartService;
use Illuminate\Support\Facades\Mail;

class DragonpayTxStatusParserListener
{
    public $service;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PrescriptionTransactionService $service,
        CartService $cartService
    ){
        $this->service = $service;
        $this->cartService = $cartService;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {   
        // dd(3);
        try {
            $txnid = $event->order->transactions[0]->transaction_id;
            // $txnid = '5e575a6c17286';
            $url =  config('recoveryhub.dragonpay_rest_api_url') . "txnid/{$txnid}";

            \Log::info("Dragonpay Check TXN: {$txnid}");
            
            $client = new \GuzzleHttp\Client();

            $request = $client->request('get',
                "{$url}" , 
                [
                    'auth' => [
                        config('recoveryhub.dragonpay_merchant_id'),
                        config('recoveryhub.dragonpay_merchant_key')
                    ]
                ])
            ;

            $response = json_decode($request->getBody(), true);
            
            foreach($response as $key => $value) {
                $response[strtolower($key)] = $value;
                unset($response[$key]);
            }

            // $response = [
            //     'status' =>'U',
            //     'txnid' => '5d89b1d5eff73',
            //     'param1' => 123,
            // ];
            
            if($response['status'] == 'U' || $response['status'] == 'F') {

                $gw = $this->cartService->setGateway('dragonpay')->gateway;
                $response['message'] = json_encode($response);
                $order = $gw->onCallbackSuccess_CLI_Dragonpay_check_status('', $response);

                $order->items->each(function($item, $key) {

                    $event = $item->object;
                    // send email ONLY if the event status is NOT "OPEN"
                    // because sometimes the patient already deleted his appointment before 
                    // this cron has run
                    if (
                        is_numeric(strpos($item->class, 'Models\\Event'))
                        && $event->status == 'RESERVED'
                    ) {
                        // 2) SWITCH EVENT status from 'RESERVED' to 'OPEN'
                        \Log::info("changin EVENT status from {$item->object->status} to ". $item->object::STATUS_OPEN);
                        $item->object->update([
                            'status' => $item->object::STATUS_OPEN
                        ]);

                        // SEND EMAIL
                        $doctor = $event->doctor;
                        $patient = $event->bookings()->withTrashed()->first()->patient;
                        $doctorName = "{$doctor->first_name} {$doctor->last_name}";
                        $patientName = "{$patient->first_name} {$patient->last_name}";
                        $doctorEmail = $doctor->email;
                        $patientEmail = $patient->email;
                        $availability = "{$event->start_at->format('D. M d, Y')} {$event->start_at->format('h:iA')} - {$event->end_at->format('h:iA')}";
                        
                        \Log::info('Dragonpay Status: Email sent to doctor ' . $doctorEmail);

                        Mail::send(
                            'recoveryhub::mail.doctor-schedule-reopened',
                            [
                                'doctor' => $doctorName,
                                'availability' => $availability,
                            ],
                            function ($message) use ($doctor, $doctorName, $doctorEmail) {
                                $m = $message->subject('Recoveryhub :: One of your reserved availability reopened')
                                    ->from('info@recoveryhub.ph', 'RecoveryHub Team')
                                    ->to($doctorEmail, $doctorName);
                            }
                        );
                        
                        Mail::send(
                            'recoveryhub::mail.patient-schedule-reopened',
                            [
                                'orderUrl' => url("/purchase/orders/{$item->order_id}"),
                                'orderNumber' => $item->order_id,
                                'patient' => $patientName,
                                'availability' => $availability,
                                'doctorName' => $doctorName,
                            ],
                            function ($message) use ($patient, $patientName, $patientEmail) {
                                $m = $message->subject('Recoveryhub :: Your booking was cancelled')
                                    ->from('info@recoveryhub.ph', 'RecoveryHub Team')
                                    ->to($patientEmail, $patientName);
                            }
                        );

                        \Log::info('Dragonpay Status: Email sent to patient ' . $patientEmail);
                    }
                });
            }
        } catch(\Exception $e) {
           //  dd($e);
        }
    }


}
