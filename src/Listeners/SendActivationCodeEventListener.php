<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Events\UserRegisteredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Mail\UserRegistered;

class SendActivationCodeEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event)
    {
        \Log::info('sending_activation_code', [
            'id' => $event->user->id ,
            'email' => $event->user->email,
        ]);

        // DO THE EMAIL
        // dd($event->user->email);
        Mail::to($event->user->email)->send(new UserRegistered($event->user));
    }
}
