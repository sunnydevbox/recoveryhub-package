<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventCalendarEntryRepository;

class SendAppointmentEmailNotificationToDoctorListener implements ShouldQueue
{
    public $rpoEventCalendarEntry;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->rpoEventCalendarEntry = new EventCalendarEntryRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AppointmentCreatedEvent $event)
    {
        $appointment = $event->appointment;
        
        if ($appointment->event->isBooked()) {
            \Log::info('sending_new_appointment_to_doctor', [
                'event_id' => $event->appointment->bookable->id ,
                'appointment_id'    => $event->appointment->id,
            ]);
    
            // SEND EMAIL TO DOCTOR
            $EventCalendarEntry = $this->rpoEventCalendarEntry->setAppointment($appointment)->generateForDoctor();
            $icsFile = $EventCalendarEntry->ics();

            $userTimezone = $appointment->bookable->doctor->timezone;
                
            $date = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('F d, Y');
            $time = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('h:iA') . ' to ' . $event->appointment->event->end_at->copy()->setTimezone($userTimezone)->format('h:iA');
            $doctor = $event->appointment->event->doctor->getMeta('first_name') . ' ' . $event->appointment->event->doctor->getMeta('last_name');
            $patient = $event->appointment->patient->getMeta('first_name') . ' ' . $event->appointment->patient->getMeta('last_name');

            $data = [
                'date'      => $date,
                'time'      => $time,
                'doctor'    => $doctor,
                'patient'   => $patient,
            ];

            Mail::send('recoveryhub::mail.appointment-created-to-doctor', $data, function($message) use ($appointment, $patient, $doctor, $icsFile) {
                    $message->subject('RecoveryHub :: Patient ' . $patient . ' booked an appointment')
                            ->to($appointment->event->doctor->email, $doctor)
                            ->attach($icsFile,['as' => 'Recoveryhub Appointment.ics']);
                }
            );

            if(!Mail::failures()){
                //Unlink the attachement file from local
                unlink($icsFile); 
            }
        }
    }
}
