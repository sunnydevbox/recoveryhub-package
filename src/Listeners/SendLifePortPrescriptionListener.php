<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

// use Sunnydevbox\Recoveryhub\Services\EventPrescriptionService;

class SendLifePortPrescriptionListener 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {   
        // dd("1",$event->prescriptionTransaction->event->bookings);
        if ($event->prescriptionTransaction) {
            $doctorEvent = $event->prescriptionTransaction->event;

            if ($doctorEvent->bookings) {

                $patient = $doctorEvent->bookings->patient;

                $prescriptionUrl = config('app.app_url')
                    . 'me/prescriptions/';

                $data = [
                    'patient' => $patient,
                    'prescriptionUrl' => $prescriptionUrl
                ];                

                // SEND MAIL
                Mail::send(
                    'recoveryhub::mail.prescription-sent-to-lifeport-notification',
                    $data,
                    function ($message) use ($doctorEvent, $patient, $prescriptionUrl) {
                        $m = $message->subject('RecoveryHub :: Your prescription is ready Check link to purchase')
                            ->to($patient->email, $patient->first_name . ' ' . $patient->last_name);
                    
                    }
                );
            }
        }
    }
}
