<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService;

class PrescriptionTransactionListener
{
    public $service;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PrescriptionTransactionService $service
    ){
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {

        $this->service->updateTotalQty($event->prescriptionTransaction);

    }


}
