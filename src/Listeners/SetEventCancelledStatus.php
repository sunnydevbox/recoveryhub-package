<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventStatusRepository;

class SetEventCancelledStatus
{
    public $rpoEventStatus;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EventStatusRepository $rpoEventStatus)
    {
        $this->rpoEventStatus = $rpoEventStatus;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $event = (isset($event->appointment) ) ? $event->appointment->event : $event->event;

        \Log::info('set_event_booked_status', [
            'event_id' => $event->id 
        ]);
 
        $this->rpoEventStatus->setEvent($event)->cancelled();
    }
}
