<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Events\AppointmentDeletedEvent;


class SendAppointmentCancelEmailNotificationToDoctorListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AppointmentDeletedEvent $event)
    {
        
        \Log::info('sending_cancel_appointment_to_doctor', [
            'event_id' => $event->appointment->event->id ,
            'appointment_id'    => $event->appointment->id,
        ]);

        // SEND EMAIL TO PATIENT
        $appointment = $event->appointment;

        $userTimezone = $appointment->event->doctor->timezone;

        $date = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('F d, Y');
        $time = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('h:iA') . ' to ' . $event->appointment->event->end_at->copy()->setTimezone($userTimezone)->format('h:iA');
        $doctor = $event->appointment->event->doctor->getMeta('first_name') . ' ' . $event->appointment->event->doctor->getMeta('last_name');
        $patient = $event->appointment->patient->getMeta('first_name') . ' ' . $event->appointment->patient->getMeta('last_name');

        $data = [
            'date'      => $date,
            'time'      => $time,
            'doctor'    => $doctor,
            'patient'   => $patient,
        ];

        Mail::send('recoveryhub::mail.appointment-deleted-to-doctor', $data, function($message) use ($appointment, $patient) {
                $message->subject('RecoveryHub :: You have booked an appointment')
                        ->to($appointment->patient->email, $patient);
            }
        );
    }
}
