<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Repositories\RateCharge\RateChargeRepository;

class DoctorSetDefaultChargeValues
{
    public $rpoRateCharge;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(RateChargeRepository $rpoRateCharge)
    {
        $this->rpoRateCharge = $rpoRateCharge;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $this->rpoRateCharge->setStartingRate($event->doctor->id);
    }
}
