<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Services\ReportService;


class SendEmailTypeEventListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        $fileAttachments = [];
   
        if ($event) {

            $resEvent = $event->getEvent();

            $data['type'] = $event->data["type"];
            $data['event_id'] = $event->data["event_id"];

            $fileAttachments = $event->attachment;

            if (!$resEvent) {
                throw new Exception('Event Not Found!');
            }

            if (!$resEvent->bookings->patient->email) {
                throw new Exception('Email Address Empty!');
            }

            $data['patient_email'] = $resEvent->bookings->patient->email;
            $data['patient_first_name'] = $resEvent->bookings->patient->getMeta('first_name');
            $data['patient_last_name'] = $resEvent->bookings->patient->getMeta('last_name');
            $data['subject'] = '';
            $data['body'] = '';
            
            switch($data['type']){
                case 'feedback': 
                    $data['subject'] = 'RecoveryHub :: Your Feedback Request is ready';
                    $data['body'] = 'Your Feedback Request is ready.';
                break;
                case 'certificate': 
                    $data['subject'] = 'RecoveryHub :: Your Medical Certificate is ready';
                    $data['body'] = 'Your Medical Certificate is ready.';
                break;
                case 'laboratory': 
                    $data['subject'] = 'RecoveryHub :: Your Laboratory Request is ready';
                    $data['body'] = 'Your Laboratory Request is ready.';
                break;
                case 'admitting': 
                    $data['subject'] = 'RecoveryHub :: Your Admitting Request is ready';
                    $data['body'] = 'Your Admitting Request is ready.';
                break;
                default : 
                    $data['subject'] = 'RecoveryHub :: '.ucfirst($data['type']).' PDF Ready';
                    $data['body'] = 'Your '.ucfirst($data['type']).' Request is Ready.';

                
            }
            
            Mail::send('recoveryhub::mail.event-send-mail', $data, function($message) use ($fileAttachments, $data) {
                $message->subject($data['subject'])
                        ->to($data['patient_email'], $data['patient_first_name'] . ' ' . $data['patient_last_name'])
                        ->attach($fileAttachments,['as' => 'Recoveryhub '. ucfirst($data['type']) .'.pdf']);
                }
            );
            
        }

    }
}
