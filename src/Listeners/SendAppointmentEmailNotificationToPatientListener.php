<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Events\AppointmentCreatedEvent;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventCalendarEntryRepository;


class SendAppointmentEmailNotificationToPatientListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->rpoEventCalendarEntry = new EventCalendarEntryRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AppointmentCreatedEvent $event)
    {
        $appointment = $event->appointment;
        
        if ($appointment->event->isBooked()) {

            \Log::info('sending_new_appointment_to_patient', [
                'event_id' => $event->appointment->event->id ,
                'appointment_id'    => $event->appointment->id,
            ]);

            // SEND EMAIL TO PATIENT
            $EventCalendarEntry = $this->rpoEventCalendarEntry->setAppointment($appointment)->generateForPatient();
            $icsFile = $EventCalendarEntry->ics();

            $userTimezone = $appointment->event->doctor->timezone;

            $date = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('F d, Y');
            $time = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('h:iA') . ' to ' . $event->appointment->event->end_at->copy()->setTimezone($userTimezone)->format('h:iA');
            $doctor = $event->appointment->event->doctor->getMeta('first_name') . ' ' . $event->appointment->event->doctor->getMeta('last_name');
            $patient = $event->appointment->patient->getMeta('first_name') . ' ' . $event->appointment->patient->getMeta('last_name');

            $data = [
                'date'      => $date,
                'time'      => $time,
                'doctor'    => $doctor,
                'patient'   => $patient,
            ];


            $email = $appointment->patient->email;
            $subject = 'RecoveryHub :: You have booked an appointment';

            if ($appointment->patient->hasRole('in-patient')) {
                $email = config('recoveryhub.organization_email');
                $subject = "Recoveryhub IP: {$patient} booked appointment";
            }

            Mail::send('recoveryhub::mail.appointment-created-to-patient', $data, function($message) use ($appointment, $patient, $icsFile, $email, $subject) {
                    $message->subject($subject)
                            ->to($email, $patient)
                            ->attach($icsFile,['as' => 'Recoveryhub Appointment.ics']);
                }
            );

            if(!Mail::failures()){
                //Unlink the attachement file from local
                unlink($icsFile); 
            }
        }
    }
}
