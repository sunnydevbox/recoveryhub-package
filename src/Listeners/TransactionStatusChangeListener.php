<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Services\PrescriptionTransactionService;

class TransactionStatusChangeListener 
{
    public $service;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        PrescriptionTransactionService $service
    ){
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($data)
    {   
        // echo 'handle'; dd($data->data['request']->status);

        Mail::send(
            'recoveryhub::mail.order-status-changed',
            ['user' => $data->data['user'], 'transaction' => $data->data['transaction'], 'request' => $data->data['request'] ],
            function ($message) use ($data) {
                $m = $message->subject('RecoveryHub :: Order Status Updated ( '. $data->data['transaction']->transaction_id .' )')
                    ->to($data->data['user']->email, $data->data['user']->first_name . ' ' . $data->data['user']->last_name);
            }
        );

    }
}
