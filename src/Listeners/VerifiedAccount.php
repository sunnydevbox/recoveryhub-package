<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Mail\UserVerifiedEmail;

class VerifiedAccount implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        \Log::info('updated_user_verfied_email', [
            'id' => $event->user->id ,
        ]);

        $event->user->is_verified           =  date('Y-m-d H:i:s');
        $event->user->verification_token    =  null;
        $event->user->status                = 'active';
        $event->user->save();
    }
}
