<?php
namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Mail\EventReminder;
use Sunnydevbox\Recoveryhub\Events\ContactFormSubmittedEvent;

class SendContactFormEmailListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
        \Log::info(config('recoveryhub.generalEmail'));
        $object = $event->contactFormSubmission;

        // DO THE EMAIL
        Mail::send('recoveryhub::mail.contact-form-submission', ['body' => $object->contents, 'attibutes' => $object->form_data], function ($m) use ($object) {
            $m->from('info@recoveryhub.ph', 'RecoveryHub');

            $m->to(config('recoveryhub.generalEmail'))
                ->replyTo($object->from)
                ->subject($object->subject);
        });
    }
}
