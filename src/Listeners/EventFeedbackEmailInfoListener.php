<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Sunnydevbox\Recoveryhub\Repositories\Patient\PatientRepository;
use Sunnydevbox\Recoveryhub\Repositories\Event\EventRepository;


class EventFeedbackEmailInfoListener 
{
    public $rpoPatient;
    public $rpoEventRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PatientRepository $rpoPatient, EventRepository $rpoEventRepository)
    {
        $this->rpoPatient = $rpoPatient;
        $this->rpoEventRepository = $rpoEventRepository;
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($eventFeedback)
    {   
        // $patient = $this->rpoPatient->find( $eventFeedback->eventFeedback->event->assignable_id );
        
        $event = $this->rpoEventRepository->skipCriteria()->find($eventFeedback->eventFeedback->event->id);

        // echo 'handle';
        // dd( $event->bookings->patient );
        $patient = $event->bookings->patient;
        // SEND MAIL to Patient
        Mail::send(
            'recoveryhub::mail.feedback-info',
            ['eventFeedback' => $eventFeedback->eventFeedback, 'patient' => $patient],
            function ($message) use ($patient) {
                $m = $message->subject('RecoveryHub :: Event Feedback From '. $patient->first_name . ' ' . $patient->last_name)
                    ->to(config('recoveryhub.info_recovery_email'));
            }
        );
    }
}
