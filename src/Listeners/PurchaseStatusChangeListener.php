<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Repositories\Cart\CartOrderRepository;
use Amsgames\LaravelShop\Events\OrderStatusChanged;

class PurchaseStatusChangeListener implements ShouldQueue
{
    use InteractsWithQueue;

    public function __construct(CartOrderRepository $rpoOrder)
    {
        $this->rpoOrder = $rpoOrder;
    }

    /**
     * Handle the event.
     *
     * @param  OrderPurchased $event
     * @return void
     */
    public function handle(OrderStatusChanged $event)
    {
        // Get order model object
        $order = $this->rpoOrder->with(['user', 'transactions'])->find($event->id);
        
        $user = $order->user;
        $newStatus = $event->statusCode;
        $oldStatus = $event->previousStatusCode;


        $email = $user->email;
        $subject = 'RecoveryHub :: Order Status Updated ( '. $order->id .' )';
        $patient = $user->first_name . ' ' . $user->last_name;

        if ($user->hasRole('in-patient')) {
            $email = config('recoveryhub.organization_email');
            $subject = "Recoveryhub IP: {$patient} :: Order Status Updated ({$order->id})";
        }


        // My code here...
        Mail::send(
            'recoveryhub::mail.order-status-changed',
            [
                'user' => $user,
                'order' => $order,
                'user' => $order->user,
                'newStatus' => $event->statusCode,
                'oldStatus' => $event->previousStatusCode,
            ],
            function ($message) use ($order, $user, $email, $subject, $patient) {
                $m = $message->subject($subject)
                    ->to($email, $patient);
            }
        );
    }
}
