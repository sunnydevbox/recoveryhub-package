<?php

namespace Sunnydevbox\Recoveryhub\Listeners;

use Sunnydevbox\Recoveryhub\Models\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Sunnydevbox\Recoveryhub\Events\AppointmentDeletedEvent;

class SendAppointmentCancelEmailNotificationToPatientListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AppointmentDeletedEvent $event)
    {
        
        \Log::info('sending_cancel_appointment_to_patient', [
            'event_id' => $event->appointment->event->id ,
            'appointment_id'    => $event->appointment->id,
        ]);

        // SEND EMAIL TO PATIENT
        $appointment = $event->appointment;

        $userTimezone = $appointment->event->doctor->timezone;

        $date = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('F d, Y');
        $time = $event->appointment->event->start_at->copy()->setTimezone($userTimezone)->format('h:iA') . ' to ' . $event->appointment->event->end_at->copy()->setTimezone($userTimezone)->format('h:iA');
        $doctor = $event->appointment->event->doctor->getMeta('first_name') . ' ' . $event->appointment->event->doctor->getMeta('last_name');
        $patient = $event->appointment->patient->getMeta('first_name') . ' ' . $event->appointment->patient->getMeta('last_name');

        $data = [
            'date'      => $date,
            'time'      => $time,
            'doctor'    => $doctor,
            'patient'   => $patient,
        ];


        $email = $appointment->patient->email;
        $subject = 'RecoveryHub :: You have cancelled an appointment';

        if ($appointment->patient->hasRole('in-patient')) {
            $email = config('recoveryhub.organization_email');
            $subject = "Recoveryhub IP: {$patient} cancelled appointment";
        }

        Mail::send('recoveryhub::mail.appointment-deleted-to-patient', $data, function($message) use ($appointment, $patient, $email, $subject) {
                $message->subject($subject)
                        ->to($email, $patient);
            }
        );
    }
}
