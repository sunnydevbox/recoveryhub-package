<?php
namespace Sunnydevbox\Recoveryhub\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactFormSubmittedEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $contactFormSubmission;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($contactFormSubmission)
    {
        $this->contactFormSubmission = $contactFormSubmission;
    }
}
