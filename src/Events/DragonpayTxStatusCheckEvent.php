<?php

namespace Sunnydevbox\Recoveryhub\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sunnydevbox\Recoveryhub\Models\Event;

use Illuminate\Contracts\Queue\ShouldQueue;

class DragonpayTxStatusCheckEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $order;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order = null)
    {
        $this->order = $order;
    }
}
