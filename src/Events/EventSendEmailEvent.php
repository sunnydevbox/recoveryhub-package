<?php

namespace Sunnydevbox\Recoveryhub\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class EventSendEmailEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    private $event;
    public $data;
    public $attachment;
    
    /**
     * Create a new Send Email Event instance.
     *
     * @return void
     */
    public function __construct($event, $data, $attachment)
    {
        $this->event = $event;
        $this->data = $data;
        $this->attachment = $attachment;
    }

    public function getEvent()
    {
        return $this->event;
    }
}
