<?php

namespace Sunnydevbox\Recoveryhub\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventPrescriptionCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $prescription;
    
    /**
     * Create a new prescr$prescription instance.
     *
     * @return void
     */
    public function __construct($prescription)
    {
        $this->prescription = $prescription;
    }
}
