<?php

namespace Sunnydevbox\Recoveryhub\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Sunnydevbox\Recoveryhub\Models\User;

class OpentokSessionStatus implements ShouldBroadcast
{
    public $session;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($session)
    {
        $this->session = $session;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    { 
        return new PrivateChannel('opentok.session.' . $this->session->session);
    }

    public function broadcastAs()
    {
        return 'my-event';
    }
}
