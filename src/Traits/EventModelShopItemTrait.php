<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Carbon\Carbon;

trait EventModelShopItemTrait
{
    // public $appends = [
    //     'price'
    // ];

    // protected function getArrayableAppends()
    // {
    //     $this->appends = array_unique(array_merge($this->appends, ['sku', 'price']));
    //     return parent::getArrayableAppends();
    // }

    public function cartItem()
    {
        // Cart Item
        return $this->morphMany('Sunnydevbox\Recoveryhub\Models\Item', 'object');
    }


    public function getSkuAttribute($value)
    {
        if (!isset($this->attributes['sku'])) {
            return 'SKU-Ev-' . $this->attributes['id'];
        }

        return $this->attributes['sku'];
    }

    public function getPriceAttribute()
    {
        $price = 0;
        
        // CHECK DOCTOR IF EMPTY
        // ADDED 4-11-2019
        if ( !$this->doctor ) {
            return $this->attributes['price'] = 0;
        }

        if ($rateCharge = $this->doctor->rateCharge()->current($this->start_at->dayOfWeekIso)->first()) {
            $price = $rateCharge->use_default ? $rateCharge->default : $rateCharge->rate;    
        }
        
        $this->attributes['price'] = stringToFloat($price);

        return $this->attributes['price'];
    }

    public function getTaxAttribute()
    {
        if (!isset($this->attributes['tax'])) {
            return (float) 0.12;
        } else {
            $tax = stringToFloat($this->attributes['tax']);

            if (!$tax) {
                $tax = 0.12;
            }

            return (float) $tax;
        }
    }

    public function getTaxAmountAttribute()
    {
        return stringToFloat($this->price * $this->tax);
    }

    public function getTotalAttribute() 
    {
        if($this->created_at->lt(Carbon::createFromFormat('Y-m-d', '2019-12-01'))) {
            return $this->price;
        }

        return stringToFloat($this->price + $this->tax_amount);
    }

    public function getTotalWithTaxAttribute()
    {
        return $this->price + $this->tax_amount;
    }

    public function getDisplayTaxAmountAttribute()
    {
        return $this->price * $this->tax;
    }

    public function getShippingAttribute()
    {
        return 0;
    }


    /** EXTRA **/

    // public function getLessTaxAttribute()
    // {
    //     return stringToFloat($this->price - ($this->price * $this->tax));
    // }

    // public function getProfessionalFeeAttribute()
    // {
    //     return stringToFloat($this->price - $this->admin_fee);
    // }

    // public function getWithholdingTaxAttribute()
    // {
    //     // Fixed value of 5%
    //     return stringToFloat(0.05);
    // }

    // public function getWithholdingAttribute()
    // {
    //     return stringToFloat($this->professional_fee * $this->withholding_tax);
    // }

    // public function getAdminFeeAttribute()
    // {
    //     // Admin fee is 25%
    //     return stringToFloat($this->price * 0.25);
    // }

    // public function getNetIncomeAttribute()
    // {
    //     return stringToFloat($this->professional_fee - $this->withholding_tax - $this->admin_fee);
    // }
}