<?php
namespace Sunnydevbox\Recoveryhub\Traits;

trait OpentokTrait
{ 
    public function opentok()
    {
        return $this->hasOne('Sunnydevbox\Recoveryhub\Models\Opentok', 'event_id');
    }
}