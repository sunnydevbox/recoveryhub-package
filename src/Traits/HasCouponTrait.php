<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Auth;
use Sunnydevbox\Recoveryhub\Models\Cart;


// AppliedCoupon
trait HasCouponTrait
{
    
    /**
     * Get all of the coupons for the order.
     */
    public function coupons()
    {
        return $this->morphMany(
            \Sunnydevbox\Recoveryhub\Models\AppliedCoupon::class
            , 'linkable'
            , 'coupon_link_type'
            , 'coupon_link_id'
        );
        // return $this->morphToMany('Sunnydevbox\Recoveryhub\Models\Coupon', 'coupon_link', config('tw-cart.tables.appliedCoupons'));
    }
}