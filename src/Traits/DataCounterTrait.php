<?php
namespace Sunnydevbox\Recoveryhub\Traits;

trait DataCounterTrait
{
    public $totalFieldCount = 0;
    public $filledFieldCount = 0;
    public $emptyFieldCount = 0;

    public function getPercentageAttribute()
    {
        $percent = ($this->filledFieldCount / $this->totalFieldCount) * 100;
        return round($percent);
    }

    public function getIsMentalDataCompleteAttribute()
    {
        return ($this->filledFieldCount === $this->totalFieldCount) ? true : false;
    }

    /**
     * @param array $data
     */

    public function dataCounter($data)
    {
        $data = collect($data);

        $totalFieldCount = 0;
        $filledFieldCount = 0;
        $requiredFields = [
            'firstName',
            'lastName',
            'sex',
            'civilStatus',
            'age',
            'dateOfBirth',
            'placeOfBirth',
            'religion',
            'nationality',
            'race',
            'ethnicity',
            'tempNoStreet',
            'tempCityMunicipality',
            'tempProvince',
            'tempZipCode',
            'permNoStreet',
            'permCityMunicipality',
            'permProvince',
            'permZipCode',
            'mobileNumber',
            'emailAddress',
            'highestEducationalAttainment',
            'occupation',
            'company',
            // 'companyAddressAndContactDetails',
            'height',
            'weight',
            'birthRank',
            'noOfSiblings',
            'noOfSupportiveSiblings',
            'noOfSupportiveChildren'
        ];

        $data->each(function ($item) use (&$filledFieldCount, &$totalFieldCount, &$requiredFields) {
            collect($item)->each(function ($i, $key) use (&$filledFieldCount, &$totalFieldCount, &$requiredFields) {
                if (in_array($key, $requiredFields, true)) {
                    if (is_array($i)) {
                        if (!empty($i)) {
                            $filledFieldCount++;
                        }
                    } else {
                        if (isset($i)) {
                            $filledFieldCount++;
                        }
                    }
                    $totalFieldCount++;
                }
            });
        });
        $this->totalFieldCount = $totalFieldCount;
        $this->filledFieldCount = $filledFieldCount;
        $this->emptyFieldCount = $totalFieldCount - $filledFieldCount;

        return $this;
    }

}