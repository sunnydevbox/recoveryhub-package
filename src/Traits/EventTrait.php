<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Illuminate\Support\Fluent;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait EventTrait
{ 
    public function events()
    {
        return $this->MorphMany('Sunnydevbox\Recoveryhub\Models\Event', 'assignable');
    }

}