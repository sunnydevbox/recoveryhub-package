<?php
namespace Sunnydevbox\Recoveryhub\Traits;

trait HasEventPrescriptionTrait
{
    public function prescriptions()
    {
        return $this->hasMany(config('recoveryhub.models.event-prescription'));
    }
}