<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Dingo\Api\Http\Request;
use Exception;

trait CartEventControllerTrait
{
    public function addToCart(Request $request)
    {
        try {
           $cartService = app(\Sunnydevbox\Recoveryhub\Services\CartService::class);
           $result = $cartService->addToCart($request->all());
           
           return $this->response->item($result, $this->transformer);

        } catch(Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'  => $e->getCode(),
            ], $e->getCode());

        }
    }
}
