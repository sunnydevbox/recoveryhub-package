<?php
namespace Sunnydevbox\Recoveryhub\Traits;

trait MentalHealthDataTrait
{
    public function mentalHealthData()
    {
        return $this->hasOne(config('recoveryhub.models.mental-health-data'));
    }
}