<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Amsgames\LaravelShop\Traits\ShopCalculationsTrait as ShopCalculationsTraitOriginal;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use InvalidArgumentException;
use Carbon\Carbon;

trait ShopCalculationsTrait
{
    use ShopCalculationsTraitOriginal {
        ShopCalculationsTraitOriginal::getTotalTaxAttribute as parentrgetTotalTaxAttribute;
        ShopCalculationsTraitOriginal::runCalculations as parentRunCalculations;
        ShopCalculationsTraitOriginal::getTotalAttribute as parentGetTotalAttribute;
        ShopCalculationsTraitOriginal::getTotalPriceAttribute as parentGetTotalPriceAttribute;
    }


    public $appointmentItems = [];
    public $prescriptionItems = [];

    public function getShopCalculations()
    {
        return $this->shopCalculations;
    }

    public function runCalculations()
    {   
        if (!empty($this->shopCalculations)) return $this->shopCalculations;

        $cacheKey = $this->calculationsCacheKey;

        $this->appointmentItems = collect();
        $this->prescriptionItems = collect();
    
        // if (Config::get('shop.cache_calculations')
        //     && Cache::has($cacheKey)
        // ) {
        //     $this->shopCalculations = Cache::get($cacheKey);
        //     return $this->shopCalculations;
        // }
              // (price * quantity) * tax

        $this->shopCalculations = DB::table($this->table)
            ->select([
                DB::raw('sum(' . Config::get('shop.item_table') . '.quantity) as itemCount'),
                DB::raw('sum(' . Config::get('shop.item_table') . '.price * ' . Config::get('shop.item_table') . '.quantity) as totalPrice'),
                
                // (price * quantity) * tax
                DB::raw('sum((' . Config::get('shop.item_table') . '.price * ' . Config::get('shop.item_table') . '.quantity) * ' . Config::get('shop.item_table') . '.tax) as totalTax'),

                DB::raw('sum(' . Config::get('shop.item_table') . '.shipping * ' . Config::get('shop.item_table') . '.quantity) as totalShipping')
            ])
            ->join(
                Config::get('shop.item_table'),
                Config::get('shop.item_table') . '.' . ($this->table == Config::get('shop.order_table') ? 'order_id' : $this->table . '_id'),
                '=',
                $this->table . '.id'
            )
            ->where($this->table . '.id', $this->attributes['id'])
            ->first();

        /**** DISCOUNTS CALC  / COUPON */
        $discountTypes = $this->calculateDiscounts();
        
        $discountSeparated = $this->separateItems();
        // dd($discountSeparated);

        // ASSIGN TOTAL AMOUNTS
        $this->shopCalculations->appointmentTotal = $discountSeparated['appointment_amount'];
        $this->shopCalculations->prescriptionTotal = $discountSeparated['item_amount'];

        // SET TOTAL SHIPPING IF PRESCRIPTION IS NOT ZERO
        $this->shopCalculations->totalShipping = ($this->shopCalculations->prescriptionTotal > 0  ? $this->shopCalculations->totalShipping + 300 : $this->shopCalculations->totalShipping);

        $Discount = $this->getDiscount($discountTypes, $discountSeparated);
        
        // discounts
        $itemDiscounted = $discountSeparated['item_amount'] - $Discount['item'];
        $appointmentDiscounted = $discountSeparated['appointment_amount'] - $Discount['appointment'];
        

        $itemDiscounted = $itemDiscounted < 0 ? 0 : $itemDiscounted;
        $appointmentDiscounted = $appointmentDiscounted < 0 ? 0 : $appointmentDiscounted;
        

        $itemD = $discountSeparated['item_amount'] > 0 ? $Discount['item'] : 0;
        $appointmentD =  $discountSeparated['appointment_amount'] > 0 ? $Discount['appointment'] : 0;
        $totalD = $Discount['percentCashDiscount'];
        

        
        $this->shopCalculations->cashDiscount = $Discount['cashDiscount'];
        $this->shopCalculations->percentDiscount = $Discount['percentDiscount'];
        $this->shopCalculations->percentCashDiscount = $Discount['percentCashDiscount'];
        $this->shopCalculations->totalDiscount = $totalD;

        $this->shopCalculations->total = $this->shopCalculations->totalPrice + $this->shopCalculations->totalShipping + $this->shopCalculations->totalTax;





        foreach($this->items as $item) {
            $object = $item->object;
            if(!$object) continue;
            switch ($object->product_type) {
                case 'appointment':
                    $this->appointmentItems->push($item);
                break;

                case 'prescription':
                case 'medicine':
                    $this->prescriptionItems->push($item);
                break;
            }
        }










        // dd($this->shopCalculations);
        /**** END DISCOUNTS CALC */

        if (Config::get('shop.cache_calculations')) {
            Cache::put(
                $cacheKey,
                $this->shopCalculations,
                Config::get('shop.cache_calculations_minutes')
            );
        }

        return $this->shopCalculations;
    }

    public function getTotalTaxAttribute()
    {        
        if (empty($this->shopCalculations)) $this->runCalculations();
        $taxableAmount = 0;
        $taxAmount = 0.12;


        $oldRecord = false;

        // ONLY APPOINTEMNTS HAVE TAX.. for now.
        // !!! When the medicine invetory is converted into
        // !!! shop item, this should already be all.
        $taxableAmount = $this->appointmentItems->reduce(function($carry, $item) use ($taxAmount) {
            if($item->object->created_at->lt(Carbon::createFromFormat('Y-m-d', '2019-12-01'))) {
                $oldRecord = true;

                return $carry + (($item->price * $item->quantity) * $taxAmount);
            }
            return $carry + $item->taxAmount;
        });


        // $taxableAmount = $this->items->reduce(function($carry, $item) use (&$oldRecord) {
            
        //     if($item->created_at->lt(Carbon::createFromFormat('Y-m-d', '2019-12-01'))) {
        //         $oldRecord = true;
        //     }
        //     if ($item->object && get_class($item->object) == \Sunnydevbox\Recoveryhub\Models\Event::class) {   
        //         return $carry + stringToFloat($item->price);
        //     }
        // });

        return stringToFloat($taxableAmount);

        if ($oldRecord) {
            return 0;
        } else {
            return round($this->shopCalculations->totalTax + ($this->taxableAmount * Config::get('shop.tax')), 2);
        }
    }

    public function getDiscount($discount, $amount)
    {
        // item total amount - value - percentage convert to value
        // $discount['cash-both'] = $discount['cash-both'] / 2;
        // $discount['percent-both'] = $discount['percent-both'] / 2;
        $cashItem = $discount['cash-medicine'] + $discount['cash-both'];
        $cashAppointment = $discount['cash-appointment'] + $discount['cash-both'];

        $percentItem = $amount['item_amount'] * $discount['percent-both'] + $amount['item_amount'] * $discount['percent-medicine'];
        $percentAppointment = $amount['appointment_amount'] * $discount['percent-both'] + $amount['appointment_amount'] * $discount['percent-appointment'];

        $item = $cashItem + $percentItem;
        $appointment = $cashAppointment + $percentAppointment;
        // $both = $discount['cash-both']

        $data['cashDiscount'] = $discount['cash-medicine'] + $discount['cash-both'] + $discount['cash-appointment'] + $discount['cash-both'];
        $data['percentDiscount'] = $discount['percent-medicine'] + $discount['percent-both'] + $discount['percent-appointment'] + $discount['percent-both'];
        $data['percentCashDiscount'] = number_format( $item + $appointment , 2);
        $data['item'] = $item <= 0 ? 0 : $item;
        $data['appointment'] = $appointment <= 0 ? 0 : $appointment;
        $data['both'] = $discount['cash-both'] || $discount['percent-both'] ? true : false;

        // if ( $data['PercentCashDiscount'] < 0) {
        //     $data['PercentCashDiscount'] *= -1;
        // }
        return $data;
    }

    private function calculateDiscounts()
    {
        $Discount = [
            'cash-both'    => 0.00,
            'cash-medicine'    => 0.00,
            'cash-appointment'    => 0.00,
            'percent-both'    => 0.00,
            'percent-medicine'    => 0.00,
            'percent-appointment'    => 0.00
        ];

        $appliedCoupons = $this->coupons;
        foreach ($appliedCoupons as $appliedCoupon)
        {
            $coupon = $appliedCoupon->coupon;

            if (!is_null($coupon->value)) {
                $Discount['cash-'.$coupon->item_type] += $coupon->value;
            } else {
                $Discount['percent-'.$coupon->item_type] += $coupon->discount;
            }
        }

        return $Discount;
    }

    public function separateItems()
    {
        $data = [
            'item_amount' => 0,
            'appointment_amount' => 0
        ];

        if ( $this->items ) {

            foreach ( $this->items as $item) {
                
                if ($item->class == \Sunnydevbox\Recoveryhub\Models\MedicineInventory::class 
                || $item->class == \Sunnydevbox\Recoveryhub\Models\PrescriptionTransaction::class
                ) {
                    $data['item_amount'] += ($item->price * $item->quantity);
                } else {
                    if($item->object) {
                        $data['appointment_amount'] += $item->object->total; //$item->price;
                    }
                }

            }

        }
        return $data;
    }

    public function getTotalPriceAttribute()
    {
        // totalTax is removed because this was already added to each item...
        // so no need to add it here
        return stringToFloat($this->appointmentTotal + $this->prescriptionTotal);// + $this->totalTax;
    }

    public function getAppointmentTotalAttribute()
    {
        if (empty($this->shopCalculations)) $this->runCalculations();

        return stringToFloat($this->appointmentItems->sum('total'));
    }

    public function getPrescriptionTotalAttribute()
    {
        if (empty($this->shopCalculations)) $this->runCalculations();

        return stringToFloat($this->prescriptionItems->sum('total'));
    }


    public function getTotalAttribute()
    {

        if (empty($this->shopCalculations)) $this->runCalculations();

        $oldRecord = false;
        $this->items->each(function($item, $k) use (&$oldRecord) {
            if($item->created_at->lt(Carbon::createFromFormat('Y-m-d', '2019-12-01'))) {
                $oldRecord = true;
            }
        });
        
        if ($oldRecord) {
            return stringToFloat(
                ($this->totalPrice + $this->totalShipping) - $this->percentCashDiscount
            );
        }

        return stringToFloat(
            ($this->totalPrice + $this->totalShipping) - $this->percentCashDiscount
        );


        return stringToFloat($this->totalPrice - $this->percentCashDiscount + $this->totalTax + $this->totalShipping);
        if (empty($this->shopCalculations)) $this->runCalculations();
        return $this->totalPrice + $this->totalTax + $this->totalShipping;
    }

    public function getCashDiscountAttribute()
    {
        return stringToFloat($this->shopCalculations->cashDiscount);
    }

    public function getPercentDiscountAttribute()
    {
        return stringToFloat($this->shopCalculations->percentDiscount);
    }

    public function getPercentCashDiscountAttribute()
    {
        return stringToFloat($this->shopCalculations->percentCashDiscount);
    }

    public function getTotalDiscountAttribute()
    {
        if (empty($this->shopCalculations)) $this->runCalculations();
        return stringToFloat($this->shopCalculations->totalDiscount);
    }

    private function _getTotalByType($type = null)
    {
        dd($this->items);
        $items = [];
        foreach($cart->items as $item) {
            $object = $item->object;
            
            $item = collect($item->toArray())->except(['created_at', 'updated_at', 'class' ]);
            
            $item['tax_amount'] = $object->tax_amount;
            $item['object'] = collect($object->toArray())->only(['id', 'status', 'start_at', 'end_at', 'label']);
            $item['product_type'] = $object->product_type;
            $item['displayTax'] = $object->displayTax;
            $item['total'] = $object->total;

            // dd($cart->percentDiscount); 
            /*
                Calculate the DISCOUNT IF ANY
            */
            
            if (isset($object) && isset($object->medicine) ) {
                $item['object']['medicine'] = $object->medicine;
            }
            
            switch ($object->product_type) {
                case 'appointment':
                    $appointmentTotal += $object->total;
                break;

                case 'prescription':
                case 'medicine':
                    $prescriptionTotal += $object->total;
                break;
            }
            
            
            // dd($item);
            $items[] = $item;
        }
        
    }
}