<?php
namespace Sunnydevbox\Recoveryhub\Traits;



use Sunnydevbox\TWCart\Traits\CartItemTrait as TWCartCartItemTrait;
// use Illuminate\Support\Facades\Config;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Cache;
// use InvalidArgumentException;
use Carbon\Carbon;

trait CartItemTrait
{
    // use ShopCalculationsTraitOriginal {
    //     ShopCalculationsTraitOriginal::getTotalTaxAttribute as parentrgetTotalTaxAttribute;
    //     ShopCalculationsTraitOriginal::runCalculations as parentRunCalculations;
    //     ShopCalculationsTraitOriginal::getTotalAttribute as parentGetTotalAttribute;
    //     ShopCalculationsTraitOriginal::getTotalPriceAttribute as parentGetTotalPriceAttribute;
    // }

    public function getSubTotalAttribute()
    {
        return stringToFloat($this->price * $this->quantity);
    }
    

    public function getTotalAttribute()
    {
        $amount = $this->sub_total;
        return stringToFloat($amount + ($amount * $this->tax));
    }

    public function getTaxAmountAttribute()
    {
        return stringToFloat(($this->price * $this->quantity) * $this->tax);
    }
}