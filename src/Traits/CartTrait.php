<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Auth;
use Sunnydevbox\TWCart\Models\Cart;


trait CartTrait
{
    public function content2()
    {       
        $cart = $this->cart();
        // dd($cart);
        // dd($cartTemp->runCalculations());
        $cartTotals = $cart->runCalculations();


        $items = [];
        foreach($cart->items as $item) {
            $object = $item->object;

            if (!$object) {
                // the object probably was deleted (deleted_at)
                // so remove it from the cart.
                $cart->remove(['sku' => $item->sku]);
            } else {
                $item = collect($item->toArray())->except(['created_at', 'updated_at', 'class' ]);
                // var_dump($object);exit;
                $item['tax_amount'] = $object->tax_amount;
                $item['object'] = collect($object->toArray())->only(['id', 'status', 'start_at', 'end_at', 'label']);
                $item['product_type'] = $object->product_type;
                $item['displayTax'] = $object->displayTax;
                $item['total'] = $object->total;

                // dd($cart->percentDiscount); 
                /*
                    Calculate the DISCOUNT IF ANY
                */
                
                if (isset($object) && isset($object->medicine) ) {
                    $item['object']['medicine'] = $object->medicine;
                }
                
                // $hasMedicine
                
                // switch ($object->product_type) {
                //     case 'appointment':
                //         $appointmentTotal += $object->total;
                //     break;

                //     default : 
                //         $prescriptionTotal += $object->total;
                //         $hasMedicine = true;
                //     break;
                // }
                
                
                
                // dd($item);
                $items[] = $item;
            }
        }

        // ADD 300 FLAT RATE IF HAS MEDICINE
        // $cartTotals->totalShipping = 300;

        // dd($cartTotals);

        // compute shipping if prescription is not 0
        // $shipping 

        $data = [
            'items' => $items, 
            'meta' => [
                // 'totalPrice'            => $cartTotals->totalPrice + $cartTotals->totalTax,
                // 'totalTax'              => $cartTotals->totalTax,
                // 'count'                 => $cart->count,
                // 'totalShipping'         => $cartTotals->totalShipping,
                // 'total'                 => $cartTotals->total,
                // 'displayTotalPrice'     => $cart->displayTotalPrice,
                // 'displayTotalTax'       => $cart->displayTotalTax,
                // 'displayTotalShipping'  => $cart->displayTotalShipping,
                // 'displayTotal'          => $cart->displayTotal,
                // 'cashDiscount'          => $cartTotals->cashDiscount,
                // 'percentDiscount'       => $cartTotals->percentDiscount,
                // 'percentCashDiscount'   => $cartTotals->percentCashDiscount,
                // 'totalDiscount'         => $cartTotals->totalDiscount,
                // 'appointmentTotal'      => $cartTotals->appointmentTotal,
                // 'prescriptionTotal'     => $cartTotals->prescriptionTotal,


                'totalPrice'            => $cart->totalPrice,
                'totalTax'              => $cart->totalTax,
                'count'                 => $cart->count,
                'totalShipping'         => $cart->totalShipping,
                'total'                 => $cart->total,
                'displayTotalPrice'     => $cart->displayTotalPrice,
                'displayTotalTax'       => $cart->displayTotalTax,
                'displayTotalShipping'  => $cart->displayTotalShipping,
                'displayTotal'          => $cart->displayTotal,
                'cashDiscount'          => $cart->cashDiscount,
                'percentDiscount'       => $cart->percentDiscount,
                'percentCashDiscount'   => $cart->percentCashDiscount,
                'totalDiscount'         => $cart->totalDiscount,
                'appointmentTotal'      => $cart->appointmentTotal,
                'prescriptionTotal'     => $cart->prescriptionTotal,
            ],
            'coupons'  => $cart->coupons->map(function($coupon) {
                            $c = $coupon->coupon->toArray();
                            $c['applied_coupon_id'] = $coupon->id;
                            return collect($c)->only(['applied_coupon_id', 'id', 'code', 'discount']);
            }),
        ];
    
        return $data;
    }

    public function destroy($instance)
    {
        return $this->cart($instance)->clear();
    }

    public function removeItem($object, $quantity = null)
    {   
        $sku = '';     
        if (is_string($object)) {
            $object = [
                'sku' => $object,
            ];
            
            $sku = $object; 
        }
        
        if (is_array($object)) {

            if (isset($object['quantity'])) {
                $quantity = $object['quantity'];
                unset($object['quantity']);
            }
            // dd($object);
            $sku = $object['sku'];
        }

        // PREPARE DATA TO BE PASSED ON EVENT
        $data = [
            'sku' => $sku,
            'id'  => $object['id']
        ];
        // dd($object);
        $resPrescription = $this->prescriptionService->getPrescriptionId($data);
        // dd($resPrescription);
        if ($resPrescription) {
            $presId = $resPrescription->prescription_item_id;

            if ( isset($presId) ) {
                // echo 'presid'; dd($object);
                // $data['presId'] = $presId;
                $data['resPrescription'] = $resPrescription;

                $resPrescription->delete();
                
                $this->prescriptionService->updateItem($data);

                
            }
        }

        // dd(2);
        // REMOVE ITEM
        $result = $this->cart()->remove($object, $quantity);
        
        return $result;
    }
}