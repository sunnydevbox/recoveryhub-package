<?php
namespace Sunnydevbox\Recoveryhub\Traits;

use Amsgames\LaravelShop\Traits\ShopCalculationsTrait as ShopCalculationsTraitOriginal;

trait ShopCalculationsTraitCopy
{
    // use ShopCalculationsTraitOriginal;
    use ShopCalculationsTraitOriginal {
        ShopCalculationsTraitOriginal::runCalculations as parentrRunCalculations;
    }

    private function runCalculations()
    {
        $this->resetCalculations();

        $result = $this->parentrRunCalculations();
        // dd($result);
        // APPLY COUPONS IF THERE ARE ANY
        //Add total discount, edit totalPrice
        $PivotOn = $this->table == config('shop.order_table') ? config('tw-cart.models.order') : config('tw-cart.models.cart');
        $discountsType = $this->calculateDiscounts($PivotOn, $this->attributes['id']);
        
        $discountSeparated = $this->separateItems();

        $Discount = $this->getDiscount($discountsType, $discountSeparated);
        // dd($Discount);
        // discounts
        $itemDiscounted = $discountSeparated['item_amount'] - $Discount['item'];
        $appointmentDiscounted = $discountSeparated['appointment_amount'] - $Discount['appointment'];

        $itemDiscounted = $itemDiscounted < 0 ? 0 : $itemDiscounted;
        $appointmentDiscounted = $appointmentDiscounted < 0 ? 0 : $appointmentDiscounted;


        $itemD = $discountSeparated['item_amount'] > 0 ? $Discount['item'] : 0;
        $appointmentD =  $discountSeparated['appointment_amount'] > 0 ? $Discount['appointment'] : 0;
        $totalD = $Discount['PercentCashDiscount'];

        $this->shopCalculations->totalPrice = $appointmentDiscounted + $itemDiscounted;
        


        $this->shopCalculations->CashDiscount = $Discount['CashDiscount'];
        $this->shopCalculations->PercentDiscount = $Discount['PercentDiscount'];
        $this->shopCalculations->PercentCashDiscount = $Discount['PercentCashDiscount'];
        $this->shopCalculations->totalDiscount = $totalD;

        // dd($this->shopCalculations);

        $cacheKey = $this->calculationsCacheKey;
        if (config('shop.cache_calculations')) {
            \Cache::put(
                $cacheKey,
                $this->shopCalculations,
                config('shop.cache_calculations_minutes')
            );
        }

        return $result;
    }

    public function getSubTotalAttribute()
    {
        if (empty($this->shopCalculations)) {
            $this->runCalculations();
        }

        return $this->totalPrice + $this->totalTax + $this->totalShipping;
    }

    public function getTotalAttribute()
    {
        return $this->getSubTotalAttribute();
    }

    public function getTotalDiscountAttribute()
    {
        if (empty($this->shopCalculations)) $this->runCalculations();

        return round($this->shopCalculations->totalDiscount, 2);
    }

    public function getDisplayTotalDiscountAttribute()
    {
        return Shop::format($this->totalDiscount);
    }

    public function getCouponDiscountAttribute()
    {
        if (empty($this->shopCalculations)) {
            $this->runCalculations();
        }
    
        return $this->shopCalculations->couponDiscount;
    }


    public function getDisplayTotalAttribute()
    {
        return $this->total;
    }


    /**
     * Calculates the discount required on a class with a specified id
     * @param $PivotOn
     * @param $id
     * @return array
     */
    private function calculateDiscounts($PivotOn, $id)
    {

        $Class = new $PivotOn;
        $Coupons = $Class::find($id)->coupons;
        // dd($Coupons);
        $Discount = [
            'cash-both'    => 0.00,
            'cash-medicine'    => 0.00,
            'cash-appointment'    => 0.00,
            'percent-both'    => 0.00,
            'percent-medicine'    => 0.00,
            'percent-appointment'    => 0.00
        ];

        // echo "this here"; dd($Coupons);
        foreach ($Coupons as $coupon)
        {

            if (!is_null($coupon->value))
                $Discount['cash-'.$coupon->item_type] += $coupon->value;
            else
                $Discount['percent-'.$coupon->item_type] += $coupon->discount;

        }

        return $Discount;
    }
    
    public function getCalculationsAttribute()
    {
        if (empty($this->shopCalculations)) $this->runCalculations();
        return $this->shopCalculations;
    }

    public function separateItems()
    {
        $data = [
            'item_amount' => 0,
            'appointment_amount' => 0
        ];

        if ( $this->items ) {

            foreach ( $this->items as $item) {
                // $d = $item;
                $e = 'Sunnydevbox\Recoveryhub\Models\PrescriptionTransaction';
                // $e = new $e;
                // dd($item);
                if ($item->class == $e) {
                    $data['item_amount'] += ($item->price * $item->quantity);
                } else {
                    $data['appointment_amount'] += $item->price;
                }

            }

        }
        return $data;
    }

    public function getDiscount($discount, $amount)
    {
        // dd($discount, $amount);

        // item total amount - value - percentage convert to value
        // $discount['cash-both'] = $discount['cash-both'] / 2;
        // $discount['percent-both'] = $discount['percent-both'] / 2;
        $cashItem = $discount['cash-medicine'] + $discount['cash-both'];
        $cashAppointment = $discount['cash-appointment'] + $discount['cash-both'];

        $percentItem = $amount['item_amount'] * $discount['percent-both'] + $amount['item_amount'] * $discount['percent-medicine'];
        $percentAppointment = $amount['appointment_amount'] * $discount['percent-both'] + $amount['appointment_amount'] * $discount['percent-appointment'];

        $item = $cashItem + $percentItem;
        $appointment = $cashAppointment + $percentAppointment;
        // $both = $discount['cash-both']

        $data['CashDiscount'] = $discount['cash-medicine'] + $discount['cash-both'] + $discount['cash-appointment'] + $discount['cash-both'];
        $data['PercentDiscount'] = $discount['percent-medicine'] + $discount['percent-both'] + $discount['percent-appointment'] + $discount['percent-both'];
        $data['PercentCashDiscount'] = number_format( $item + $appointment , 2);
        $data['item'] = $item <= 0 ? 0 : $item;
        $data['appointment'] = $appointment <= 0 ? 0 : $appointment;
        $data['both'] = $discount['cash-both'] || $discount['percent-both'] ? true : false;

        // if ( $data['PercentCashDiscount'] < 0) {
        //     $data['PercentCashDiscount'] *= -1;
        // }
        // $data['']
        // dd($data);
        return $data;

    }

}