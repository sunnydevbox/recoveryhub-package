<?php
namespace Sunnydevbox\Recoveryhub\Traits;

trait MedicalPractitionerRateChargeModelTrait
{

    public function rateCharge()
    {
        return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\RateCharge::class, 'doctor_id');
    }

    public function getCurrentRateAttribute()
    {
        // dd($this->attributes);
    }

    // protected function getArrayableAppends()
    // {
    //     $this->appends = array_unique(array_merge($this->appends, ['fileUrl', 'thumbUrl']));

    //     return parent::getArrayableAppends();
    // }
}
