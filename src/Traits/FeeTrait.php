<?php
namespace Sunnydevbox\Recoveryhub\Traits;


trait FeeTrait 
{

    /** EXTRA **/

    public function getLessTaxAttribute()
    {
        return stringToFloat($this->sub_total - ($this->sub_total * $this->tax));
    }

    public function getProfessionalFeeAttribute()
    {
        // price * quantity
        return stringToFloat($this->sub_total);
    }

    public function getWithholdingTaxAttribute()
    {
        // Fixed value of 5%
        // Should this be in a centralized location
        return stringToFloat(0.05);
    }

    public function getWithholdingAttribute()
    {
        return stringToFloat(($this->professional_fee * 0.75) * $this->withholding_tax);
    }

    public function getAdminFeeAttribute()
    {
        // Admin fee is 25%
        // Should this be in a centralized location
        return stringToFloat($this->sub_total * 0.25);
    }

    public function getNetIncomeAttribute()
    {
        return stringToFloat($this->professional_fee - $this->withholding - $this->admin_fee);
    }


    // public function initializeFeeTrait()
    // {
    //     dd(1);
    //     $this->append([        'less_tax',
    //     'withholding',
    //     'professional_fee',
    //     'net_income',
    //     'admin_fee',]);
    // }
}