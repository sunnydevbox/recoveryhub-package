<?php
namespace Sunnydevbox\Recoveryhub;

use Sunnydevbox\TWCore\BaseServiceProvider;

class RecoveryhubServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        parent::boot();

        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/recoveryhub/'),
        ],'resources');

        $this->publishes([
            __DIR__.'/../assets' => public_path('vendor/recoveryhub'),
        ],'assets');

        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('recoveryhub.php')
        ], 'config');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    public function mergeConfig()
    {
        return [
           realpath(__DIR__ . '/../config/config.php') => 'recoveryhub'
        ];
    }

    public function loadRoutes()
    {
        return [
            realpath(__DIR__.'/../routes/api.php')
        ];
    }

    public function loadViews()
    {
        return [
            __DIR__.'/../resources/views' => 'recoveryhub',
        ];
    }
    
    public function registerProviders()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        if (class_exists('\Barryvdh\Snappy\ServiceProvider')
            && !$this->app->resolved('\Barryvdh\Snappy\ServiceProvider')
        ) {
            $this->app->register(\Barryvdh\Snappy\ServiceProvider::class);  
            
            $loader->alias('PDF', \Barryvdh\Snappy\Facades\SnappyPdf::class);
            $loader->alias('SnappyImage', \Barryvdh\Snappy\Facades\SnappyImage::class);
        }

        if (class_exists('\Sunnydevbox\TWUser\TWUserServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWUser\TWUserServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWUser\TWUserServiceProvider::class);    
        }

        // EVENTS
        if (class_exists('\Sunnydevbox\Recoveryhub\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\Recoveryhub\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\Recoveryhub\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWEvents\TWEventsServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWEvents\TWEventsServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWEvents\TWEventsServiceProvider::class);
        }

        if (class_exists('\Sunnydevbox\TWBookings\TWBookingsServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWBookings\TWBookingsServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWBookings\TWBookingsServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWCart\TWCartServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWCart\TWCartServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWCart\TWCartServiceProvider::class);    
        }

        if (class_exists('\JSefton\ContactForm\LaravelContactFormProvider')
            && !$this->app->resolved('\JSefton\ContactForm\LaravelContactFormProvider')
        ) {
            $this->app->register(\JSefton\ContactForm\LaravelContactFormProvider::class);    
        }

        if (class_exists('\Intervention\Image\ImageServiceProvider')
            && !$this->app->resolved('\Intervention\Image\ImageServiceProvider')
        ) {
            $this->app->register(\Intervention\Image\ImageServiceProvider::class);    
            $loader->alias('Image', \Intervention\Image\Facades\Image::class);

        }



        $this->app->register('App\Providers\BroadcastServiceProvider');
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\Recoveryhub\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\Recoveryhub\Console\Commands\PublishMigrationCommand::class,
                \Sunnydevbox\Recoveryhub\Console\Commands\MigrateCommand::class,
                \Sunnydevbox\Recoveryhub\Console\Commands\CheckDragonpayTxStatusCommand::class,
            ]);
        }
    }
}