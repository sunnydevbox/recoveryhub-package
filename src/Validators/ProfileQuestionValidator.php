<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ProfileQuestionValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'question'    => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'question'    => 'required',
        ]
   ];
}