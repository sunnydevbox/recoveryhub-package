<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class EventPrescriptionItemValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'event_prescription_id'     => 'required',
            'medicine_id'               => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'event_prescription_id'     => 'required',
            'medicine_id'               => 'required',
        ]
   ];
}