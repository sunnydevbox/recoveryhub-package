<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class OpenTokValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            
        ],
        ValidatorInterface::RULE_UPDATE => [
            
        ],
        'REGISTER_EVENT' => [
                'event_id' => 'required'
        ],
   ];
}