<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PrescriptionTransactionValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'prescription_item_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'prescription_item_id'     => 'required',
        ]
   ];
}