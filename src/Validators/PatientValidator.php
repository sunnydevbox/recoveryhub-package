<?php

namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class PatientValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'         => 'required|email|unique:users',
            'password'      => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         => 'email|unique:users',
            'password'      => 'min:8',
        ]
   ];

}