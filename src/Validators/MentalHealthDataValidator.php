<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class MentalHealthDataValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'user_id'  => 'required',
        ]
   ];
}