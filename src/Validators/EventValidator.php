<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use Sunnydevbox\TWEvents\Validators\EventValidator as ExtendedEventValidator;
use Sunnydevbox\Recoveryhub\Rules\EventDate;

class EventValidator extends ExtendedEventValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'start_at'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'label'     => 'required|min:3',
            'start_at'  => 'required',
        ]
   ];
}