<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class CouponValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'event_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'event_id'  => 'required',
        ]
   ];
}