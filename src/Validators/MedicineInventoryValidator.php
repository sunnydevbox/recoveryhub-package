<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class MedicineInventoryValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'              => 'required',
            'generic_name_id'   => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name'              => 'required',
            'generic_name_id'   => 'required',
        ]
   ];
}