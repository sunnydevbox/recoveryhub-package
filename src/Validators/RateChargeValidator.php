<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class RateChargeValidator extends LaravelValidator
{
    protected $attributes = [
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
        'sunday' => 'Sunday',
    ];
    
    protected $messages = [
        'required' => 'The :attribute field is required',
        'numeric' => ':attribute: Invalid amount',
        'min' => ':attribute minimum fee: Php:min.00',
        'default.min' => 'Default minimum fee is Php:min.00'
    ];

	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'doctor_id'     => 'numeric',
            'use_default'   => 'boolean',
            'default'       => 'numeric|min:1000',
            'monday'        => 'numeric|min:1000',
            'tuesday'       => 'numeric|min:1000',
            'wednesday'     => 'numeric|min:1000',
            'thursday'      => 'numeric|min:1000',
            'friday'        => 'numeric|min:1000',
            'saturday'      => 'numeric|min:1000',
            'sunday'        => 'numeric|min:1000',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'doctor_id'     => 'numeric',
            'use_default'   => 'boolean',
            'default'       => 'numeric|min:1000',
            'monday'        => 'numeric|min:1000',
            'tuesday'       => 'numeric|min:1000',
            'wednesday'     => 'numeric|min:1000',
            'thursday'      => 'numeric|min:1000',
            'friday'        => 'numeric|min:1000',
            'saturday'      => 'numeric|min:1000',
            'sunday'        => 'numeric|min:1000',
        ]
   ];
}