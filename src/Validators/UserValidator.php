<?php

namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserValidator extends \Sunnydevbox\TWUser\Validators\UserValidator {

    protected $attributes = [
        'doctorRole' => 'Role'
    ];

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'         => 'required|email|unique:users',
            // 'password'      => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         => 'email',
            // 'password'      => 'min:8',
        ],

        'CREATE_WITH_DOCTOR_ROLE' => [
            'email'         => 'required|email|unique:users',
            'doctorRole'    => 'required|in:medical-practitioner,psychologist',
        ],
   ];

}