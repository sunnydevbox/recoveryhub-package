<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class CartPaymentValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [],
        'BILLING_INFO' => [
            'firstName' => 'required',
            'middleName' => 'required',
            'lastName' => 'required',
            'address' => 'required',
            'address2' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'zip' => 'required',
            'telno' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'event_id'  => 'required',
        ]
   ];


   protected $attributes = [
        'firstName' => 'First Name',
        'middleName' => 'Middle Name',
        'lastName' => 'Last Name',
        'address' => 'Address',
        'address2' => 'Address 2',
        'city' => 'City',
        'state' => 'State/Province',
        'country' => 'Country',
        'zip' => 'Zip Code',
        'telno' => 'Contact Number',
    ];

}