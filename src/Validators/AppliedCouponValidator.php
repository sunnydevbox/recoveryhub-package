<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class AppliedCouponValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'coupon_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'coupon_id'  => 'required',
        ]
   ];
}