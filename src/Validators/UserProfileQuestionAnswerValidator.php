<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UserProfileQuestionAnswerValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            // 'user_id'    => 'required',
            'question_id'    => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'question_id'    => 'required',
        ]
   ];
}