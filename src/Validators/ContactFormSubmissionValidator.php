<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class ContactFormSubmissionValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [            
            'subject' => 'required',
            'form_data' => 'required',
            'contents' => 'required',
            'from' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'subject' => 'required',
            'form_data' => 'required',
            'contents' => 'required',
            'from' => 'required',
        ]
   ];
}