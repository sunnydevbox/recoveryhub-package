<?php
namespace Sunnydevbox\Recoveryhub\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class TokboxCallbackValidator extends LaravelValidator
{
	protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'session_id'    => 'required',
            'project_id'    => 'required',
            'event'         => 'required',
            'timestamp'     => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'session_id'    => 'required',
            'project_id'    => 'required',
            'event'         => 'required',
            'timestamp'     => 'required',
        ]
   ];
}