<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class MedicineInventoryTransformer extends TransformerAbstract
{
    public function transform($obj)
    {   
		return $obj->toArray();
    }
}