<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class EventFeedbackTransformer extends TransformerAbstract
{
    public function transform($obj)
    {   
        return $obj->toArray();
    }
}