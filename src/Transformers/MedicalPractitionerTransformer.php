<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

// use Dingo\Api\Http\Request;
// use Dingo\Api\Transformer\Binding;
// use Dingo\Api\Contract\Transformer\Adapter;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\TWUser\Transformers\UserTransformer as ExtendUsertransformer;

class MedicalPractitionerTransformer extends ExtendUsertransformer
{
    public function __construct()
    {
        $this->setDefaultIncludes(['roles', 'permissions']);
    }
    
    public function transform($obj)
    {
        if (request()->get('filter')) {
            return $obj->toArray();
            
        } else {
            $data = [
                'id'            => (int) $obj->id,
                'email'         => $obj->email,
                'date_joined'   => $obj->created_at->toDateTimeString(),
                'events'        => [],
                'status'        => $obj->status,
                'is_verified'   => ($obj->is_verified) ? strtotime($obj->is_verified) : false,
                'picture'       => $obj->attachment('picture') ? $obj->attachment('picture')->url : null,
                'first_name'    => $obj->first_name,
                'last_name'     => $obj->last_name,
                'name'          => $obj->first_name . ' ' . $obj->last_name,
                'general_info'  => $obj->general_information
            ];
            
            // if (method_exists(config('recoveryhub.models.user'),'getAllMeta')) {
                
            //     $metas = collect($obj->getAllMeta());
                
            //     foreach ($obj->metaFields() as $column_name) {
            //         $data[$column_name] = $metas->get($column_name);
            //     }
            // }
        
            if ($events = $obj->events) {
                foreach($events as $index => $event) {
                    $data['events'][] = [
                        'id'        => $event->id,
                        'start_at'  => $event->start_at,
                        'end_at'    => $event->end_at,
                        'label'     => $event->label,
                        'price'     => $event->price,
                        'status'     => $event->status,
                    ];
                }
            }
        }

        


        return $data;
    }
}