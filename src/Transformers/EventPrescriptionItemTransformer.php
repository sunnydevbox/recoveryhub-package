<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

// use Dingo\Api\Http\Request;
// use Dingo\Api\Transformer\Binding;
// use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;

class EventPrescriptionItemTransformer extends TransformerAbstract
{
    public function transform($obj)
    {   
		return $obj->toArray();
    }
}