<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class TokboxCallbackTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $data = $obj->toArray();

        return $data;
    }
}