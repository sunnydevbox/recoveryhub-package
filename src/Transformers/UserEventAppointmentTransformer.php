<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use Sunnydevbox\TWEvents\Transformers\EventTransformer;
use Sunnydevbox\TWBookings\Transformers\BookingTransformer;

class UserEventAppointmentTransformer extends BookingTransformer
{
    public function transform($obj)
    {
        $data = [
            'id'            => $obj->bookings->id,
            'status'        => (boolean) $obj->bookings->status,
            
            'event' => [
                'id'        => $obj->id,
                'start_at'  => $obj->start_at,
                'end_at'    => $obj->end_at,
                'label'     => $obj->label,
                'price'     => $obj->price,
            ],
        ];

        //dd($obj->bookings);
        if ($obj->bookings->count()) {
            
            $data['patient'] = [
                'id'            => $obj->bookings->patient->id,
                'first_name'    => $obj->bookings->patient->first_name,
                'last_name'     => $obj->bookings->patient->last_name,
            ];
        }


        return $data;
    }
}