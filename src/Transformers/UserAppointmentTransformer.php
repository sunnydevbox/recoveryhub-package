<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use Sunnydevbox\TWEvents\Transformers\EventTransformer;
use Sunnydevbox\TWBookings\Transformers\BookingTransformer;

class UserAppointmentTransformer extends BookingTransformer
{  
    public function __construct()
    {
        // $this->setDefaultIncludes([
        //     'assignable',
        //     'bookable',
        // ]);
    }

    public function transform($obj)
    {
        $data = [
            'id'            => (int) $obj->id,
            // 'status'        => (boolean) $obj->status,
        ];

        if ($obj->bookable) {
            $data['event']     = [
                'id'    => $obj->bookable->id,
                'label'    => $obj->bookable->label,
                'status'    => $obj->bookable->status,
                'start_at'  => $obj->bookable->start_at,
                'end_at'    => $obj->bookable->end_at,
                'price'     => $obj->bookable->price,
                
            ];

            $doctor = $obj->bookable->assignable()->withTrashed()->first();
            if ($doctor) {
                $data['event']['doctor'] = [
                    'id'            => $doctor->id,
                    'email'         => $doctor->email,
                    'first_name'    => $doctor->getMeta('first_name'),
                    'last_name'     => $doctor->getMeta('last_name'),
                    'picture'       => $doctor->attachment('picture') ? $doctor->attachment('picture')->url : null,
                ];
            }
        } 

        return $data;
    }
}