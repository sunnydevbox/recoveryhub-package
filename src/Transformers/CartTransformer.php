<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class CartTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        // echo "this"; dd(0);
        return $obj->toArray();
    }
}