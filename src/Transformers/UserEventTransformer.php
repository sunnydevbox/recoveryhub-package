<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class UserEventTransformer extends EventTransformer
{
    public function transform($obj)
    {
        // dd($obj);
        $data = $obj->toArray();
        $data['displayTax'] = $obj->total;
        
        // dd($data);
        /** DEPRECATED BLOCK */
        // $data['patient'] = null;
        
        if (isset($data['bookings']) && count($data['bookings'])) {
            $booking = isset($obj->bookings[0]) ? $obj->bookings[0] : $obj->bookings;
            $data['patient'] = [
                'id'            => $booking->patient->id,
                'first_name'    => $booking->patient->first_name,
                'lat_name'      => $booking->patient->last_name,
                'last_name'      => $booking->patient->last_name,
                'picture'       => $booking->patient->picture
            ];
        }

        return $data;


        $data = parent::transform($obj);

        // 'start_at'		=> $obj->start_at,
        //     'end_at'		=> $obj->end_at,

        // SET TIMEZONE
        $data['start_at']   = $data['start_at']->setTimeZone($obj->doctor->timezone);
        $data['end_at']     = $data['end_at']->setTimeZone($obj->doctor->timezone);

        $owner = $obj->bookings->first();
        $data['patient'] = null;
        
        if ($owner) {
            $data['patient'] = [
                'id'            => $owner->assignable->id,
                'first_name'    => $owner->assignable->getMeta('first_name'),
                'lat_name'      => $owner->assignable->getMeta('last_name'),
                'last_name'      => $owner->assignable->getMeta('last_name'),
            ];
        }

        return $data;
    }
}