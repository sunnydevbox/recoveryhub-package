<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;
use Sunnydevbox\TWEvents\Transformers\EventTransformer;

class CouponTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return $obj->toArray();
    }
}