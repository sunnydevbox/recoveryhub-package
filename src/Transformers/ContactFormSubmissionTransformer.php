<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class ContactFormSubmissionTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        return $obj->toArray();
    }
}