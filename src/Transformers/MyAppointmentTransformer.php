<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

// use Dingo\Api\Http\Request;
// use Dingo\Api\Transformer\Binding;
// use Dingo\Api\Contract\Transformer\Adapter;

use Sunnydevbox\TWUser\Transformers\RoleTransformer;
use Sunnydevbox\TWUser\Transformers\UserTransformer as ExtendUsertransformer;

class MyAppointmentTransformer extends ExtendUsertransformer
{
    public function transform($obj)
    {
        
        $data = [
            'id'                => $obj->id,
            'appointment_id'    => $obj->id,
        ];

        if ($obj->relationLoaded('bookable')) {
            $data['event_id']          = $obj->bookable->id;
            $data['start_at']          = $obj->bookable->start_at;
            $data['end_at']            = $obj->bookable->end_at;

            $data['doctor'] = [
                'id'            => $obj->bookable->doctor->id,
                'first_name'    => $obj->bookable->doctor->first_name,
                'last_name'     => $obj->bookable->doctor->last_name,
            ];
        }

        if ($filters = request()->get('filters')) {

            $filters = explode(';', $filters);

            foreach($filters as $filter) {
                switch(trim($filter)) {
                    case 'patient': 
                        $data['patient'] = $obj->patient;
                    break;

                    case 'doctor': 
                        $data['doctor'] = $obj->bookable->doctor;
                    break;
                }
            }


        }

        return $data;
    }
}