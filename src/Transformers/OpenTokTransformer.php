<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class OpenTokTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $data = $obj->toArray();

        return $data;
    }
}