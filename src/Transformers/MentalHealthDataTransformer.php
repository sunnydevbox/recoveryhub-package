<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

use League\Fractal\TransformerAbstract;

class MentalHealthDataTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        $data = [
            "id" => (int)$obj->id,
            'user_id' => $obj->user_id,
            'patient_data' => $obj->patient_data,
        ];

        $data['patient_data']['generalInformation'] = isset($obj->generalInformation) ? $obj->generalInformation : array();

        $obj->dataCounter($data['patient_data']);

        $data['isMentalDataComplete'] = $obj->is_mental_data_complete;
        $data['percentage'] = $obj->percentage;
        return $data;
    }
}