<?php

namespace Sunnydevbox\Recoveryhub\Transformers;

// use Dingo\Api\Http\Request;
// use Dingo\Api\Transformer\Binding;
// use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;

class EventNoteTransformer extends TransformerAbstract
{
    public function transform($obj)
    {   
        if (app('request')->get('filter')) {
			return $obj->toArray();
        }
        
        $data = [
            'id'            => $obj->id,
            'event_id'      => $obj->event_id,
            'updated_at'    => $obj->updated_at,
            'notes'         => $obj->notes,
        ];
        
        return $data;
    }
}