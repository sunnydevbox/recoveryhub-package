<?php
namespace Sunnydevbox\Recoveryhub\Transformers;

use Dingo\Api\Http\Request;
use League\Fractal\TransformerAbstract;

class CartOrderTransformer extends TransformerAbstract
{
    public function transform($obj)
    {
        // dd($obj->items[0]);

        // echo "this"; dd($obj->total);

        // $cart = [
        //     // 'cashDiscount' => $obj->cashDiscount,
        //     // 'percentDiscount' => $obj->percentDiscount,
        //     // 'percentCashDiscount' => $obj->percentCashDiscount,
        //     'totalDiscount' => $obj->totalDiscount,
        //     'itemCount' => $shopCalculations->itemCount,
        //     'totalPrice' => $shopCalculations->totalPrice,
        //     'totalTax' => $obj->totalTax,
        //     'totalShipping' => $obj->totalShipping,
        //     'total' => $obj->total,
        // ];

        

        $appointmentTotal = 0;
        $prescriptionTotal = 0;

        foreach($obj->items as $k => $item) {
            // dd($item->price, $item->tax, $item->id, $item->total);
            //dd($item->object);
        
            $item->object = $item->object;
            
            if ($item->object) {
                
                // if ($item->object->id == 98) {
                    
                    $item->total = $item->object->total;
                    // dd($item->total);
                //}

                
                switch ($item->object->product_type) {
                    case 'appointment':
                        // dd($item->object->total);
                        $appointmentTotal += $item->object->total;
                    break;

                    case 'prescription':
                    case 'medicine':
                        $prescriptionTotal += $item->object->total;
                    break;
                }


                $obj->items[$k] = $item;
                // dd($obj->items[$k]->total);
            }
        }

        $data = $obj->toArray();
        
        $shopCalculations = $obj->getShopCalculations();
        $data['meta'] = [
            'totalPrice'            => $obj->totalPrice,
            'totalTax'              => $obj->totalTax,
            'count'                 => $obj->itemCount,
            'totalShipping'         => $obj->totalShipping,
            'total'                 => $obj->total,
            'displayTotalPrice'     => $obj->displayTotalPrice,
            'displayTotalTax'       => $obj->displayTotalTax,
            'displayTotalShipping'  => $obj->displayTotalShipping,
            'displayTotal'          => $obj->displayTotal,
            'cashDiscount'          => $obj->cashDiscount,
            'percentDiscount'       => $obj->percentDiscount,
            'percentCashDiscount'   => $obj->percentCashDiscount,
            'totalDiscount'         => $obj->totalDiscount,
            'appointmentTotal'      => $obj->appointmentTotal,
            'prescriptionTotal'     => $obj->prescriptionTotal,
        ];

        $data['coupons'] = $obj->coupons->map(function($coupon) {
                        $c = $coupon->coupon->toArray();
                        $c['applied_coupon_id'] = $coupon->id;
                        return collect($c)->only(['applied_coupon_id', 'id', 'code', 'discount']);
        });

        return $data;
    }
}