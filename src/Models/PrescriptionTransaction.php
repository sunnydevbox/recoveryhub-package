<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Amsgames\LaravelShop\Traits\ShopItemTrait;
use Sunnydevbox\Recoveryhub\Models\PrescriptionTransactionTraits;

class PrescriptionTransaction extends BaseModel
{
    use ShopItemTrait;
    use PrescriptionTransactionTraits;
    
    protected $fillable =  [
        'cart_id',
        'prescription_item_id',
        'quantity_ordered',
        'price',
        'sku',
    ];

    public function scopePendingStatus($query)
    {
        $query->where([
            ['status', '=', 'pending']
        ]);
    }

    // public function prescription()
    // {
    //     return $this->belongsTo(config('recoveryhub.models.event-prescription-item'));
    // }

    public function item()
	{
        return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\EventPrescriptionItem::class, 'prescription_item_id', 'id');
    }
    
    
}