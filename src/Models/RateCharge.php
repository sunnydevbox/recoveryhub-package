<?php

namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class RateCharge extends BaseModel
{
	protected $fillable = [
		'doctor_id',
		'default',
		'use_default',
		'monday',
		'tuesday',
		'wednesday',
		'thursday',
		'friday',
		'saturday',
		'sunday',
	];

	protected $hidden = [
		'created_at',
		'updated_at',
	];

	public function doctor()
	{
		return $this->belongsTo(config('recoveryhub.models.medical-practitioner'));
	}

	public function scopeCurrent($query, $dayOfWeek)
	{
        if ($dayOfWeek) {
            $days = [
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
                7 => 'sunday',
            ];

            $query->select([$days[$dayOfWeek]. ' as rate','default', 'use_default', $days[$dayOfWeek]]);
        }
	}

	public function scopeSetDoctor($query, $doctorId = null)
	{
		$query->where('doctor_id', $doctorId);
	}
}