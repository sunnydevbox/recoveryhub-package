<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Amsgames\LaravelShop\Models\ShopOrderModel;
use Sunnydevbox\Recoveryhub\Traits\ShopCalculationsTrait;
use Sunnydevbox\Recoveryhub\Traits\HasCouponTrait;

class Order extends \Sunnydevbox\TWCart\Models\Order
{
    use ShopCalculationsTrait;
    use HasCouponTrait;


    public function scopeIsPending($query)
    {
        $query->where('statusCode', 'pending');
    }
}