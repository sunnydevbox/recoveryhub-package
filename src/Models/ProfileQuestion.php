<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class ProfileQuestion extends BaseModel
{
    protected $table = 'profile_questions';

    public $timestamps = false;

    protected $fillable = [
        'question',
        'answer_type',
        'parent_id',
        'weight',
        'status',
        'settings'
    ];

    protected $hidden = [
        'parent_id',
        'weight',
    ];

    public function getSettingsAttribute()
    {
        return unserialize($this->attributes['settings']);
    }
    
    public function answers()
    {
        return $this->hasMany(config('recoveryhub.models.profile-question-answer'), 'question_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeInactive($query)
    {
        return $query->where('status', 0);
    }
}