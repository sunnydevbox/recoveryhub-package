<?php
namespace Sunnydevbox\Recoveryhub\Models;

use JSefton\ContactForm\ContactForm;

class ContactFormSubmission extends ContactForm
{
	protected $table = 'contact_form_submissions';
    
    protected $fillable = [
        'from',
        'to',
        'subject',
        'contents',
		'form_data',
		'form_url',
    ];

    protected $hidden = [
		'updated_at',
    ];

    public function setFormDataAttribute($value)
    {
    	$this->attributes['form_data'] = serialize($value);
    }

    public function getFormDataAttribute()
    {
    	return unserialize($this->attributes['form_data']);
    }

    public static function boot()
    {
        parent::boot();
        ContactFormSubmission::observe(new \Sunnydevbox\Recoveryhub\Observers\ContactFormSubmissionObserver);
    }
}