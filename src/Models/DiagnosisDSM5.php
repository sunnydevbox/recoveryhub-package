<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Kalnoy\Nestedset\NodeTrait;
use Sunnydevbox\TWCore\Models\BaseModel;

class DiagnosisDSM5 extends BaseModel
{
    use NodeTrait;
    
    protected $table = 'diagnosis_dsm5';
    public $timestamps = false;

    protected $fillable = [
        'label',
        'status',
        'parent_id',
    ];

}