<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWEvents\Models\Event as ExtendEvent;
use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use Sunnydevbox\TWBookings\Traits\BookingTrait;
use Amsgames\LaravelShop\Traits\ShopItemTrait;
use Sunnydevbox\Recoveryhub\Traits\EventModelShopItemTrait;
use Sunnydevbox\Recoveryhub\Traits\OpentokTrait;
use Carbon\Carbon;
use Sunnydevbox\Recoveryhub\Traits\HasEventPrescriptionTrait;

class Event extends ExtendEvent
{
	use BookingTrait;
    use TWMetaTrait;
    use ShopItemTrait;
    use EventModelShopItemTrait;
    use HasEventPrescriptionTrait;
    use OpentokTrait;

    protected $table = 'events';

    const STATUS_RESERVED = 'RESERVED';
    const STATUS_OPEN = 'OPEN';
    const STATUS_BOOKED = 'BOOKED';

    protected $meta = [
        'session_id',
    ];

	protected $dates = ['start_at', 'end_at'];

    protected $hidden = [
        // 'created_at',
        // 'updated_at',
        // 'deleted_at',
    ];

    protected $appends = [
        'session_id',
        'product_type',
        'has_started',
    ];
    
	public function user()
	{
        return $this->belongsTo(config('recoveryhub.models.user'), 'assignable_id');
    }

    public function notes()
    {
        return $this->hasOne(config('recoveryhub.models.event-note'));
    }
    
    public function doctor()
	{
		return $this->user()->withTrashed();	
    }
    
    public function opentok()
    {
        return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\OpenTok::class);
    }

    public function logs()
    {
        return $this->hasMany(\Sunnydevbox\Recoveryhub\Models\OpentokLog::class, 'event_id')->orderBy('event_at', 'desc');
    }

	public function bookings()
    {
        return $this->morphOne(config('recoveryhub.models.appointment'), 'bookable');
    }

    public function cartItemA()
    {
        return $this->hasMany(\Sunnydevbox\Recoveryhub\Models\Item::class, 'reference_id')
                    ->where('class', config('recoveryhub.models.event'));
    }

    public function cartItem()
    {
        return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\Item::class, 'reference_id')
                    ->where('class', config('recoveryhub.models.event'));
    }

    public function setStartAtAttribute($value)
    {
    	$this->attributes['start_at'] = $value->setTimeZone(config('app.timezone'));
    }

    public function setEndAtAttribute($value)
    {
    	$this->attributes['end_at'] = $value->setTimeZone(config('app.timezone'));
    }


    public function getStartAtAttribute()
    {
    	return \Carbon\Carbon::parse($this->attributes['start_at'], config('app.timezone'))->setTimeZone('Asia/Manila');
    }

    public function getEndAtAttribute()
    {
    	return \Carbon\Carbon::parse($this->attributes['end_at'], config('app.timezone'))->setTimeZone('Asia/Manila');
    }

    public function getSessionIdAttribute()

    {
        return $this->getMeta('session_id', null);
    }


    public function getDisplayTaxAttribute()
    {
        if (!isset($this->attributes['tax'])) {
            $tax = 0.12;
        } else {
            $tax = stringToFloat($this->attributes['tax']);

            if (!$tax) {
                $tax = 0.12;
            }
        }
        return ($tax * 100) .'%';
        // return (array_key_exists('tax', $this->attributes) ? ($this->attributes['tax'] * 100).'%' : '0%');
    }

    public function getProductTypeAttribute()
    {
        return 'appointment';
    }

    public function isBooked()
    {
        return ($this->status == self::STATUS_BOOKED) ? true : false;
    }

    public function isReserved()
    {
        return ($this->status == self::STATUS_RESERVED) ? true : false;
    }

    public function isOpen()
    {
        return ($this->status == self::STATUS_OPEN) ? true : false;
    }

    public function scopeReserved($query)
    {
        $query->where('status', self::STATUS_RESERVED);
    }

    public function isExpired(Carbon $time = null)
    {
        $time = !$time ? Carbon::now() : $time;
        return $time->gt($this->start_at); 
    }

    public static function boot()
    {
        parent::boot();

        Event::observe(new \Sunnydevbox\Recoveryhub\Observers\EventObserver);
    }

    public function getHasStartedAttribute()
    {
        $result = $this->logs()->where('name', 'READY')->first();
        return ($result) ? 1 : 0;
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable(array_merge( [
            'status',
            'price',
            'tax',
        ], parent::getFillable()));
    }
}