<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

trait EventPrescriptionTraits
{
    public function deliverToLifePort()
    {
        $this->flag = 'deliver-to-lifeport';    
        $this->save();
    }

    public function sendToEmail()
    {
        $this->flag = 'send-to-email';    
        $this->save();
    }
}