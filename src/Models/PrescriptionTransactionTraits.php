<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

trait PrescriptionTransactionTraits
{

    public function complete()
    {
        $this->status = 'complete';    
        $this->save();
    }

    public function processing()
    {
        $this->status = 'processing';    
        $this->save();
    }

    public function prepared()
    {
        $this->status = 'prepared';    
        $this->save();
    }

    public function pending()
    {
        $this->status = 'pending';    
        $this->save();
    }

    public function addTransactionNumber($transaction_number)
    {
        $this->transaction_number = $transaction_number;    
        $this->save();
    }
}