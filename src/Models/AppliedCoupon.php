<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\TWCart\Models\AppliedCoupon as OrigAppliedCoupon;

class AppliedCoupon extends OrigAppliedCoupon
{
    public function coupon()
    {
        return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\Coupon::class, 'coupon_id');
    }

    public function linkable()
    {
        return $this->morphTo();
    }
}
