<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\Recoveryhub\Traits\DataCounterTrait;

class MentalHealthData extends BaseModel
{
    use DataCounterTrait;

    protected $table = 'mental_health_data';
    
    protected $fillable =  [
        'user_id',
        'patient_data',
    ];

    public function user()
    {
        return $this->belongsTo(config('recoveryhub.models.user'));
    }

    public function getPatientDataAttribute()
    {
        return unserialize($this->attributes['patient_data']);
    }

    public function setPatientDataAttribute($value)
    {
        if (is_array($value)) {
            $this->attributes['patient_data'] = serialize($value);
        }
    }

}