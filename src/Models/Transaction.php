<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCart\Models\Transaction as TTransaction;

class Transaction extends TTransaction
{
    // protected $fillable = [
    //     'notes',
    //     'status',
    //     'received_by',
    //     'date_received',
    // ];
    
    public function order()
	{
        return $this->belongsTo(\Sunnydevbox\TWCart\Models\Order::class, 'order_id', 'id');
    }

    public function getDetailAttribute()
    {
        $detail = $this->attributes['detail'];
        $d = [];
        
        if (!@unserialize($detail)) {
            $d[] = $detail;
            return $d;
        } else {
            $d = unserialize($detail);
            return $d;
        }
    }

    public function setDetailAttribute($value)
    {
        if (gettype($value) == 'string') {
            $detail = [
                $value
            ];
        } else if (gettype($value) == 'array') {
            $detail = $value;
        }
        
        $this->attributes['detail'] = serialize($detail);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable(array_merge( [
                'notes',
                'status',
                'received_by',
                'date_received',
            ], parent::getFillable()));
    }
    
}