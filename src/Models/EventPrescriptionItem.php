<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Amsgames\LaravelShop\Traits\ShopItemTrait;
use Sunnydevbox\Recoveryhub\Traits\PrescriptionItemModelShopItemTrait;

class EventPrescriptionItem extends BaseModel
{
    use ShopItemTrait;
    use PrescriptionItemModelShopItemTrait;

    protected $table = 'event_prescription_items';
    
    protected $fillable = [
        'event_prescription_id',
        'medicine_id',
        'qty_tabs_dispensed',
        'dosage',
        'instructions',
        'total_qty_purchased',
        'total_qty_purchased_complete',
        'total_qty_purchased_pending',
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    protected $appends = [
        'available',
        'has_available',
        'product_type',
    ];

    public function prescription()
    {
        return $this->belongsTo(config('recoveryhub.models.event-prescription'));
    }

    public function medicine()
    {
        return $this->belongsTo(config('recoveryhub.models.medicine-inventory'));
    }

    public function s2()
    {
        return $this->medicine()->s2();
    }

    public function transactions()
	{
        return $this->hasMany(\Sunnydevbox\Recoveryhub\Models\PrescriptionTransaction::class, 'prescription_item_id', 'id');
    }

    public function getAvailableAttribute()
    {
        $available = abs($this->attributes['qty_tabs_dispensed'] - ($this->attributes['total_qty_purchased_pending'] + $this->attributes['total_qty_purchased']));
        return $available;
    }

    public function getHasAvailableAttribute()
    {
        return ($this->getAvailableAttribute() <= 0) ? false : true;
    }

    public function getProductTypeAttribute()
    {
        return 'medicine';
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable(array_merge( [
            'status',
            'price',
            'tax',
        ], parent::getFillable()));
    }
}