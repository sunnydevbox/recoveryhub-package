<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\Recoveryhub\Models\User;
use Sunnydevbox\TWEvents\Traits\EventTrait;
use Illuminate\Database\Eloquent\Builder;
use Sunnydevbox\TWUser\Models\Role;

use Sunnydevbox\Recoveryhub\Traits\MedicalPractitionerRateChargeModelTrait;

class MedicalPractitioner extends User
{
	use EventTrait;
	use MedicalPractitionerRateChargeModelTrait;

	protected $appends = [
		'first_name',
	];

	public function getTable()
	{
		return (new User)->getTable();
	}
}