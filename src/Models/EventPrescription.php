<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\Recoveryhub\Models\EventPrescriptionTraits;

class EventPrescription extends BaseModel
{

    use EventPrescriptionTraits; 

    protected $table = 'event_prescriptions';

    protected $fillable = [
        'event_id',
        'notes',
        'is_print_s2',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    public function items()
    {
        return $this->hasMany(config('recoveryhub.models.event-prescription-item'));
    }

    public function event()
    {
        return $this->belongsTo(config('recoveryhub.models.event'));
    }

    public function setNotesAttribute($value)
    {
        if (is_string($value)) {
            $isJson = json_decode($value);
            if (is_object($isJson)) {
                $json = serialize((array)$isJson);
                $this->attributes['notes'] = $json;
            }else{
                $this->attributes['notes'] = $value;
            }

        } else if (is_array($value)) {
            $this->attributes['notes'] = serialize($value);
        }
    }

    public function getNotesAttribute()
    {
        if (is_string($this->attributes['notes'])) {
            if (@unserialize($this->attributes['notes'])) {
                return unserialize($this->attributes['notes']);
            }else {
                return $this->attributes['notes'];
            }
        } 

        return null;
    }
}