<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class EventLog extends BaseModel
{
	public function event()
	{
        return $this->belongsTo(config('recoveryhub.models.event'));
    }

    public function getTable()
    {
        return 'event_logs';
    }
}