<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class DiagnosticRequest extends BaseModel
{
    protected $table = 'diagnostic_request';

    protected $fillable = [
        'name'
    ];

    public function type()
    {
        return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\DiagnosticType::class);
    }
}