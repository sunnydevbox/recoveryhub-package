<?php
namespace Sunnydevbox\Recoveryhub\Models;

use \Sunnydevbox\TWCart\Models\Item as ExetendItem;
use \Sunnydevbox\Recoveryhub\Traits\CartItemTrait;
use \Sunnydevbox\Recoveryhub\Traits\FeeTrait;

class Item extends ExetendItem
{
    use CartItemTrait, FeeTrait;

    protected $appends = [
        'total',

        // CartItemTrait
        'tax_amount',
        'total',
        'sub_total',

        // FEE
        'less_tax',
        'withholding',
        'professional_fee',
        'net_income',
        'admin_fee',
    ];

    public function product()
    {
        // return $this->morphMany('App\Comment', 'commentable');
        return $this->morphTo();
    }

    public function object()
    {
        return $this->morphTo(null, 'class', 'reference_id');
    }

    public function order()
    {
        // return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\Order::class, 'order_id','id');
        return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\Order::class);
    }

    public function getPriceAttribute()
    {
        return stringToFloat($this->attributes['price']);
    }

    public function getTaxAttribute()
    {
        return stringToFloat($this->attributes['tax']);
    }

    public function scopeIsOrdered($query)
    {
        return $query->whereNotNull('cart_items.order_id')
            ->whereNull('cart_items.cart_id')
            ;
    }

    public function scopeIsNotOrdered($query)
    {
        return $query->whereNull('order_id')
            ->whereNotNull('cart_id')
            ;
    }
}