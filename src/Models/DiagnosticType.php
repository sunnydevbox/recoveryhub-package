<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class DiagnosticType extends BaseModel
{
    protected $table = 'diagnostic_type';

    protected $fillable = [
        'name',
        'diagnostic_request_id',
        'diagnostic_test_id'
    ];

    public function request()
    {
        return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\DiagnosticRequest::class, 'id', 'diagnostic_request_id');
    }

    public function test()
    {
        return $this->hasOne(\Sunnydevbox\Recoveryhub\Models\DiagnosticTest::class, 'id', 'diagnostic_test_id');
    }
}