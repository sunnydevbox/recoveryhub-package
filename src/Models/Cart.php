<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Amsgames\LaravelShop\Models\ShopCartModel;
use Sunnydevbox\Recoveryhub\Traits\ShopCalculationsTrait;

use Sunnydevbox\Recoveryhub\Traits\HasCouponTrait;
use Config;

class Cart extends ShopCartModel
{
    use ShopCalculationsTrait;
    use HasCouponTrait; 


    //     /**
    //  * Transforms cart into an order.
    //  * Returns created order.
    //  *
    //  * @param string $statusCode Order status to create order with.
    //  *
    //  * @return Order
    //  */
    // public function placeOrdera($statusCode = null)
    // {
    //     if (empty($statusCode)) $statusCode = Config::get('shop.order_status_placement');
    //     // Create order
    //     $order = call_user_func( Config::get('shop.order') . '::create', [
    //         'user_id'       => $this->user_id,
    //         'statusCode'    => $statusCode
    //     ]);


    //     // Map cart items into order
    //     for ($i = count($this->items) - 1; $i >= 0; --$i) {
    //         // Attach to order
    //         $this->items[$i]->order_id  = $order->id;
    //         // Remove from cart
    //         $this->items[$i]->cart_id   = null;
    //         // Update
    //         $this->items[$i]->save();
    //     }
    //     $this->resetCalculations();
        
    //     return $order;
    // }
    
}