<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class DiagnosticTest extends BaseModel
{
    protected $table = 'diagnostic_test';

    protected $fillable = [
        'name'
    ];

    public function type()
    {
        return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\DiagnosticType::class);
    }
}