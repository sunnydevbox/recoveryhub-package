<?php
namespace Sunnydevbox\Recoveryhub\Models;

use \Sunnydevbox\TWUser\Models\User as TWUserModel;
use \Sunnydevbox\TWCore\Repositories\TWMetaTrait;
use Sunnydevbox\Recoveryhub\Traits\EventTrait;
use Sunnydevbox\TWCore\Traits\FileAttachmentTrait;
use Amsgames\LaravelShop\Traits\ShopUserTrait;

use Sunnydevbox\Recoveryhub\Traits\MedicalPractitionerRateChargeModelTrait;
use Sunnydevbox\Recoveryhub\Traits\MentalHealthDataTrait;
use Sunnydevbox\Recoveryhub\Traits\DataCounterTrait;

class User extends TWUserModel
{
    use TWMetaTrait;
    use EventTrait;
    use ShopUserTrait;
    use MedicalPractitionerRateChargeModelTrait;
    use MentalHealthDataTrait;
    use DataCounterTrait;

    use FileAttachmentTrait;

    protected $appends = [
        'first_name',
        'last_name',
        'general_information',
        'picture',
        'signature',
        'doctorRole',
        'avatar',
    ];

    // public $mentalHealthDataColumn = 'general_information';

    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
        'is_verified',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $meta = [
        'first_name',
        'middle_name',
        'last_name',
        'phone',
        'address_1',
        'barangay',
        'city',
        'state',
        'zipcode',
        'license_number',
        's2_number',
        's2_expiration_date',
        'prt_number',
        'tin',
        'sinch_ticket',
        'timezone',
        'sex',
        'civil_status',
        'age',
        'date_of_birth',
        'place_of_birth',
        'religion',
        'nationality',
        'race',
        'ethnicity',
        'perm_no_street',
        'perm_barangay',
        'perm_city_municipality',
        'perm_province',
        'perm_zip_code',
        'landline_number',
        'mobile_number',
        'email_address',
        'highest_educational_attainment',
        'occupation',
        'company',
        'company_address_and_contact_details',
        'height',
        'weight',
        'contact_first_name',
        'contact_land_line',
        'contact_last_lame',
        'contact_middle_name',
        'contact_mobile_no',
        'contact_address',
        'voluntarily_accept',
        'suffix',
        'prefix',
        'fellowOrDiplomate',
        'hospitalAffiliation',
        'specialty',
        'streetAddress',
        'streetNo',
        'life',
        'license_expiration_date',
    ];

    protected $fieldSearchable = [
        'email'
    ]; 

    public function getFirstNameAttribute()
    {
        return $this->getMeta('first_name');
    }

    public function getLastNameAttribute()
    {
        return $this->getMeta('last_name');
    }

    public function getAddress1Attribute()
    {
        return $this->getMeta('address_1');
    }

    public function getCityAttribute()
    {
        return $this->getMeta('ciy');
    }

    public function getStateAttribute()
    {
        return $this->getMeta('state');
    }

    public function getZipcodeAttribute()
    {
        return $this->getMeta('zipcode');
    }

    public function getFullAddressAttribute()
    {
        return join(' ', [$this->getMeta('address_1'), $this->getMeta('city'), $this->getMeta('state'), $this->getMeta('zipcode')]);
    }

    public function getPermNoStreetAttribute()
    {
        return $this->getMeta('perm_no_street');
    }

    public function getPermBarangayAttribute()
    {
        return $this->getMeta('perm_barangay');
    }

    public function getPermCityMunicipalityAttribute()
    {
        return $this->getMeta('perm_city_municipality');
    }

    public function getPermProvinceAttribute()
    {
        return $this->getMeta('perm_province');
    }

    public function getPermZipCodeAttribute()
    {
        return $this->getMeta('perm_zip_code');
    }

    public function getGeneralInformationAttribute()
    {        
        $m = $this->getAllMeta();
        $m['doctorRole'] = null;
        
        $roles = $this->roles->pluck('name');
        if ($roles->count()) {
            $m['doctorRole'] = $this->roles->pluck('name')[0];
        } 

        return $m;
    }

    public function getPictureAttribute()
    {
        return isset($this->attachment('picture')->url) ? $this->attachment('picture')->url : '';
    }

    public function getAvatarAttribute()
    {
        $avatars = $this->attachmentsGroup('user_avatar')
            ->keyBy('key')
            ->map(function($item) {
                return $item->url;
            })->all();
        
        if (!isset($avatars['picture'])) {
            $avatars['picture'] = $this->getPictureAttribute();
        }

        return $avatars;
    }

    public function getSignatureAttribute()
    {
        if ($this->attachment('signature')) {
            return storage_path('app/' . $this->attachment('signature')->filepath);
        }
        
        return null;
    }


    public function getDoctorRoleAttribute()
    {
        $roles = $this->roles->pluck('name');
        if ($roles->count()) {
            return $this->roles->pluck('name')[0];
        } 
        return null;
    }

    public function bookings()
    {
        return $this->MorphMany('Sunnydevbox\Recoveryhub\Models\Appointment', 'assignable');
    }
        

    public function _meta()
    {
        return collect($this->meta);
    }

    public function scopeIsActive($query)
	{
		$query->where('status', 'active');
	}

	public function scopeIsInactive($query)
	{
		$query->where('status', 'inactive');
	}

    public static function boot()
    {
        parent::boot();

        User::observe(new \Sunnydevbox\Recoveryhub\Observers\PatientObserver);
    }
}