<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class EventFeedback extends BaseModel {

    protected $table = 'event_feedbacks';
    
    protected $fillable =  [
        'user_id',
        'event_id',
        'comment',
        'history_of_present_illness',
        'brief_summary_of_positive_aspects',
        'symptom_information',
        'developmental_history',
        'past_medical_psychiatric_history',
        'family_history',
        'medication_history_current_medications',
        'abuses_trauma',
        'mental_status_examination',
        'physical_appearance',
        'behavior',
        'speech',
        'mood',
        'affect',
        'thoughts',
        'suicidal_or_homicidal_intent_present',
        'psychosis',
        'cognition',
        'insight',
        'impression_and_codes',
        'psychiatric_diagnosis',
        'pedical_diagnosis',
        'self_harmViolence_potential',
        'treatment_recommendations',
        'lifestyle',
        'recommended_medication_changes',
        'discuss_cautionsSide_effects_for_use_of_medication',
        'referrals_required',
        'psychotherapy',
        'psycho_education',
        'chief_complaint',
        'general_data',
        'course_in_the_ward_out_patient',
        'laboratory_examination_result',
        'psychological_examination_result',
        'diagnosis',
        'medication',
        'recommendation',
    ];

    public function event()
    {
        return $this->belongsTo(config('recoveryhub.models.event'));
    }

    public function user()
    {
        return $this->belongsTo(config('recoveryhub.models.user'));
    }

    public static function boot()
    {
        parent::boot();

        EventFeedback::observe(new \Sunnydevbox\Recoveryhub\Observers\EventFeedbackObserver);
    }
}