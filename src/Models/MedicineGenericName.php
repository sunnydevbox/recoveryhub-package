<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class MedicineGenericName extends BaseModel
{
    protected $table = 'medicine_generic_name';

    public $timestamps = false;
    
    protected $fillable = [
        'name', 
    ];

    public function medicines()
    {
        return $this->hasMany(config('recoveryhub.models.medicine-inventory'), 'generic_name_id');
    }
}