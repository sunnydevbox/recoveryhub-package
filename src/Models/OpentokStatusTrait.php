<?php
namespace Sunnydevbox\Recoveryhub\Models;

trait OpentokStatusTrait
{

    public function scopeStatusStarted($query)
    {
        $this->scopeStatus($query, self::STATUS_STARTED);
    }

    public function scopeStatusEnded($query)
    {
        $this->scopeStatus($query, self::STATUS_ENDED);
    }

    public function scopeStatusPending($query)
    {
        $this->scopeStatus($query, self::STATUS_PENDING);
    }

    public function scopeStatusCancelled($query)
    {
        $this->scopeStatus($query, self::STATUS_CANCELLED);
    }

    public function scopeStatus($query, $status)
    {
        switch(strtolower(trim($status))) {

            case self::STATUS_ENDED: 
                $status = self::STATUS_ENDED;
            break;

            case self::STATUS_STARTED: 
                $status = self::STATUS_STARTED;
            break;

            case self::STATUS_PENDING: 
                $status = self::STATUS_PENDING;
            break;

            default:
                $status = false;
            break;
        }
        
        if (is_string($status)) {
            $query->where('status', $status);
        }
    }

    public function isStarted()
    {
        $this->where('status', self::STATUS_STARTED);
    }

    public function isEnded()
    {
        $this->where('status', self::STATUS_ENDTED);
    }
    
    public function isPending()
    {
        $this->where('status', self::STATUS_PENDING);
    }

    public function isCancelled()
    {
        $this->where('status', self::STATUS_CANCELLED);
    }
}