<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sunnydevbox\Recoveryhub\Models\OpentokStatusTrait;

class OpenTok extends BaseModel
{
    use SoftDeletes, OpentokStatusTrait;

    protected $table = 'opentok';

    protected $fillable = [
        'event_id',
        'session',
        'status',
        'notes',
        'host_status', // doctor
        'attendee_status', // patient
    ];

    const STATUS_PENDING = 'pending';
    const STATUS_STARTED = 'started';
    const STATUS_ENDED = 'ended';
    const STATUS_CANCELLED = 'cancelled';

    const USER_STATUS_DISCONNECTED = 'disconnected';
    const USER_STATUS_CONNECTED = 'connected'; // user is connected to the session and is ready to receive call

	public function event()
	{
		return $this->belongsTo(\Sunnydevbox\Recoveryhub\Models\Event::class);
    }

    public function getHostStatusAttribute()
    {
        return (!$this->attributes['host_status']) ? 0 : 1;
    }

    public function getAttendeeStatusAttribute()
    {
        return (!$this->attributes['attendee_status']) ? 0 : 1;
    }

    public function scopeHostConnected($query) 
    {
        $this->scopeUserStatus($query, 'host', self::USER_STATUS_CONNECTED);
    }

    public function scopeHostDisconnected($query) 
    {
        $this->scopeUserStatus($query, 'host', self::USER_STATUS_DISCONNECTED);
    }

    public function scopeAttendeeConnected($query) 
    {
        $this->scopeUserStatus($query, 'attendee', self::USER_STATUS_CONNECTED);
    }

    public function scopeAttendeeDisconnected($query) 
    {
        $this->scopeUserStatus($query, 'attendee', self::USER_STATUS_DISCONNECTED);
    }

    /**
     * param $userType host|attendee
     */
    public function scopeUserStatus($query, $userType, $status)
    {
        $query->where($userType . '_status', $status);
    }
    
}