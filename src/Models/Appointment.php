<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWBookings\Models\Booking;
use Sunnydevbox\TWBookings\Traits\BookingTrait;

class Appointment extends Booking
{
	// use BookingTrait;

	// protected $fillable = [
	// 	'bookable_id',
	// 	'bookable_type',
	// 	'assignable_id',
	// 	'assignable_type',
	// 	'deleted_at',
	// ];

	protected $hidden = [
		'bookable_id',
		'bookable_type',
		'assignable_id',
		'assignable_type',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	public function event()
    {	
		return $this->bookable();
		// return $this->belongsTo(config('recoveryhub.models.event'), 'bookable_id');
        return $this->morphedByMany(config('recoveryhub.models.event')
        	, 'bookable'
        	, 'bookings'
        	, 'bookable_type'
        	, 'bookable_id'
        );
	}

	public function event_v2()
	{
		// return $this->morphedByMany(config('recoveryhub.models.event')
    //     	, 'bookable'
    //     	, 'bookings'
    //     	, 'bookable_type'
    //     	, 'bookable_id'
    //     );
		// return $this->morphOne(config('recoveryhub.models.event'), 'bookable');
		// return $this->bookable();
		return $this->belongsTo(config('recoveryhub.models.event'), 'bookable_id');
		return $this->morphTo(config('recoveryhub.models.event'), 'bookable');
		dd('e2');
	}
	
	public function patient()
	{
		return $this->belongsTo(config('auth.providers.users.model'), 'assignable_id');
	}

	public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->fillable(array_merge( [
            'deleted_at',
        ], parent::getFillable()));
    }
}