<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Carbon\Carbon;

class OpentokLog extends BaseModel
{
    protected $table = 'opentok_logs';

    protected $dates = ['event_at'];

    protected $fillable = [
        'event_id',
        'user_id',
        'name',
        'detail',
        'event_at',
    ];

    public function setEventAtAttribute($value)
    {
        if (is_numeric($value)) {
            $this->attributes['event_at'] = Carbon::createFromTimestampMs($value);
        }   
    }

    public function getDetailAttribute()
    {
        $detail = $this->attributes['detail'];
        $d = [];
        
        if (!@unserialize($detail)) {
            $d[] = $detail;
            return $d;
        } else {
            $d = unserialize($detail);
            return $d;
        }
    }

    public function setDetailAttribute($value)
    {
        if (gettype($value) == 'string') {
            $detail = [
                $value
            ];
        } else if (gettype($value) == 'array') {
            $detail = $value;
        }
        
        $this->attributes['detail'] = serialize($detail);
    }

    
}