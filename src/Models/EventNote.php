<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class EventNote extends BaseModel
{
    protected $fillable = [
        'event_id',
        'notes',
    ];

    protected $hidden = [
        'created_at',
        'deleted_at',
    ];

	public function event()
	{
        return $this->belongsTo(config('recoveryhub.models.event'));
    }

    public function getNotesAttribute()
    {
        if (@unserialize($this->attributes['notes'])) {
            return unserialize($this->attributes['notes']);
        }
        return $this->attributes['notes'];
    }

    public function setNotesAttribute($data)
    {
        if (is_array($data)) {
            $this->attributes['notes'] = serialize($data);
        }
    }

    public function getTable()
    {
        return 'event_notes';
    }
}