<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class ProfileQuestionAnswer extends BaseModel
{
    protected $table = 'profile_question_answers';

    protected $fillable = [
        'user_id',
        'question_id',
        'answer',
    ];

    public function question()
    {
        return $this->belongsTo(config('recoveryhub.models.profile-question'));
    }

    public function user()
    {
        return $this->belongsTo(condif('auth.providers.users.model'));
    }

    public function getAnswerAttribute()
    {
        return unserialize($this->attributes['answer']);
    }

}