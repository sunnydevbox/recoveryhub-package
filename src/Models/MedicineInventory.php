<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Amsgames\LaravelShop\Traits\ShopItemTrait;

class MedicineInventory extends BaseModel
{
    use ShopItemTrait;
    
    protected $table = 'medicine_inventory';

    public $timestamps = false;

    public $appends = [
        'generic_name'
    ];

    protected $fillable = [
        'name', 
        'generic_name_id',
        'unit',
        'dosage',
        'remarks', 
        'is_s2',
        'is_injectable',
        'is_active',
        'price'
    ];

    public function generic()
    {
        return $this->hasOne(config('recoveryhub.models.medicine-generic-name'), 'id', 'generic_name_id');
    }

    public function scopeS2($query)
    {
        $query->where('is_s2', true);
    }

    public function scopeActive($query)
    {
        $query->where('is_active', true);
    }

    public function scopeInjectable($query)
    {
        $query->where('is_injectable', true);
    }

    public function getGenericNameAttribute()
    {
        return $this->generic->name;
    }


    /** SHOP */


    public function getTaxAttribute()
    {
        if (!isset($this->attributes['tax'])) {
            return (float) 0.12;
        } else {
            $tax = stringToFloat($this->attributes['tax']);

            if (!$tax) {
                $tax = 0.12;
            }

            return (float) $tax;
        }
    }
}