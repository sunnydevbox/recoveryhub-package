<?php

namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;
use Sunnydevbox\Recoveryhub\Models\User;
use Sunnydevbox\TWEvents\Traits\EventTrait;

class Patient extends User
{
	public function getTable()
	{
		return (new User)->getTable();
	}
}