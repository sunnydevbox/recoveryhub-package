<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCart\Models\Coupon as CCoupon;
// use \Carbon\Carbon;

class Coupon extends CCoupon
{

    protected $fillable = [
        'item_type',
        'code',
        'name',
        'description',
        'sku',
        'value',
        'discount',
        'active',
        'expires_at',
        'processor',
        'usage_limit',
    ];


    public function hasLimitReached()
    {
        if (!$this->usage_limit) {
            return false;
        } else {
            return ($this->usage_limit <= $this->orders()->count()) ? true : false;
        }
    }
    
}