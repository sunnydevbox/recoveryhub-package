<?php
namespace Sunnydevbox\Recoveryhub\Models;

use Sunnydevbox\TWCore\Models\BaseModel;

class TokboxCallback extends BaseModel
{
    public $timestamps = false;

    protected $fillable = [
        'session_id',
        'project_id',
        'event',
        'reason',
        'timestamp',
        'connection',
        'stream'
    ];

    public function getTimestampAttribute()
    {
        return \Carbon\Carbon::createFromTimestampMs($this->attributes['timestamp'],  'UTC')->format('Y-m-d H:i:s');
    }

    public function setConnectionAttribute($value) 
    {
        $this->attributes['connection'] = $value; 

        if (is_array($value)) {
            $this->attributes['connection'] = serialize($value);
        }
    }

    public function getConnectionAttribute() 
    {
        if ($value = @unserialize($this->attributes['connection'])) {
            $this->attributes['connection'] = $value;
        }

        return $this->attributes['connection']; 
    }

    public function setStreamAttribute($value) 
    {
        $this->attributes['stream'] = $value; 

        if (is_array($value)) {
            $this->attributes['stream'] = serialize($value);
        }
    }

    public function getStreamAttribute() 
    {
        if ($value = @unserialize($this->attributes['stream'])) {
            $this->attributes['stream'] = $value;
        }

        return $this->attributes['stream']; 
    }
}